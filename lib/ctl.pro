TEMPLATE = lib
VERSION = 0.5.3

win32 {
    CONFIG += staticlib
}

DEFINES += CTL_LIBRARY

# CTL modules that should be included in the pre-compiled library
include(../modules/ctl.pri)
DEFINES += CTL_DATABASE_DIR=\\\"$$clean_path($$PWD/../database)\\\"

include(../modules/ctl_ocl.pri)
DEFINES += OCL_SOURCE_DIR=\\\"$$clean_path($$PWD/../modules/src/ocl/cl_src)\\\"

include(../modules/ctl_qtgui.pri)

#include(../modules/ctl_nlopt.pri)

DESTDIR = $$PWD
