#include "ctl.h"
#include <QCoreApplication>

int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);

    using namespace CTL;
    qInstallMessageHandler(MessageHandler::qInstaller);
    MessageHandler::instance().blacklistMessageType(QtDebugMsg);

    // create a C-arm-CT system
    auto system = makeSimpleCTSystem<blueprints::GenericCarmCT>();

    // set the tube voltage [kV]
    static_cast<XrayTube*>(system->source())->setTubeVoltage(109.);

    // add a filter (optional)
    auto beamfilterAl = makeComponent<AttenuationFilter>(database::Element::Al, 1.f); // 1mm alu
    system->addBeamModifier(std::move(beamfilterAl));

    // create a RadiationEncoder instance to compute spectrum (meta) values
    const RadiationEncoder encoder(system.get());

    constexpr auto cm2mm = 0.1; // conversion factor from 1/cm to 1/mm
    constexpr auto density = 1.0; // = database::density(database::Composite::Water)
    constexpr auto massMu2Mu = density * cm2mm; // converts mass atten. coef. to atten. coef.

    // ### Compute attenuation of water for the mean energy ###
    // mean energy of the system (depending on X-ray source, beam modifiers and detector response)
    const auto meanEnergy = encoder.detectiveMeanEnergy();
    // lookup mu for mean energy
    qInfo() << "Mean mu:"
            << attenuationModel(database::Composite::Water)->valueAt(meanEnergy) * massMu2Mu;

    // ### Compute attenuation of water for the effective energy ###
    // sample the spectrum with 100 samples
    const auto spectrum = encoder.finalSpectrum(100);
    // sample attenuation spectrum (same sampling as spectrum)
    const auto sampledMu = XYDataSeries::sampledFromModel(
        attenuationModel(database::Composite::Water), spectrum.samplingPoints());
    // compute weigted sum

    qInfo() << "Effective mu:" << sampledMu.weightedSum(spectrum.values()) * massMu2Mu;

    return 0;
}
