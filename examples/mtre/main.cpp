#include "ctl.h"

#include <QCommandLineParser>
#include <QCoreApplication>
#include <iostream>

struct OrientationParameter
{
    double transX, transY, transZ;
    double rotX, rotY, rotZ;
};

OrientationParameter extractParameters(QVector<double> transParamVector, QVector<double> rotParamVector, int idx);
OrientationParameter extractAveragedParameters(QVector<double> transParamVector, QVector<double> rotParamVector);
CTL::VoxelVolume<float> loadSampleVolume(const QString& fileName);

/*!
 * Computes the mean target registration error (mTRE).
 *
 * Sampling is defined by the volume data and groundtruth/goldstandard and estimated transformations are read as vectors of rotation and translation parameters.
 */
int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("mean target registration error (mTRE)");
    parser.addVersionOption();
    parser.addHelpOption();

    parser.addPositionalArgument("sample-volume", "NRRD file");
    parser.addPositionalArgument("groundtruth-rotation", "DEN file");
    parser.addPositionalArgument("groundtruth-translation", "DEN file");
    parser.addPositionalArgument("estimate-rotation", "DEN file");
    parser.addPositionalArgument("estimate-translation", "DEN file");
    parser.addOptions({
        { { "i", "index" }, "Parameter (view) index.", "index" }
    });
    parser.process(app);
    
    try
    {
    	qInstallMessageHandler(CTL::MessageHandler::qInstaller);

        CTL::io::BaseTypeIO<CTL::io::NrrdFileIO> fileIO;
        
        const auto vol = loadSampleVolume(parser.positionalArguments().at(0));

        QVector<double> rotParamVectorTrue = CTL::io::den::loadDouble<QVector>(parser.positionalArguments().at(1));
        QVector<double> transParamVectorTrue = CTL::io::den::loadDouble<QVector>(parser.positionalArguments().at(2));
        QVector<double> rotParamVectorEst = CTL::io::den::loadDouble<QVector>(parser.positionalArguments().at(3));
        QVector<double> transParamVectorEst = CTL::io::den::loadDouble<QVector>(parser.positionalArguments().at(4));
        
        if (transParamVectorTrue.size() != rotParamVectorTrue.size())
            qWarning("Groundtruth translation and rotation parameters differ in size!");
        if (transParamVectorEst.size() != rotParamVectorEst.size())
            qWarning("Estimated translation and rotation parameters differ in size!");
        
        OrientationParameter paramTrue;
        OrientationParameter paramEst;
        
        if (parser.isSet("i"))
        {
            const auto idx = parser.value("i").toInt();
            paramTrue = extractParameters(transParamVectorTrue, rotParamVectorTrue, idx);
            paramEst = extractParameters(transParamVectorEst, rotParamVectorEst, idx);
        }
        else
        {
            paramTrue = extractAveragedParameters(transParamVectorTrue, rotParamVectorTrue);
            paramEst = extractAveragedParameters(transParamVectorEst, rotParamVectorEst);
        }
        
        const auto rotationTrue = CTL::mat::rotationMatrix({paramTrue.rotX, paramTrue.rotY, paramTrue.rotZ});
        const auto translationTrue = CTL::mat::Matrix<3,1>{paramTrue.transX, paramTrue.transY, paramTrue.transZ};
        
        const auto rotationEst = CTL::mat::rotationMatrix({paramEst.rotX, paramEst.rotY, paramEst.rotZ});
        const auto translationEst = CTL::mat::Matrix<3,1>{paramEst.transX, paramEst.transY, paramEst.transZ};
        
        const auto HTrue = CTL::Homography3D(rotationTrue, translationTrue);
        const auto HEst = CTL::Homography3D(rotationEst, translationEst);
        
        CTL::mat::Matrix<4,1> homogenousCoord = {0, 0, 0, 1};
        std::vector<double> err;
        err.reserve(vol.dimensions().totalNbElements());
        
        std::cout << std::endl;

        for (uint z = 0; z < vol.dimensions().z; ++z)
            for (uint y = 0; y < vol.dimensions().y; ++y)
                for (uint x = 0; x < vol.dimensions().x; ++x)
                {
                    const auto c = vol.coordinates(x,y,z);
                    homogenousCoord.get<0>() = c.x();
                    homogenousCoord.get<1>() = c.y();
                    homogenousCoord.get<2>() = c.z();
                            
                    const auto ATrue = HTrue * homogenousCoord;
                    const auto AEst = HEst * homogenousCoord;
                    
                    // normalization is not required due to Euclidian transform
                    //ATrue /= ATrue.get<3,0>();
                    //AEst /= AEst.get<3,0>();
                    
                    const auto diff = ATrue - AEst;
                    err.push_back(diff.norm());

                    //std::cout << "(" << x << "," << y << "," << z << "): " << diff.norm() << std::endl;
                }
        
        const auto mTRE = std::accumulate(err.begin(), err.end(), 0.0)
                / double(vol.nbVoxels().totalNbElements());
        const auto minError = *std::min_element(err.begin(), err.end());
        const auto maxError = *std::max_element(err.begin(), err.end());
        
        
        std::cout << std::endl << std::endl << "mTRE: " << mTRE << " (mm)" << std::endl
                  << " min: " << minError << " (mm)" << std::endl 
                  << " max: " << maxError << " (mm)" << std::endl;
        
        std::cout << std::endl << "Program finished" << std::endl;
        //return app.exec();
    }
    catch(const std::exception& e)
    {
        std::cerr << "exception caught:\n" << e.what() << std::endl;
        return -1;
    }
}

/*!
 * Extracts the translation and rotation parameters at the specified index.
 *
 */
OrientationParameter extractParameters(QVector<double> transParamVector, QVector<double> rotParamVector, int idx)
{
    return {
        transParamVector.at(3 * idx + 0),
        transParamVector.at(3 * idx + 1),
        transParamVector.at(3 * idx + 2),
        rotParamVector.at(3 * idx + 0),
        rotParamVector.at(3 * idx + 1),
        rotParamVector.at(3 * idx + 2)
    };
}

/*!
 * Extracts the averaged translation and rotation parameters.
 *
 * startOffset and endOffset specify the indices between which the average is to be computed.
 */
OrientationParameter extractAveragedParameters(QVector<double> transParamVector, QVector<double> rotParamVector)
{
    if((transParamVector.size() != rotParamVector.size()) || (transParamVector.size() % 3 != 0))
        throw std::runtime_error("invalid number of translation/rotation vectors.");

    OrientationParameter ret{ 0., 0., 0.,
                              0., 0., 0. };

    for (auto i = 0; i < transParamVector.size(); ++i)
    {
        const auto coordNb = i % 3;
        if (coordNb == 0)
        {
            ret.transX += transParamVector.at(i);
            ret.rotX += rotParamVector.at(i);
        }
        else if (coordNb == 1)
        {
            ret.transY += transParamVector.at(i);
            ret.rotY += rotParamVector.at(i);
        }
        else //if (coordNb == 2)
        {
            ret.transZ += transParamVector.at(i);
            ret.rotZ += rotParamVector.at(i);
        }
    }

    ret.transX /= transParamVector.size() / 3;
    ret.transY /= transParamVector.size() / 3;
    ret.transZ /= transParamVector.size() / 3;
    ret.rotX /= rotParamVector.size() / 3;
    ret.rotY /= rotParamVector.size() / 3;
    ret.rotZ /= rotParamVector.size() / 3;

    return ret;
}

CTL::VoxelVolume<float> loadSampleVolume(const QString& fileName)
{
    using namespace CTL::io;

    // load only nrrd meta info
    const auto metaInfo = BaseTypeIO<NrrdFileIO>{}.metaInfo(fileName);

    const auto volDims = metaInfo.value(meta_info::dimensions).value<meta_info::Dimensions>();

    if(volDims.nbDim != 3)
        throw std::runtime_error("Invalid number of dimensions of the input volume.");

    auto ret = CTL::VoxelVolume<float>{ volDims.dim1,
                                        volDims.dim2,
                                        volDims.dim3,
                                        metaInfo.value(meta_info::voxSizeX).toFloat(),
                                        metaInfo.value(meta_info::voxSizeY).toFloat(),
                                        metaInfo.value(meta_info::voxSizeZ).toFloat() };

    ret.setVolumeOffset(metaInfo.value(meta_info::volOffX).toFloat(),
                        metaInfo.value(meta_info::volOffY).toFloat(),
                        metaInfo.value(meta_info::volOffZ).toFloat());

    std::cout << "volume info\n"
              << "-----------\n"
              << "number voxels: " << ret.dimensions().info() << '\n'
              << "voxel size:    " << ret.voxelSize().info() << '\n'
              << "volume offset: " << ret.offset().info() << '\n';

    return ret;
}

