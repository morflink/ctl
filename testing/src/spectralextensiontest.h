#ifndef SPECTRALEXTENSIONTEST_H
#define SPECTRALEXTENSIONTEST_H

#include <QtTest>

#include "acquisition/acquisitionsetup.h"
#include "img/compositevolume.h"

namespace CTL
{
class ProjectionData;
}

class SpectralExtensionTest : public QObject
{
    Q_OBJECT

public:
    SpectralExtensionTest();

    void makeTestDataGroundTruth() const;

private Q_SLOTS:
    void initTestCase();
    void testSpectralExtension();
    void testNegativeVolume();

private:
    // helper methods
    double projectionMean(const CTL::ProjectionData& projections) const;
    double projectionVariance(const CTL::ProjectionData& projections) const;
    void evaluateData(const CTL::ProjectionData& difference, const QString& caseName) const;

    CTL::AcquisitionSetup _setup;
    CTL::CompositeVolume _compVol;
    CTL::SpectralVolumeData _volume;
};

#endif // SPECTRALEXTENSIONTEST_H
