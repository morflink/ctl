#ifndef CTL_NEIGHBORSELECTOR_H
#define CTL_NEIGHBORSELECTOR_H

#include "img/voxelvolume.h"

#include <array>

namespace CTL {

template<typename>
class VoxelVolume;

namespace assist {
struct Neighbors
{
    std::array<float, 12> order2;
    std::array<float, 8>  order3;
    std::array<float, 6>  order1;
    float center;
};

int mirrorIdx(int idx, int boundary);
} // namespace assist

class NeighborSelector
{
public:
    NeighborSelector(const VoxelVolume<float>& volume);

    using Neighbors = assist::Neighbors;

    Neighbors neighborsUnsafe(int x, int y, int z) const;
    Neighbors neighborsMirrored(int x, int y, int z) const;

    std::array<float, 6> directNeighborsUnsafe(int x, int y, int z) const;
    std::array<float, 6> directNeighborsMirrored(int x, int y, int z) const;

    std::array<float, 7> voxelAndDirectNeighborsUnsafe(int x, int y, int z) const;
    std::array<float, 7> voxelAndDirectNeighborsMirrored(int x, int y, int z) const;

    std::array<float, 27> voxelAndNeighborsUnsafe(int x, int y, int z) const;
    std::array<float, 27> voxelAndNeighborsMirrored(int x, int y, int z) const;



private:
    const VoxelVolume<float>& _volume;
    int X;
    int Y;
    int Z;
};

inline NeighborSelector::NeighborSelector(const VoxelVolume<float>& volume)
    : _volume(volume)
{
    X = int(volume.dimensions().x);
    Y = int(volume.dimensions().y);
    Z = int(volume.dimensions().z);
}

inline std::array<float, 6> NeighborSelector::directNeighborsUnsafe(int x, int y, int z) const
{
    std::array<float, 6> nb;
    nb[0] = _volume(x - 1, y,     z);
    nb[1] = _volume(x + 1, y,     z);
    nb[2] = _volume(x,     y - 1, z);
    nb[3] = _volume(x,     y + 1, z);
    nb[4] = _volume(x,     y,     z - 1);
    nb[5] = _volume(x,     y,     z + 1);

    return nb;
}

inline std::array<float, 6> NeighborSelector::directNeighborsMirrored(int x, int y, int z) const
{
    using assist::mirrorIdx;
    std::array<float, 6> nb;
    nb[0]  = _volume(mirrorIdx(x - 1, X), y,                   z);
    nb[1]  = _volume(mirrorIdx(x + 1, X), y,                   z);
    nb[2]  = _volume(x,                   mirrorIdx(y - 1, Y), z);
    nb[3]  = _volume(x,                   mirrorIdx(y + 1, Y), z);
    nb[4]  = _volume(x,                   y,                   mirrorIdx(z - 1, Z));
    nb[5]  = _volume(x,                   y,                   mirrorIdx(z + 1, Z));

    return nb;
}

inline std::array<float, 7> NeighborSelector::voxelAndDirectNeighborsMirrored(int x, int y, int z) const
{
    using assist::mirrorIdx;
    std::array<float, 7> nb;
    nb[0]  = _volume(x,                   y,                   z);
    nb[1]  = _volume(mirrorIdx(x - 1, X), y,                   z);
    nb[2]  = _volume(mirrorIdx(x + 1, X), y,                   z);
    nb[3]  = _volume(x,                   mirrorIdx(y - 1, Y), z);
    nb[4]  = _volume(x,                   mirrorIdx(y + 1, Y), z);
    nb[5]  = _volume(x,                   y,                   mirrorIdx(z - 1, Z));
    nb[6]  = _volume(x,                   y,                   mirrorIdx(z + 1, Z));

    return nb;
}

inline std::array<float, 7> NeighborSelector::voxelAndDirectNeighborsUnsafe(int x, int y, int z) const
{
    std::array<float, 7> nb;
    nb[0] = _volume(x,     y,     z);
    nb[1] = _volume(x - 1, y,     z);
    nb[2] = _volume(x + 1, y,     z);
    nb[3] = _volume(x,     y - 1, z);
    nb[4] = _volume(x,     y + 1, z);
    nb[5] = _volume(x,     y,     z - 1);
    nb[6] = _volume(x,     y,     z + 1);

    return nb;
}

inline NeighborSelector::Neighbors NeighborSelector::neighborsUnsafe(int x, int y, int z) const
{
    Neighbors nb;
    nb.center    = _volume(x,     y,     z);

    nb.order1[0] = _volume(x - 1, y,     z);
    nb.order1[1] = _volume(x + 1, y,     z);
    nb.order1[2] = _volume(x,     y - 1, z);
    nb.order1[3] = _volume(x,     y + 1, z);
    nb.order1[4] = _volume(x,     y,     z - 1);
    nb.order1[5] = _volume(x,     y,     z + 1);

    nb.order2[0]  = _volume(x,     y - 1, z - 1);
    nb.order2[1]  = _volume(x,     y - 1, z + 1);
    nb.order2[2]  = _volume(x,     y + 1, z - 1);
    nb.order2[3]  = _volume(x,     y + 1, z + 1);
    nb.order2[4]  = _volume(x - 1, y    , z - 1);
    nb.order2[5]  = _volume(x - 1, y    , z + 1);
    nb.order2[6]  = _volume(x + 1, y    , z - 1);
    nb.order2[7]  = _volume(x + 1, y    , z + 1);
    nb.order2[8]  = _volume(x - 1, y - 1, z);
    nb.order2[9]  = _volume(x - 1, y + 1, z);
    nb.order2[10] = _volume(x + 1, y - 1, z);
    nb.order2[11] = _volume(x + 1, y + 1, z);

    nb.order3[0] = _volume(x - 1, y - 1, z - 1);
    nb.order3[1] = _volume(x - 1, y - 1, z + 1);
    nb.order3[2] = _volume(x - 1, y + 1, z - 1);
    nb.order3[3] = _volume(x - 1, y + 1, z + 1);
    nb.order3[4] = _volume(x + 1, y - 1, z - 1);
    nb.order3[5] = _volume(x + 1, y - 1, z + 1);
    nb.order3[6] = _volume(x + 1, y + 1, z - 1);
    nb.order3[7] = _volume(x + 1, y + 1, z + 1);

    return nb;
}

inline NeighborSelector::Neighbors NeighborSelector::neighborsMirrored(int x, int y, int z) const
{
    using assist::mirrorIdx;
    Neighbors nb;
    nb.center     = _volume(x,     y,     z);

    nb.order1[0]  = _volume(mirrorIdx(x - 1, X), y,                   z);
    nb.order1[1]  = _volume(mirrorIdx(x + 1, X), y,                   z);
    nb.order1[2]  = _volume(x,                   mirrorIdx(y - 1, Y), z);
    nb.order1[3]  = _volume(x,                   mirrorIdx(y + 1, Y), z);
    nb.order1[4]  = _volume(x,                   y,                   mirrorIdx(z - 1, Z));
    nb.order1[5]  = _volume(x,                   y,                   mirrorIdx(z + 1, Z));

    nb.order2[0]  = _volume(x,                   mirrorIdx(y - 1, Y), mirrorIdx(z - 1, Z));
    nb.order2[1]  = _volume(x,                   mirrorIdx(y - 1, Y), mirrorIdx(z + 1, Z));
    nb.order2[2]  = _volume(x,                   mirrorIdx(y + 1, Y), mirrorIdx(z - 1, Z));
    nb.order2[3]  = _volume(x,                   mirrorIdx(y + 1, Y), mirrorIdx(z + 1, Z));
    nb.order2[4]  = _volume(mirrorIdx(x - 1, X), y,                   mirrorIdx(z - 1, Z));
    nb.order2[5]  = _volume(mirrorIdx(x - 1, X), y,                   mirrorIdx(z + 1, Z));
    nb.order2[6]  = _volume(mirrorIdx(x + 1, X), y,                   mirrorIdx(z - 1, Z));
    nb.order2[7]  = _volume(mirrorIdx(x + 1, X), y,                   mirrorIdx(z + 1, Z));
    nb.order2[8]  = _volume(mirrorIdx(x - 1, X), mirrorIdx(y - 1, Y), z);
    nb.order2[9]  = _volume(mirrorIdx(x - 1, X), mirrorIdx(y + 1, Y), z);
    nb.order2[10] = _volume(mirrorIdx(x + 1, X), mirrorIdx(y - 1, Y), z);
    nb.order2[11] = _volume(mirrorIdx(x + 1, X), mirrorIdx(y + 1, Y), z);

    nb.order3[0]  = _volume(mirrorIdx(x - 1, X), mirrorIdx(y - 1, Y), mirrorIdx(z - 1, Z));
    nb.order3[1]  = _volume(mirrorIdx(x - 1, X), mirrorIdx(y - 1, Y), mirrorIdx(z + 1, Z));
    nb.order3[2]  = _volume(mirrorIdx(x - 1, X), mirrorIdx(y + 1, Y), mirrorIdx(z - 1, Z));
    nb.order3[3]  = _volume(mirrorIdx(x - 1, X), mirrorIdx(y + 1, Y), mirrorIdx(z + 1, Z));
    nb.order3[4]  = _volume(mirrorIdx(x + 1, X), mirrorIdx(y - 1, Y), mirrorIdx(z - 1, Z));
    nb.order3[5]  = _volume(mirrorIdx(x + 1, X), mirrorIdx(y - 1, Y), mirrorIdx(z + 1, Z));
    nb.order3[6]  = _volume(mirrorIdx(x + 1, X), mirrorIdx(y + 1, Y), mirrorIdx(z - 1, Z));
    nb.order3[7]  = _volume(mirrorIdx(x + 1, X), mirrorIdx(y + 1, Y), mirrorIdx(z + 1, Z));

    return nb;
}

inline std::array<float, 27> NeighborSelector::voxelAndNeighborsMirrored(int x, int y, int z) const
{
    using assist::mirrorIdx;
    std::array<float, 27> nb;
    // the voxel itself
    nb[0]  = _volume(x,                   y,                   z);
    // direct neighbors
    nb[1]  = _volume(mirrorIdx(x - 1, X), y,                   z);
    nb[2]  = _volume(mirrorIdx(x + 1, X), y,                   z);
    nb[3]  = _volume(x,                   mirrorIdx(y - 1, Y), z);
    nb[4]  = _volume(x,                   mirrorIdx(y + 1, Y), z);
    nb[5]  = _volume(x,                   y,                   mirrorIdx(z - 1, Z));
    nb[6]  = _volume(x,                   y,                   mirrorIdx(z + 1, Z));
    // order 2 neighbors (distance sqrt(2))
    nb[7]  = _volume(x,                   mirrorIdx(y - 1, Y), mirrorIdx(z - 1, Z));
    nb[8]  = _volume(x,                   mirrorIdx(y - 1, Y), mirrorIdx(z + 1, Z));
    nb[9]  = _volume(x,                   mirrorIdx(y + 1, Y), mirrorIdx(z - 1, Z));
    nb[10] = _volume(x,                   mirrorIdx(y + 1, Y), mirrorIdx(z + 1, Z));
    nb[11] = _volume(mirrorIdx(x - 1, X), y,                   mirrorIdx(z - 1, Z));
    nb[12] = _volume(mirrorIdx(x - 1, X), y,                   mirrorIdx(z + 1, Z));
    nb[13] = _volume(mirrorIdx(x + 1, X), y,                   mirrorIdx(z - 1, Z));
    nb[14] = _volume(mirrorIdx(x + 1, X), y,                   mirrorIdx(z + 1, Z));
    nb[15] = _volume(mirrorIdx(x - 1, X), mirrorIdx(y - 1, Y), z);
    nb[16] = _volume(mirrorIdx(x - 1, X), mirrorIdx(y + 1, Y), z);
    nb[17] = _volume(mirrorIdx(x + 1, X), mirrorIdx(y - 1, Y), z);
    nb[18] = _volume(mirrorIdx(x + 1, X), mirrorIdx(y + 1, Y), z);
    // order 3 neighbors (distance sqrt(3))
    nb[19] = _volume(mirrorIdx(x - 1, X), mirrorIdx(y - 1, Y), mirrorIdx(z - 1, Z));
    nb[20] = _volume(mirrorIdx(x - 1, X), mirrorIdx(y - 1, Y), mirrorIdx(z + 1, Z));
    nb[21] = _volume(mirrorIdx(x - 1, X), mirrorIdx(y + 1, Y), mirrorIdx(z + 1, Z));
    nb[22] = _volume(mirrorIdx(x - 1, X), mirrorIdx(y + 1, Y), mirrorIdx(z - 1, Z));
    nb[23] = _volume(mirrorIdx(x - 1, X), mirrorIdx(y + 1, Y), mirrorIdx(z + 1, Z));
    nb[24] = _volume(mirrorIdx(x + 1, X), mirrorIdx(y - 1, Y), mirrorIdx(z - 1, Z));
    nb[25] = _volume(mirrorIdx(x + 1, X), mirrorIdx(y - 1, Y), mirrorIdx(z + 1, Z));
    nb[26] = _volume(mirrorIdx(x + 1, X), mirrorIdx(y + 1, Y), mirrorIdx(z + 1, Z));

    return nb;
}

inline std::array<float, 27> NeighborSelector::voxelAndNeighborsUnsafe(int x, int y, int z) const
{
    std::array<float, 27> nb;
    // the voxel itself
    nb[0]  = _volume(x,    y,     z);
    // direct neighbors
    nb[1]  = _volume(x - 1, y,     z);
    nb[2]  = _volume(x + 1, y,     z);
    nb[3]  = _volume(x,     y - 1, z);
    nb[4]  = _volume(x,     y + 1, z);
    nb[5]  = _volume(x,     y,     z - 1);
    nb[6]  = _volume(x,     y,     z + 1);
    // order 2 neighbors (distance sqrt(2))
    nb[7]  = _volume(x,     y - 1, z - 1);
    nb[8]  = _volume(x,     y - 1, z + 1);
    nb[9]  = _volume(x,     y + 1, z - 1);
    nb[10] = _volume(x,     y + 1, z + 1);
    nb[11] = _volume(x - 1, y    , z - 1);
    nb[12] = _volume(x - 1, y    , z + 1);
    nb[13] = _volume(x + 1, y    , z - 1);
    nb[14] = _volume(x + 1, y    , z + 1);
    nb[15] = _volume(x - 1, y - 1, z);
    nb[16] = _volume(x - 1, y + 1, z);
    nb[17] = _volume(x + 1, y - 1, z);
    nb[18] = _volume(x + 1, y + 1, z);
    // order 3 neighbors (distance sqrt(3))
    nb[19] = _volume(x - 1, y - 1, z - 1);
    nb[20] = _volume(x - 1, y - 1, z + 1);
    nb[21] = _volume(x - 1, y + 1, z + 1);
    nb[22] = _volume(x - 1, y + 1, z - 1);
    nb[23] = _volume(x - 1, y + 1, z + 1);
    nb[24] = _volume(x + 1, y - 1, z - 1);
    nb[25] = _volume(x + 1, y - 1, z + 1);
    nb[26] = _volume(x + 1, y + 1, z + 1);

    return nb;
}

namespace assist {
inline int mirrorIdx(int idx, int boundary)
{
    if(idx < 0)
        return std::abs(idx);
    if(uint(idx) >= uint(boundary))  // uint cast to suppress strict-overflow warning
        return 2*(boundary-1) - idx;

    return idx;
}
} // namespace assist

} // namespace CTL

#endif // CTL_NEIGHBORSELECTOR_H
