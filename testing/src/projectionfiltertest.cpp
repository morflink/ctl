#include "projectionfiltertest.h"
#include "processing/oclprojectionfilters.h"
#include "acquisition/systemblueprints.h"
#include "acquisition/trajectories.h"

#include <random>

using namespace CTL;

ProjectionFilterTest::ProjectionFilterTest()
    : _dummyData(100, 100, 1)
{
}

void ProjectionFilterTest::initTestCase()
{
    _dummyData.allocateMemory(10);
    std::mt19937 rng;
    rng.seed(1234);
    std::uniform_real_distribution<float> dist(0.0f, 2.0f);
    std::generate(_dummyData.begin(), _dummyData.end(), [&dist, &rng](){ return dist(rng); });

    _dummySetup = AcquisitionSetup(makeCTSystem<blueprints::GenericCarmCT>(),10);
    _dummySetup.applyPreparationProtocol(protocols::ShortScanTrajectory(750));
}

void ProjectionFilterTest::testSerialization()
{
    static const double eps = 1e-3;

    {
        qInfo() << "Test OCL::CosineWeighting";
        OCL::CosineWeighting filter;
        QVERIFY(runSerializationCheckWithSetup(filter) < eps);
    }
    {
        qInfo() << "Test OCL::ParkerWeightingRev";
        OCL::ParkerWeightingRev filter;
        QVERIFY(runSerializationCheckWithSetup(filter) < eps);
    }
    {
        qInfo() << "Test OCL::ApodizationFilter (RamLak)";
        OCL::ApodizationFilter filter(OCL::ApodizationFilter::RamLak);
        QVERIFY(runSerializationCheck(filter) < eps);
    }
    {
        qInfo() << "Test OCL::ApodizationFilter (Hann)";
        OCL::ApodizationFilter filter(OCL::ApodizationFilter::Hann);
        QVERIFY(runSerializationCheck(filter) < eps);
    }
    {
        qInfo() << "Test OCL::RamLakFilter";
        OCL::RamLakFilter filter(1.337f);
        QVERIFY(runSerializationCheck(filter) < eps);
    }
}
