#ifndef PROJECTIONFILTERTEST_H
#define PROJECTIONFILTERTEST_H

#include "img/projectiondata.h"
#include "io/jsonserializer.h"
#include "processing/errormetrics.h"

#include <QtTest>

class ProjectionFilterTest : public QObject
{
    Q_OBJECT
public:
    ProjectionFilterTest();

private Q_SLOTS:
    void initTestCase();
    void testSerialization();

private:
    CTL::ProjectionData _dummyData;
    CTL::AcquisitionSetup _dummySetup;

    template <class Filter>
    double runSerializationCheck(Filter& filt);
    template <class Filter>
    double runSerializationCheckWithSetup(Filter& filt);
};

template <class Filter>
double ProjectionFilterTest::runSerializationCheck(Filter& filt)
{
    auto projCpy1 = _dummyData;
    filt.filter(projCpy1);

    CTL::JsonSerializer ser;
    ser.serialize(filt, "regul.json");

    auto obj = ser.deserialize<Filter>("regul.json");

    auto projCpy2 = _dummyData;
    obj->filter(projCpy2);

    return CTL::metric::L2(projCpy1.cbegin(), projCpy1.cend(), projCpy2.cbegin());
}

template <class Filter>
double ProjectionFilterTest::runSerializationCheckWithSetup(Filter& filt)
{
    auto projCpy1 = _dummyData;
    filt.filter(projCpy1, _dummySetup);

    CTL::JsonSerializer ser;
    ser.serialize(filt, "regul.json");

    auto obj = ser.deserialize<Filter>("regul.json");

    auto projCpy2 = _dummyData;
    obj->filter(projCpy2, _dummySetup);

    return CTL::metric::L2(projCpy1.cbegin(), projCpy1.cend(), projCpy2.cbegin());
}

#endif // PROJECTIONFILTERTEST_H
