#ifndef CTL_XRAYSPECTRUMMODELS_H
#define CTL_XRAYSPECTRUMMODELS_H

#include "abstractxrayspectrummodel.h"
#include "tabulateddatamodel.h"

namespace CTL {

/*!
 * \brief The XraySpectrumTabulatedModel class provides an implementation of an X-ray spectrum model
 * based on tabulated data.
 *
 * This class implements AbstractXraySpectrumModel with a backend of several TabulatedDataModel
 * lookup tables. Each of these tables corresponds to a particular energy of operation (e.g. tube
 * voltage). Lookup tables can be added through addLookupTable(). If an energy parameter is set
 * for which no exact match exists in the lookup tables, interpolation between the tables closest
 * to the requested energy parameter is performed.
 *
 * Note that for introducing a fixed spectrum to the simulation (e.g. some spectrum pre-generated
 * by an external tool), the class FixedXraySpectrumModel might be a more suitable choice.
 *
 * Example: creating an XraySpectrumTabulatedModel with two lookup tables for spectra at 40 kV
 * and 70 kV, and setting it to provide (interpolated) data for a 55 kV setting.
 * \code
 *  // we first create a TabulatedDataModel to hold our spectrum at a 40 kV setting
 *  auto table_40kV = std::make_shared<TabulatedDataModel>();
 *  table_40kV->setName("40 kV Table");         // optional name for visualization
 *
 *  // now, we fill in sampling points of the actual spectrum
 *  table_40kV->insertDataPoint(0.0f, 0.0f);
 *  table_40kV->insertDataPoint(10.0f, 15.0f);
 *  table_40kV->insertDataPoint(20.0f, 4.0f);
 *  table_40kV->insertDataPoint(30.0f, 1.5f);
 *  table_40kV->insertDataPoint(40.0f, 0.0f);
 *
 *
 *  // we create another TabulatedDataModel to hold a spectrum at a 70 kV setting
 *  auto table_70kV = std::make_shared<TabulatedDataModel>();
 *  table_70kV->setName("70 kV Table");         // optional name for visualization
 *
 *  // now, we fill in sampling points of the actual spectrum
 *  table_70kV->insertDataPoint(0.0f, 0.0f);
 *  table_70kV->insertDataPoint(10.0f, 10.0f);
 *  table_70kV->insertDataPoint(20.0f, 9.0f);
 *  table_70kV->insertDataPoint(30.0f, 6.0f);
 *  table_70kV->insertDataPoint(40.0f, 5.0f);
 *  table_70kV->insertDataPoint(50.0f, 3.0f);
 *  table_70kV->insertDataPoint(60.0f, 1.0f);
 *  table_70kV->insertDataPoint(70.0f, 0.0f);
 *
 *
 *  // we now create our XraySpectrumTabulatedModel...
 *  auto tabulatedSpectrum = std::make_shared<XraySpectrumTabulatedModel>();
 *
 *  // ... and add both lookup tables at their corresponding energies (i.e. 40 and 70 kV).
 *  tabulatedSpectrum->addLookupTable(40.0f, *table_40kV);
 *  tabulatedSpectrum->addLookupTable(70.0f, *table_70kV);
 *
 *  // we finally set the energy parameter of the XraySpectrumTabulatedModel to 55 kV
 *  // this means that the model will try to provide a spectrum for a 55 kV setting
 *  // (using interpolation between the two lookup tables)
 *  tabulatedSpectrum->setParameter(55.0f);
 *
 *  // we could now sample values from the model; here we choose to visualize the model
 *  // Note: Visualization requires 'gui_widgets_charts.pri' module
 *  gui::plot(table_40kV);
 *  gui::plot(table_70kV);
 *  gui::plot(tabulatedSpectrum);
 * \endcode
 *
 * ![Visualization from the example. (left) Tabulated model for 40 kV, (center) Tabulated model for 70 kV, (left) XrayTabulatedDataModel containing both lookup tables (i.e. at 40 and 70 kV) and sampled at energy parameter 55 kV.](XrayTabulatedDataModel.png)
 */
class XraySpectrumTabulatedModel : public AbstractXraySpectrumModel
{
    CTL_TYPE_ID(35)

    // abstract interfaces
    public: float valueAt(float position) const override;
    public: float binIntegral(float position, float binWidth) const override;
    public: AbstractIntegrableDataModel* clone() const override;

public:
    XraySpectrumTabulatedModel();
    void addLookupTable(float voltage, const TabulatedDataModel& table);
    void setLookupTables(const QMap<float, TabulatedDataModel>& tables);

    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

    bool hasTabulatedDataFor(float voltage) const;

protected:
    QMap<float, TabulatedDataModel> _lookupTables; //!< The spectra lookup tables (key = energy).

};

/*!
 * \brief The XrayLaserSpectrumModel class is a data model to represent monoenergetic X-ray spectra.
 *
 * This data model provides a simple means to represent a pure monoenergetic spectrum, i.e. with a
 * contribution only at one single well-defined photon energy.
 *
 * To enable this particular behavior, it implements AbstractXraySpectrumModel in a way that its
 * binIntegral() method returns 1.0 only if the bin contains the current energy parameter of this
 * instance. All other bin integrals are always zero.
 * In theory, valueAt() should represent a Dirac delta distrubution here; however, for simplicity
 * it returns 1.0 at the position that is identical to the current energy parameter and zero
 * elsewhere.
 *
 * This spectrum model is used by the XrayLaser component class.
 *
 *
 * Example:
 * \code
 *  XrayLaserSpectrumModel model;
 *  model.setParameter(42.0f);
 *
 *  qInfo() << model.valueAt(41.0f);            // output: 0
 *  qInfo() << model.valueAt(42.0f);            // output: 1
 *
 *  qInfo() << model.binIntegral(42.0f, 1.0f);  // output: 1
 *  qInfo() << model.binIntegral(40.0f, 1.0f);  // output: 0
 *
 *  // change energy parameter to 40 keV
 *  model.setParameter(40.0f);
 *
 *  qInfo() << model.valueAt(41.0f);            // output: 0
 *  qInfo() << model.valueAt(42.0f);            // output: 0
 *
 *  qInfo() << model.binIntegral(42.0f, 1.0f);  // output: 0
 *  qInfo() << model.binIntegral(40.0f, 1.0f);  // output: 1
 * \endcode
 */
class XrayLaserSpectrumModel : public AbstractXraySpectrumModel
{
    CTL_TYPE_ID(40)

    // abstract interfaces
    public: float valueAt(float position) const override;
    public: float binIntegral(float position, float binWidth) const override;
    public: AbstractIntegrableDataModel* clone() const override;

public:
    XrayLaserSpectrumModel();
    explicit XrayLaserSpectrumModel(float energy);
};

/*!
 * \brief The FixedXraySpectrumModel class is a data model that uses a single well-defined lookup
 * table to sample its values, independent of the energy parameter that has been set to it.
 *
 * The main use case of this model is introducing a fixed spectrum to the simulation, such as some
 * spectrum pre-generated by an external tool, actual measurement data, or simply to make sure to
 * exactly reproduce some spectrum from another source component without the need to consider its
 * specific parameters.
 *
 * The lookup table data can be set during construction, using setLookupTable(), or by direct
 * manipulation through lookupTable().
 *
 * Example 1: populate a TabulatedDataModel with data and use it as lookup table for a
 * FixedXraySpectrumModel
 * \code
 *  auto table = TabulatedDataModel();
 *  table.insertDataPoint(5.0f, 1.0f);
 *  table.insertDataPoint(10.0f, 3.0f);
 *  table.insertDataPoint(15.0f, 10.0f);
 *  table.insertDataPoint(20.0f, 7.7f);
 *
 *  auto model = FixedXraySpectrumModel(table);
 *
 *  // get individual values (lin. interpolation)
 *  qInfo() << model.valueAt(7.5f);             // output: 2
 *  qInfo() << model.valueAt(14.5f);            // output: 9.3
 *
 *  // get bin integrals (trapezoidal rule)
 *  qInfo() << model.binIntegral(7.5f, 5.0f);   // output: 10
 *  qInfo() << model.binIntegral(10.0f, 20.0f); // output: 89.25
 *
 *  //trying to change energy of 'table'
 *  model.setParameter(13.37f);
 *  // output: "Warning: FixedXraySpectrumModel::setParameter(): Setting energy parameter is not
 *  //          supported in FixedXraySpectrumModel. This call is ignored!"
 *
 *  // output is unchanged:
 *  qInfo() << model.valueAt(7.5f);             // output: 2
 *  qInfo() << model.binIntegral(7.5f, 5.0f);   // output: 10
 * \endcode
 *
 * Example 2: sample a spectrum from an XrayTube instance and use these values as a lookup table for
 * a FixedXraySpectrumModel
 * \code
 *  // create an XrayTube object, set the tube voltage to 115 kV and sample its spectrum
 *  XrayTube tube;
 *  tube.setTubeVoltage(115.0f);
 *  const auto sampledSpectrum = tube.spectrum(10);     // sample 10 values
 *
 *  // create a TabulatedDataModel with the points of the sampled spectrum
 *  const auto table = TabulatedDataModel(sampledSpectrum);
 *
 *  // create our FixedXraySpectrumModel and directly set its lookup table to 'table'
 *  auto model = FixedXraySpectrumModel(table);
 *
 *  // get some values
 *  qInfo() << model.valueAt(10.0f);    // output: 0.0176055
 *  qInfo() << model.valueAt(20.0f);    // output: 0.0824766
 *  qInfo() << model.valueAt(40.0f);    // output: 0.218841
 *  qInfo() << model.valueAt(90.0f);    // output: 0.0414241
 *
 *  // alternatively, visualize the model (requires 'gui_widgets_charts.pri' module)
 *  gui::plot(model);
 * \endcode
 */
class FixedXraySpectrumModel : public AbstractXraySpectrumModel
{
    CTL_TYPE_ID(36)

    // abstract interfaces
    public: float valueAt(float position) const override;
    public: float binIntegral(float position, float binWidth) const override;
    public: AbstractIntegrableDataModel* clone() const override;

public:
    FixedXraySpectrumModel();
    explicit FixedXraySpectrumModel(const TabulatedDataModel& table);

    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

    // getter
    TabulatedDataModel& lookupTable();
    const TabulatedDataModel& lookupTable() const;

    // setter
    void setLookupTable(const TabulatedDataModel& table);

protected:
    TabulatedDataModel _lookupTable; //!< The spectrum lookup table

};

/*!
 * \brief The KramersLawSpectrumModel class is a data model to represent an (idealized) X-ray
 * spectrum with respect to Kramers' law.
 *
 * For details on the model, see valueAt() and binIntegral().
 *
 * Example:
 * \code
 * KramersLawSpectrumModel model;
 * model.setParameter(75.0f);
 *
 * // sample the model in the interval [0, 100] keV and visualize the model
 * // Note: visualization requires the 'gui_widgets_charts.pri' submodule
 * gui::plot(XYDataSeries::sampledFromModel(model, 0.0f, 100.0f, 100),
 *           "Energy [keV]", "dN(E)/dE");
 * \endcode
 *
 * ![Visualization of a KramersLawSpectrumModel at energy parameter 75 keV.](kramerslaw.png)
 */
class KramersLawSpectrumModel : public AbstractXraySpectrumModel
{
    CTL_TYPE_ID(41)

    // abstract interfaces
    public: float valueAt(float position) const override;
    public: float binIntegral(float position, float binWidth) const override;
    public: AbstractIntegrableDataModel* clone() const override;

public:
    KramersLawSpectrumModel();
};

/*!
 * \brief The HeuristicCubicSpectrumModel class is a data model to represent an (heuristic) X-ray
 * spectrum with respect to a simple degree-three polynomial.
 *
 * This class uses a simple cubic polynomial to (roughly) approximate the shape of a real X-ray
 * spectrum. Different from KramersLawSpectrumModel, this also includes the effect that the number
 * of emitted photons drops towards low energies.
 *
 * For details on the model, see valueAt() and binIntegral().
 *
 * Example:
 * \code
 * HeuristicCubicSpectrumModel model;
 * model.setParameter(95.0f);
 *
 * // sample the model in the interval [0, 100] keV and visualize the model
 * // Note: visualization requires the 'gui_widgets_charts.pri' submodule
 * gui::plot(XYDataSeries::sampledFromModel(model, 0.0f, 100.0f, 100),
 *           "Energy [keV]", "dN(E)/dE");
 * \endcode
 *
 * ![Visualization of a HeuristicCubicSpectrumModel at energy parameter 95 keV.](heuristiccubicmodel.png)
 */
class HeuristicCubicSpectrumModel : public AbstractXraySpectrumModel
{
    CTL_TYPE_ID(42)

    // abstract interfaces
    public: float valueAt(float position) const override;
    public: float binIntegral(float position, float binWidth) const override;
    public: AbstractIntegrableDataModel* clone() const override;

public:
    HeuristicCubicSpectrumModel();
};

/*!
 * \brief The TASMIPSpectrumModel class is a data model to represent a realistic X-ray spectrum
 * based on a set of coefficients from the so-called TASMIP model.
 *
 * Note that the underlying TASMIP model data is valid only for tube voltages (i.e. the energy
 * parameter of the model) below 140 kV.
 *
 * This spectrum model is used by the XrayTube component class.
 *
 * *Reference for the TASMIP model:*
 *
 *     Author(s):     John Boone & J. Anthony Seibert
 *
 *     Article Title: An accurate method for computer-generating tungsten anode x-ray spectra from 30 to 140 kV
 *
 *     Journal:       Medial Physics   E-MPHYA-24-1661
 *     Issue Date:    November 1997    Volume: 24  Issue No.: 11
 *     https://doi.org/10.1118/1.597953
 *
 *
 * Example:
 * \code
 * TASMIPSpectrumModel model;
 * model.setParameter(100.0f);
 *
 * // sample the model in the interval [0, 120] keV and visualize the model
 * // Note: visualization requires the 'gui_widgets_charts.pri' submodule
 * gui::plot(XYDataSeries::sampledFromModel(model, 0.0f, 120.0f, 100),
 *           "Energy [keV]", "dN(E)/dE");
 *
 *
 * // setting an energy parameter outside the valid range
 * model.setParameter(175.0f);
 * // output: Warning: "Trying to set energy parameter to 175. TASMIP data is only available up to 140 kV."
 *
 * // we can continue to use the model, but the resulting data is not physically meaningful
 * gui::plot(XYDataSeries::sampledFromModel(model, 0.0f, 200.0f, 100),
 *           "Energy [keV]", "dN(E)/dE");
 * \endcode
 *
 * ![Visualization of a TASMIPSpectrumModel at (a) energy parameter 100 keV and (b) 175 keV, which is outside the valid range (until 140 keV).](tasmipmodel.png)
 */
class TASMIPSpectrumModel : public FixedXraySpectrumModel
{
    CTL_TYPE_ID(43)

    // abstract interfaces
    public: AbstractIntegrableDataModel* clone() const override;

public:
    explicit TASMIPSpectrumModel(float tubeVoltage = 100.0f);

    void setParameter(const QVariant& parameter) override;

private:
    TabulatedDataModel TASMIPtable(float tubeVoltage);
    using FixedXraySpectrumModel::setLookupTable;
};

} // namespace CTL

#endif // CTL_XRAYSPECTRUMMODELS_H
