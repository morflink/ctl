#ifndef CTL_GRANGEATREGISTRATION2D3D_H
#define CTL_GRANGEATREGISTRATION2D3D_H

#include "abstractregistration2d3d.h"
#include "processing/errormetrics.h"
#include <nlopt.hpp>

namespace CTL {

namespace OCL {
class IntermedGen2D3D;
} // namespace OCL

namespace NLOPT {

/*!
 * \class GrangeatRegistration2D3D
 *
 * \brief Grangeat-based 2D/3D registration using NLopt for optimization
 */

class GrangeatRegistration2D3D : public AbstractNloptRegistration2D3D
{
public:
    mat::Homography3D optimize(const Chunk2D<float>& projectionImage,
                               const OCL::VolumeResampler& volumeIntermedResampler,
                               const mat::ProjectionMatrix& pMat) override;

    mat::Homography3D optimize(const std::vector<Chunk2D<float>>& projectionImages,
                               const OCL::VolumeResampler& volumeIntermedResampler,
                               const std::vector<mat::ProjectionMatrix>& pMats) override;

    const imgproc::AbstractErrorMetric* metric() const override;
    void setMetric(const imgproc::AbstractErrorMetric* metric) override;

    float subSamplingLevel() const override;
    void setSubSamplingLevel(float subSamplingLevel) override;

    bool truncationSieveEnabled() const override;
    float truncationThreshold() const override;
    void setTruncationSieve(float extinctionThreshold, bool enabled) override;

    nlopt::opt& optObject() override;

    float maxLineDistance() const;
    void setMaxLineDistance(float maxLineDistance);

private:
    nlopt::opt _opt{ nlopt::algorithm::LN_SBPLX, 6u };
    const imgproc::AbstractErrorMetric* _metric = &metric::L2;
    float _maxLineDistance = 1.0f;
    float _subSamplingLevel = 1.0f;
    float _truncationThreshold = 0.1f;
    bool _useTruncationSieve = false;

    OCL::IntermedGen2D3D buildIntermedGenerator(float deltaS) const;
    mat::Homography3D performOptimization();
};

} // namespace NLOPT
} // namespace CTL

#endif // CTL_GRANGEATREGISTRATION2D3D_H
