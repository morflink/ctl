#include "consistency.h"
#include "ocl/clfileloader.h"
#include "processing/radontransform2d.h"
#include "processing/radontransform3d.h"
#include "processing/imageprocessing.h"
#include "mat/projectionmatrix.h"
#include "mat/matrix.h"
#include "mat/mat.h"

#include <QDebug>
#include <bitset>
#include <random>

const std::string CL_KERNEL_HOM2RADON = "homToRadon"; //!< name of the OpenCL kernel function
const std::string CL_KERNEL_RADON2HOM = "radonToHom"; //!< name of the OpenCL kernel function

namespace {

std::vector<bool> findTrucatedLines(const std::vector<CTL::Radon2DCoord>& radon2DCoords,
                                    const CTL::Chunk2D<float>& proj,
                                    float truncationThreshold);

bool isTruncatedLine(const CTL::Radon2DCoord& radonCoords,
                     const CTL::Chunk2D<float>& proj,
                     float truncationThreshold);

CTL::mat::Matrix<2, 1> defaultOriginRadon2D(CTL::Chunk2D<float>::Dimensions projSize);

template <typename T>
std::vector<T> selectUntruncatedValues(std::vector<T>&& values,
                                       const std::vector<bool>& truncatedIndices);

template <class T>
std::vector<T> randomSubset(std::vector<T>&& fullSamples, uint seed, float subsampleLevel);

} // unnamed namespace

namespace CTL {

// #### IntermediateFctPair ####
// -----------------------------

IntermediateFctPair::IntermediateFctPair(std::vector<float> first, std::vector<float> second,
                                         Type secondType)
    : _first(first.size() == second.size()
             ? std::make_shared<const std::vector<float>>(std::move(first))
             : std::make_shared<const std::vector<float>>())
    , _second(_first->size() == second.size()
              ? std::make_shared<const std::vector<float>>(std::move(second))
              : std::make_shared<const std::vector<float>>())
    , _secondType(secondType)
{
}

IntermediateFctPair::IntermediateFctPair(std::shared_ptr<const std::vector<float>> first,
                                         std::vector<float> second, Type secondType)
    : _first(first->size() == second.size()
             ? std::move(first)
             : std::make_shared<const std::vector<float>>())
    , _second(_first->size() == second.size()
              ? std::make_shared<const std::vector<float>>(std::move(second))
              : std::make_shared<const std::vector<float>>())
    , _secondType(secondType)
{
}

IntermediateFctPair::IntermediateFctPair(std::vector<float> first,
                                         std::shared_ptr<const std::vector<float>> second,
                                         Type secondType)
    : _first(first.size() == second->size()
             ? std::make_shared<const std::vector<float>>(std::move(first))
             : std::make_shared<const std::vector<float>>())
    , _second(_first->size() == second->size()
              ? std::move(second)
              : std::make_shared<const std::vector<float>>())
    , _secondType(secondType)
{
}

IntermediateFctPair::IntermediateFctPair(std::shared_ptr<const std::vector<float>> first,
                                         std::shared_ptr<const std::vector<float>> second,
                                         Type secondType)
    : _first(first->size() == second->size()
             ? std::move(first)
             : std::make_shared<const std::vector<float>>())
    , _second(_first->size() == second->size()
              ? std::move(second)
              : std::make_shared<const std::vector<float>>())
    , _secondType(secondType)
{
}

double IntermediateFctPair::inconsistency(const imgproc::AbstractErrorMetric& metric, bool swapInput) const
{
    Q_ASSERT(!isEmpty());
    return swapInput ? metric(*_second, *_first) : metric(*_first, *_second);
}

bool IntermediateFctPair::isEmpty() const
{
    return _first->empty();
}

const std::vector<float>& IntermediateFctPair::first() const { return *_first; }

const std::vector<float>& IntermediateFctPair::second() const { return *_second; }

const std::shared_ptr<const std::vector<float>>& IntermediateFctPair::ptrToFirst() const
{
    return _first;
}

const std::shared_ptr<const std::vector<float>>& IntermediateFctPair::ptrToSecond() const
{
    return _second;
}

namespace OCL {

// #### IntermedGen2D2D ####
// -------------------------

double IntermedGen2D2D::angleIncrement() const
{
    return _angleIncrement;
}

float IntermedGen2D2D::subsampleLevel() const
{
    return _subsampleLevel;
}

void IntermedGen2D2D::setAngleIncrement(double angleIncrement)
{
    _angleIncrement = angleIncrement;
}

void IntermedGen2D2D::setSubsampleLevel(float subsampleLevel)
{
    if(subsampleLevel <= 0.0f)
        qWarning("New subsampling level ignored, since it is negative or zero.");
    else if(subsampleLevel > 1.0f)
        qWarning("New subsampling level ignored, since it is greater than one.");
    else
    {
        _subsampleLevel = subsampleLevel;
        _useSubsampling = true;
    }
}

void IntermedGen2D2D::setTruncationThreshold(float extinction, bool enabled)
{
    _truncationThreshold = extinction;
    _useTruncationSieve = enabled;
}

void IntermedGen2D2D::toggleSubsampling(bool enabled)
{
    _useSubsampling = enabled;
}

void IntermedGen2D2D::toggleTruncationSieve(bool enabled)
{
    _useTruncationSieve = enabled;
}

IntermediateFctPair IntermedGen2D2D::intermedFctPair(const Chunk2D<float>& proj1,
                                                     const mat::ProjectionMatrix& P1,
                                                     const Chunk2D<float>& proj2,
                                                     const mat::ProjectionMatrix& P2,
                                                     float plusMinusH) const
{
    if(proj1.dimensions() != proj2.dimensions())
        throw std::runtime_error("IntermedGen2D2D::intermedFctPair: size of projections must match.");

    auto radon2DCoords = linePairs(P1, P2, proj1.dimensions());

    if(_useSubsampling)
        radon2DCoords = selectRandomSubset(std::move(radon2DCoords));

    if(_useTruncationSieve)
        radon2DCoords = selectUntruncatedLinePairs(std::move(radon2DCoords), proj1, proj2);

    const IntermediateProj intermedFct1(proj1, P1.intrinsicMatK());
    const IntermediateProj intermedFct2(proj2, P2.intrinsicMatK());

    return { intermedFct1.sampled(radon2DCoords.first, plusMinusH),
             intermedFct2.sampled(radon2DCoords.second, plusMinusH),
             IntermediateFctPair::ProjectionDomain };
}

IntermediateFctPair IntermedGen2D2D::intermedFctPair(const ImageResampler& radon2dSampler1,
                                                     const mat::ProjectionMatrix& P1,
                                                     const ImageResampler& radon2dSampler2,
                                                     const mat::ProjectionMatrix& P2,
                                                     const Chunk2D<float>::Dimensions& projSize) const
{
    if(radon2dSampler1.imgDim() != radon2dSampler2.imgDim())
        throw std::runtime_error("IntermedGen2D2D::intermedFctPair: size of projections must match.");

    auto radon2DCoords = linePairs(P1, P2, projSize);

    if(_useSubsampling)
        radon2DCoords = selectRandomSubset(std::move(radon2DCoords));

    if(_useTruncationSieve)
        radon2DCoords = selectUntruncatedLinePairs(std::move(radon2DCoords),
                                                   radon2dSampler1.image(),
                                                   radon2dSampler2.image());

    return { radon2dSampler1.sample(toGeneric2DCoord(radon2DCoords.first)),
             radon2dSampler2.sample(toGeneric2DCoord(radon2DCoords.second)),
             IntermediateFctPair::ProjectionDomain };
}

std::pair<IntermedGen2D2D::LineSet, IntermedGen2D2D::LineSet>
IntermedGen2D2D::linePairs(const mat::ProjectionMatrix& P1,
                           const mat::ProjectionMatrix& P2,
                           const Chunk2D<float>::Dimensions& projSize,
                           const mat::Matrix<2, 1>& originRadon,
                           float maxLineDistance,
                           double angleIncrement)
{
    static constexpr auto nbCorners = 4u;
    const std::array<mat::Matrix<2, 1>, nbCorners> detectorCorners = {
        mat::Matrix<2, 1>{ 0.0, 0.0 },
        mat::Matrix<2, 1>{ projSize.width - 1.0, 0.0 },
        mat::Matrix<2, 1>{ 0.0, projSize.height - 1.0 },
        mat::Matrix<2, 1>{ projSize.width - 1.0, projSize.height - 1.0 }
    };
    auto intersectsDetector = [&](const Radon2DCoord& line)
    {
        const mat::Matrix<1, 2> n2D = { std::cos(line.angle()), std::sin(line.angle()) };
        std::bitset<nbCorners> side;
        for(auto i = 0u; i < nbCorners; ++i)
            side.set(i, std::signbit(n2D * (detectorCorners[i] - originRadon) - line.dist()));
        return !(side.all() || side.none());
    };
    const auto imgDiag = float(mat::Matrix<2, 1>(projSize.width, projSize.height).norm());
    const auto maxS = maxLineDistance * 0.5f * imgDiag;

    const auto source1 = P1.sourcePosition();
    const auto source2 = P2.sourcePosition();
    const auto M1 = P1.M(), M1transp = P1.M().transposed();
    const auto M2 = P2.M(), M2transp = P2.M().transposed();

    // vector parallel to the connection between the sources (baseline)
    auto baseLine = source2 - source1;
    const auto src2srcDistance = baseLine.norm();
    if(qFuzzyIsNull(src2srcDistance))
        throw std::runtime_error("IntermedGen2D2D::intermedFctPair: distance between the two source"
                                 " positions is close to zero.");
    baseLine /= src2srcDistance;

    // initialize plane normal
    const auto initNormal = orthonormalTo(baseLine);

    // rotate plane around baseline
    const auto nbRotAngles = size_t(PI / std::abs(angleIncrement) + 0.5);
    const auto negSource1 = -source1.transposed();
    const auto originRadonTransp = originRadon.transposed();
    LineSet lineSet1, lineSet2;

    for(size_t i = 0; i < nbRotAngles; ++i)
    {
        // plane `p` in homogeneous coordinates [nx, ny, nz, -d]
        const auto rotAngle = double(i) * angleIncrement;
        const auto n = mat::rotationMatrix(rotAngle, baseLine) * initNormal;
        const auto p = mat::vertcat(n, negSource1 * n);

        // Pluecker matrix of the intersection line of `p` with the plane at infinity
        const mat::Matrix<3, 3> L{  0.0,  p(2), -p(1),
                                  -p(2),   0.0,  p(0),
                                   p(1), -p(0),   0.0 };

        // transform Pluecker line representation to local CT frames
        const auto L1 = M1 * L * M1transp;
        const auto L2 = M2 * L * M2transp;

        // 2D radon parameters of the lines
        const auto line1 = plueckerTo2DRadon(L1, originRadonTransp);
        const auto line2 = plueckerTo2DRadon(L2, originRadonTransp);

        // boundary checks and append coordinates
        if(intersectsDetector(line1) && intersectsDetector(line2))
        {
            if(std::abs(line1.dist()) <= maxS && std::abs(line2.dist()) <= maxS)
            {
                lineSet1.push_back(line1);
                lineSet2.push_back(line2);
            }
        }
    }

    return std::make_pair(std::move(lineSet1), std::move(lineSet2));
}

std::pair<IntermedGen2D2D::LineSet, IntermedGen2D2D::LineSet>
IntermedGen2D2D::selectRandomSubset(std::pair<LineSet, LineSet>&& radon2DCoords) const
{
    const uint seed = std::random_device{}(); // pull random seed for subsampling
    radon2DCoords.first = randomSubset(std::move(radon2DCoords.first), seed, _subsampleLevel);
    radon2DCoords.second = randomSubset(std::move(radon2DCoords.second), seed, _subsampleLevel);
    return std::move(radon2DCoords);
}

std::pair<IntermedGen2D2D::LineSet, IntermedGen2D2D::LineSet>
IntermedGen2D2D::selectUntruncatedLinePairs(std::pair<LineSet, LineSet>&& radon2DCoords,
                                          const Chunk2D<float>& proj1,
                                          const Chunk2D<float>& proj2) const
{
    const auto pairsWithDetectedTruncation = [&]()
    {
        const auto nbPairs = radon2DCoords.first.size();
        auto ret = std::vector<bool>(nbPairs, false);

        for(auto pairNb = 0u; pairNb < nbPairs; ++pairNb)
            if(   isTruncatedLine(radon2DCoords.first[pairNb], proj1, _truncationThreshold)
               || isTruncatedLine(radon2DCoords.second[pairNb], proj2, _truncationThreshold))
                ret[pairNb] = true; // truncation occured in at least one of the two detectors

        return ret;
    }(); // immediately invoked lambda

    radon2DCoords.first = selectUntruncatedValues(std::move(radon2DCoords.first),
                                                  pairsWithDetectedTruncation);
    radon2DCoords.second = selectUntruncatedValues(std::move(radon2DCoords.second),
                                                   pairsWithDetectedTruncation);

    return std::move(radon2DCoords);
}

std::pair<IntermedGen2D2D::LineSet, IntermedGen2D2D::LineSet>
IntermedGen2D2D::linePairs(const mat::ProjectionMatrix& P1, const mat::ProjectionMatrix& P2,
                           const Chunk2D<float>::Dimensions& projSize) const
{
    return linePairs(P1, P2, projSize, defaultOriginRadon2D(projSize),
                     _maxLineDistance, _angleIncrement);
}

float IntermedGen2D2D::maxLineDistance() const
{
    return _maxLineDistance;
}

void IntermedGen2D2D::setMaxLineDistance(float newMaxLineDistance)
{
    _maxLineDistance = newMaxLineDistance;
}

Radon2DCoord IntermedGen2D2D::plueckerTo2DRadon(const mat::Matrix<3, 3>& L,
                                                const mat::Matrix<1, 2>& originRadon)
{
    // detector lines `l` in homogeneous coordinates
    mat::Matrix<3, 1> l{ L.get<1, 2>(), L.get<2, 0>(), L.get<0, 1>() };

    // normalize with length of line normal to obtain [nx, ny, -s]
    l /= l.subMat<0, 1>().norm();

    // radon parameter distance `s` and angle `mu`
    const auto mu = std::atan2(l.get<1>(), l.get<0>());
    const auto s = -l.get<2>() - originRadon * l.subMat<0, 1>();

    return { float(mu), float(s) };
}

// #### IntermedGen2D3D ####
// -----------------------------

float IntermedGen2D3D::lineSpacing() const { return _lineSpacing; }

void IntermedGen2D3D::setLineSpacing(float lineSpacing)
{
    if(qFuzzyIsNull(lineSpacing))
        throw std::domain_error("IntermedGen2D3D::setLineDistance: line distance is close to zero");
    if(std::abs(lineSpacing) < 1.0f)
        qWarning("Line spacing below 1 is not meaningful, due to underlying linear interpolation");
    if(lineSpacing < 0.0f)
        qWarning("Negative sign of the line spacing is ignored");

    _lineSpacing = std::abs(lineSpacing);
}

/*!
 * Returns the sampling points in 3D Radon space that were used by the last call of
 * `intermedFctPair`.
 */
const std::vector<Radon3DCoord>& IntermedGen2D3D::lastSampling() const { return _lastSampling; }

IntermediateFctPair IntermedGen2D3D::intermedFctPair(const Chunk2D<float>& proj,
                                                     const mat::ProjectionMatrix& P,
                                                     const VoxelVolume<float>& volume,
                                                     float plusMinusH_mm,
                                                     imgproc::DiffMethod derivativeMethodProj)
{
    // compute intermediate function values from projection
    auto intermedProj = computeIntermedProj(
        proj, P, static_cast<imgproc::FiltMethod>(derivativeMethodProj));

    // compute corresponding 3D Radon coordinates
    _lastSampling = intersectionPlanesWCS(intermedProj.radon2DCoords, P,
                                          defaultOriginRadon2D(proj.dimensions()));

    // compute intermediate function values from volume
    const IntermediateVol intermedFctOfVol(volume);
    auto intermedVol = intermedFctOfVol.sampled(_lastSampling, plusMinusH_mm);

    return IntermediateFctPair(std::move(intermedProj.values),
                               std::move(intermedVol), IntermediateFctPair::VolumeDomain);
}

IntermediateFctPair IntermedGen2D3D::intermedFctPair(const Chunk2D<float>& proj,
                                                     const mat::ProjectionMatrix& P,
                                                     const VolumeResampler& radon3dSampler,
                                                     imgproc::DiffMethod derivativeMethodProj)
{
    return intermedFctPair(proj, P, radon3dSampler,
                           static_cast<imgproc::FiltMethod>(derivativeMethodProj));
}

IntermediateFctPair IntermedGen2D3D::intermedFctPair(const Chunk2D<float>& proj,
                                                     const mat::ProjectionMatrix& P,
                                                     const VolumeResampler& radon3dSampler,
                                                     imgproc::FiltMethod filterMethodProj)
{
    // compute intermediate function values from projection
    auto intermedProj = computeIntermedProj(proj, P, filterMethodProj);

    // compute corresponding 3D Radon coordinates
    _lastSampling = intersectionPlanesWCS(intermedProj.radon2DCoords, P,
                                          defaultOriginRadon2D(proj.dimensions()));

    // sample volume intermediate function values
    auto intermedVol = radon3dSampler.sample(toGeneric3DCoord(_lastSampling));

    return IntermediateFctPair(std::move(intermedProj.values),
                               std::move(intermedVol), IntermediateFctPair::VolumeDomain);
}

IntermediateFctPair IntermedGen2D3D::intermedFctPair(const ImageResampler& radon2dSampler,
                                                     const mat::ProjectionMatrix& P,
                                                     const Chunk2D<float>::Dimensions& projSize,
                                                     const VolumeResampler& radon3dSampler)
{
    // all individual 2D Radon coordinates
    auto radon2DCoords = radon2DSamples(radon2DSampling(projSize));

    // compute intermediate function from volume
    if(_useSubsampling)
    {
        const uint seed = std::random_device{}(); // pull random seed for subsampling
        radon2DCoords = randomSubset(std::move(radon2DCoords), seed, _subsampleLevel);
    }

    if(_useTruncationSieve)
    {
        throw std::runtime_error(
                    "IntermedGen2D3D::intermedFctPair(const ImageResampler& radon2dSampler, ...): "
                    "It is impossible to use the truncation sieve for pre-sampled projection "
                    "intermediate function values.");
    }

    // compute 3D Radon coordinates
    _lastSampling = intersectionPlanesWCS(radon2DCoords, P, defaultOriginRadon2D(projSize));

    auto intermedProj = radon2dSampler.sample(toGeneric2DCoord(radon2DCoords));
    auto intermedVol = radon3dSampler.sample(toGeneric3DCoord(_lastSampling));

    return IntermediateFctPair(std::move(intermedProj),
                               std::move(intermedVol), IntermediateFctPair::VolumeDomain);
}

SamplingRange IntermedGen2D3D::computeSRange(const mat::Matrix<2, 1>& projSize) const
{
    const auto imgDiag = static_cast<float>(projSize.norm());
    const auto maxS = _maxLineDistance * imgDiag;

    return { -0.5f * maxS, 0.5f * maxS };
}

float IntermedGen2D3D::maxLineDistance() const
{
    return _maxLineDistance;
}

void IntermedGen2D3D::setMaxLineDistance(float maxLineDistance)
{
    _maxLineDistance = maxLineDistance;
}

float IntermedGen2D3D::subsampleLevel() const
{
    return _subsampleLevel;
}

void IntermedGen2D3D::setSubsampleLevel(float subsampleLevel)
{
    if(subsampleLevel <= 0.0f)
        qCritical("New subsampling level ignored, since it is negative or zero.");
    else if(subsampleLevel > 1.0f)
        qCritical("New subsampling level ignored, since it is greater than one.");
    else
    {
        _subsampleLevel = subsampleLevel;
        _useSubsampling = true;
    }
}

void IntermedGen2D3D::setTruncationThreshold(float extinction, bool enabled)
{
    _truncationThreshold = extinction;
    _useTruncationSieve = enabled;
}

void IntermedGen2D3D::toggleSubsampling(bool enabled)
{
    _useSubsampling = enabled;
}

void IntermedGen2D3D::toggleTruncationSieve(bool enabled)
{
    _useTruncationSieve = enabled;
}

 /*!
  * Constructs a vector that has a list of `Radon3DCoord` for all 2D Radon coordinates (`mu`, `s`)
  * in \a radon2DCoords of the detector plane. The 3D Radon coordinates for a plane are
  * determined by a projection matrix \a P (plane must contain the source position).
  * The \a origin specifies the placement of the coordinate frame where `mu` and `s` are defined.
  */
std::vector<Radon3DCoord>
IntermedGen2D3D::intersectionPlanesWCS(const std::vector<Radon2DCoord>& radon2DCoords,
                                       const mat::ProjectionMatrix& P,
                                       const mat::Matrix<2, 1>& origin)
{
    std::vector<Radon3DCoord> ret;
    ret.reserve(radon2DCoords.size());

    const auto Mtransp = P.M().transposed();
    const auto srcPosTransp = P.sourcePosition().transposed();
    const auto originTransp = origin.transposed();

    for(const auto& radon2DCoord : radon2DCoords)
    {
        const auto n2D = mat::Matrix<2, 1>{ std::cos(radon2DCoord.angle()),
                                            std::sin(radon2DCoord.angle()) };
        const auto z = radon2DCoord.dist() + originTransp * n2D;

        auto n3D = Mtransp * vertcat(n2D, mat::Matrix<1, 1>{ -z });
        n3D.normalize();

        const auto azimuth = std::atan2(float(n3D.get<1>()), float(n3D.get<0>()));
        const auto polar = std::acos(float(n3D.get<2>()));
        const auto dist = float(srcPosTransp * n3D);

        ret.emplace_back(azimuth, polar, dist);
    }

    return ret;
}

std::vector<Radon2DCoord> IntermedGen2D3D::radon2DSamples(const Radon2DSamplingInfo& radon2DInfo)
{
    const auto muSamples = radon2DInfo.muRange.linspace(radon2DInfo.nbMu);
    const auto sSamples = radon2DInfo.sRange.linspace(radon2DInfo.nbS);

    std::vector<CTL::Radon2DCoord> ret;
    ret.reserve(muSamples.size() * sSamples.size());

    for(auto s : sSamples)
        for(auto mu : muSamples)
            ret.emplace_back(mu, s);

    return ret;
}

IntermedGen2D3D::Radon2DSamplingInfo
IntermedGen2D3D::radon2DSampling(const Chunk2D<float>::Dimensions& projSize) const
{
    const auto sRange = computeSRange({ double(projSize.width), double(projSize.height) });
    const auto muRange = SamplingRange{ float(0.0_deg), float(180.0_deg) };
    const auto nbS = uint(std::ceil(sRange.width() / _lineSpacing));
    const auto nbMu = uint(std::ceil(double(nbS) * PI_2));

    return { sRange, muRange, nbS, nbMu };
}

IntermedGen2D3D::SampledProjectionIntermediateFunction
IntermedGen2D3D::computeIntermedProj(const Chunk2D<float>& proj,
                                     const mat::ProjectionMatrix& P,
                                     imgproc::FiltMethod filterMethodProj) const
{
    const auto radon2DInfo = radon2DSampling(proj.dimensions());

    qDebug("Sample intermediate function with: %i x %i (mu, s) samples.",
           radon2DInfo.nbMu, radon2DInfo.nbS);

    // compute all 2D Radon coordinates
    auto radon2Dcoords = radon2DSamples(radon2DInfo);
    // compute all intermediate function values from projection
    const IntermediateProj intermedFctOfProj{ proj, P.intrinsicMatK() };
    auto intermedProj = intermedFctOfProj
                          .sampled(radon2DInfo.muRange, radon2DInfo.nbMu,
                                   radon2DInfo.sRange, radon2DInfo.nbS, filterMethodProj)
                          .data();

    if(_useSubsampling)
    {
        const uint seed = std::random_device{}(); // pull random seed for subsampling
        radon2Dcoords = randomSubset(std::move(radon2Dcoords), seed, _subsampleLevel);
        intermedProj = randomSubset(std::move(intermedProj), seed, _subsampleLevel);
    }

    if(_useTruncationSieve)
    {
        const auto nbValuesBeforeSieve = radon2Dcoords.size();

        const auto truncatedLines = findTrucatedLines(radon2Dcoords, proj, _truncationThreshold);

        radon2Dcoords = selectUntruncatedValues(std::move(radon2Dcoords), truncatedLines);
        intermedProj = selectUntruncatedValues(std::move(intermedProj), truncatedLines);

        qDebug() << "discard"
                 << 100.0 - 100.0 * double(radon2Dcoords.size()) / double(nbValuesBeforeSieve)
                 << "% of the intermediate function values due to truncation. "
                    "(threshold of truncation sieve: " << _truncationThreshold << ')';
    }

    return { std::move(radon2Dcoords), std::move(intermedProj) };
}

// #### IntermediateProj ####
// --------------------------

IntermediateProj::IntermediateProj(const Chunk2D<float>& proj, const mat::Matrix<3, 3>& K, bool useWeighting)
    : _intrinsicK(K)
    , _useWeighting(useWeighting)
{
    // perform cosine pre-weighting of projections
    if(_useWeighting)
    {
        Chunk2D<float> projCpy(proj);
        imgproc::cosWeighting(projCpy, K);

        // construct Radon transform
        _radon2D.reset(new RadonTransform2D(projCpy));
    }
    else
        _radon2D.reset(new RadonTransform2D(proj));
}

IntermediateProj::IntermediateProj(const Chunk2D<float> &proj) // Aichert approximation (no weighting)
    : IntermediateProj(proj, mat::Matrix<3,3>(), false)
{
}

ImageResampler IntermediateProj::sampler(const SamplingRange& angleRange, uint nbAngles,
                                         const SamplingRange& distRange, uint nbDist,
                                         imgproc::DiffMethod derivativeMethod) const
{
    const auto intermedFct = sampled(angleRange, nbAngles, distRange, nbDist, derivativeMethod);
    // construct and return resampler
    return ImageResampler(intermedFct, angleRange, distRange);
}

ImageResampler IntermediateProj::sampler(const SamplingRange& angleRange, uint nbAngles,
                                         const SamplingRange& distRange, uint nbDist,
                                         imgproc::FiltMethod filterMethod) const
{
    const auto intermedFct = sampled(angleRange, nbAngles, distRange, nbDist, filterMethod);
    // construct and return resampler
    return ImageResampler(intermedFct, angleRange, distRange);
}

Chunk2D<float> IntermediateProj::sampled(const SamplingRange& angleRange, uint nbAngles,
                                         const SamplingRange& distRange, uint nbDist,
                                         imgproc::DiffMethod derivativeMethod) const
{
    return sampled(angleRange, nbAngles, distRange, nbDist,
                   static_cast<imgproc::FiltMethod>(derivativeMethod));
}

Chunk2D<float> IntermediateProj::sampled(const SamplingRange& angleRange, uint nbAngles,
                                         const SamplingRange& distRange, uint nbDist,
                                         imgproc::FiltMethod filterMethod) const
{
    if(nbDist < 2)
        throw std::runtime_error("IntermediateProj::sampler: nbDist must be greater than 1.");

    const auto angleSamples = angleRange.linspace(nbAngles);
    const auto distSamples  = distRange.linspace(nbDist);

    // compute 2D Radon transform
    auto radonTransf = _radon2D->sampleTransform(angleSamples, distSamples);

    // compute filter (or partial derivative) along distance dimension
    imgproc::filter<1>(radonTransf, filterMethod);
    radonTransf /= (distSamples[1] - distSamples[0]);

    // perform post-weighting
    if(_useWeighting)
        postWeighting(radonTransf, angleSamples, distSamples);

    return radonTransf;
}

std::vector<float> IntermediateProj::sampled(const std::vector<Radon2DCoord>& samplingPts,
                                             float plusMinusH) const
{
    // sampling points for central difference
    std::vector<Radon2DCoord> samplingPtsDerivative;
    samplingPtsDerivative.reserve(2 * samplingPts.size());
    for(const auto& centralCoord : samplingPts)
    {
        samplingPtsDerivative.emplace_back(centralCoord.angle(), centralCoord.dist() - plusMinusH);
        samplingPtsDerivative.emplace_back(centralCoord.angle(), centralCoord.dist() + plusMinusH);
    }

    // line integrals
    auto lineIntegrals = _radon2D->sampleTransform(samplingPtsDerivative);
    auto* lineItegralPtr = lineIntegrals.data();

    // derivative
    std::vector<float> ret(samplingPts.size());
    for(auto& val : ret)
    {
        // factor 1/(2h) for central difference
        val = (lineItegralPtr[1] - lineItegralPtr[0]) / (2.0f * plusMinusH);
        lineItegralPtr += 2;
    }

    if(_useWeighting)
        postWeighting(ret, samplingPts);

    return ret;
}

void IntermediateProj::postWeighting(Chunk2D<float>& radonTransDerivative,
                                     const std::vector<float>& theta,
                                     const std::vector<float>& s) const
{
    const auto& K = _intrinsicK;
    const auto p = mat::Matrix<2, 1>{ K.get<0, 2>(), K.get<1, 2>() };
    const auto originShiftTransp = (p - _radon2D->origin()).transposed();
    const auto sSize = s.size();
    const auto tSize = theta.size();

    for(uint tIdx = 0; tIdx < tSize; ++tIdx)
    {
        mat::Matrix<2, 1> n = { std::cos(theta[tIdx]), std::sin(theta[tIdx]) };
        auto sCorr = originShiftTransp * n;

        for(uint sIdx = 0; sIdx < sSize; ++sIdx)
        {
            auto cosPlaneAnlge = cosineOfPlaneAngle((s[sIdx] - sCorr) * n + p, K);
            radonTransDerivative(tIdx, sIdx) /= float(std::pow(cosPlaneAnlge, 2.0));
        }
    }
}

void IntermediateProj::postWeighting(std::vector<float>& lineIntegralDerivative,
                                     const std::vector<Radon2DCoord>& samplingPts) const
{
    const auto& K = _intrinsicK;
    const auto p = mat::Matrix<2, 1>{ K.get<0, 2>(), K.get<1, 2>() };
    const auto originShiftTransp = (p - _radon2D->origin()).transposed();

    std::transform(lineIntegralDerivative.cbegin(), lineIntegralDerivative.cend(), // input 1
                   samplingPts.cbegin(),                                           // input 2
                   lineIntegralDerivative.begin(),                                 // output
                   [&K, &p, &originShiftTransp](float val, const Radon2DCoord& coord)
                   {
                       mat::Matrix<2, 1> n{ std::cos(coord.angle()), std::sin(coord.angle()) };
                       auto sCorr = originShiftTransp * n;
                       auto cosPlaneAnlge = cosineOfPlaneAngle((coord.dist() - sCorr) * n + p, K);
                       return val / float(std::pow(cosPlaneAnlge, 2.0));
                   });
}

double IntermediateProj::cosineOfPlaneAngle(const mat::Matrix<2, 1>& x, const Matrix3x3 K)
{
    // cosine of an angle between the vector pointing from the source to a certain pixel location
    // on the detector and the z-axis (principal ray) of the CT cooradinate frame (CTS):
    // 1. calculate direction d = K^-1 * [x1,x2,1]^t
    // 2. normalize and
    // 3. take 3rd component

    // back substitution to find 'd' in K*d = [x,y,1]^t
    mat::Matrix<3, 1> d;
    d.get<2>() = 1.0;
    d.get<1>() = (x.get<1>() - K.get<1, 2>()) / K.get<1, 1>();
    d.get<0>() = (x.get<0>() - d.get<1>() * K.get<0, 1>() - K.get<0, 2>()) / K.get<0, 0>();

    // cosine to z-axis = <unitDirection, [0 0 1]^t>
    return d.get<2>() / d.norm();
}

void IntermediateProj::setOrigin(float x, float y)
{
    _radon2D->setOrigin(x, y);
}

mat::Matrix<2, 1> IntermediateProj::origin() const
{
    return _radon2D->origin();
}


// #### IntermediateVol ####
// -----------------------------

IntermediateVol::IntermediateVol(const VoxelVolume<float> &vol)
    : _radon3D(vol)
{
}

VolumeResampler IntermediateVol::sampler(const SamplingRange& phiRange, uint nbPhi,
                                              const SamplingRange& thetaRange, uint nbTheta,
                                              const SamplingRange& distRange, uint nbDist,
                                              imgproc::DiffMethod derivativeMethod) const
{
    // compute 3D Radon transform
    auto radonTransf = _radon3D.sampleTransform(phiRange.linspace(nbPhi),
                                                thetaRange.linspace(nbTheta),
                                                distRange.linspace(nbDist));

    // compute (partial) derivative along distance dimension
    imgproc::diff<2>(radonTransf, derivativeMethod);

    // construct and return resampler
    return VolumeResampler(radonTransf, phiRange, thetaRange, distRange);
}

VolumeResampler IntermediateVol::sampler(const SamplingRange& phiRange, uint nbPhi,
                                         const SamplingRange& thetaRange, uint nbTheta,
                                         const SamplingRange& distRange, uint nbDist,
                                         imgproc::FiltMethod filterMethod) const
{
    // compute 3D Radon transform
    auto radonTransf = _radon3D.sampleTransform(phiRange.linspace(nbPhi),
                                                thetaRange.linspace(nbTheta),
                                                distRange.linspace(nbDist));

    // compute (partial) derivative along distance dimension
    imgproc::filter<2>(radonTransf, filterMethod);

    // construct and return resampler
    return VolumeResampler(radonTransf, phiRange, thetaRange, distRange);
}

std::vector<float> IntermediateVol::sampled(const std::vector<Radon3DCoord>& samplingPointsWCS,
                                            float plusMinusH_mm) const
{
    std::vector<float> ret;
    ret.reserve(samplingPointsWCS.size());

    for(const auto& smpl : samplingPointsWCS)
    {
        // compute two plane integrals at points dist +- h (central difference)
        auto value = _radon3D.planeIntegral(smpl.azimuth(), smpl.polar(), smpl.dist() + plusMinusH_mm )
                     - _radon3D.planeIntegral(smpl.azimuth(), smpl.polar(), smpl.dist() - plusMinusH_mm );

        ret.push_back(value / (2.0f * plusMinusH_mm)); // factor 1/(2h) for central difference
    }

    return ret;
}


// #### Radon3DCoordTransform ####
// -----------------------------

Radon3DCoordTransform::Radon3DCoordTransform(size_t nbCoords, uint oclDeviceNb)
    : _q(OpenCLConfig::instance().context(), OpenCLConfig::instance().devices()[oclDeviceNb])
    , _homTransfBuf(16, _q)
    , _initialPlanesRadonCoord(nbCoords * 3, _q)
    , _initialPlanesHomCoord(OpenCLConfig::instance().context(), CL_MEM_READ_WRITE,
                             nbCoords * 4 * sizeof(float))
    , _transformedCoords(OpenCLConfig::instance().context(), CL_MEM_READ_WRITE,
                         nbCoords * 3 * sizeof(float))
{
}

Radon3DCoordTransform::Radon3DCoordTransform(const std::vector<Radon3DCoord>& initialCoords,
                                             uint oclDeviceNb)
    : Radon3DCoordTransform(initialCoords.size(), oclDeviceNb)

{
    addKernels();

    _initialPlanesRadonCoord.writeToDev(&initialCoords.front().coord1());
    transformRadonToHom();
}

Radon3DCoordTransform::Radon3DCoordTransform(const std::vector<HomCoordPlaneNormalized>& initialCoords, uint oclDeviceNb)
    : Radon3DCoordTransform(initialCoords.size(), oclDeviceNb)
{
    addKernels();
    _q.enqueueWriteBuffer(_initialPlanesHomCoord, CL_TRUE, 0,
                          initialCoords.size() * 4 * sizeof(float), initialCoords.data());
}

void Radon3DCoordTransform::resetIninitialCoords(const std::vector<Radon3DCoord>& initialCoords)
{
    if(nbCoords() != initialCoords.size())
        recreateBuffers(initialCoords.size());

    _initialPlanesRadonCoord.writeToDev(&initialCoords.front().coord1());
    transformRadonToHom();
}

void Radon3DCoordTransform::resetIninitialCoords(const std::vector<HomCoordPlaneNormalized>& initialCoords)
{
    if(nbCoords() != initialCoords.size())
        recreateBuffers(initialCoords.size());

    _q.enqueueWriteBuffer(_initialPlanesHomCoord, CL_TRUE, 0,
                          initialCoords.size() * 4 * sizeof(float), initialCoords.data());
}

const cl::Buffer& Radon3DCoordTransform::transform(const Homography3D& homography) const
{
    const auto H = homography.transposed();

    std::transform(H.begin(), H.end(), _homTransfBuf.hostPtr(),
                   [](double val){ return float(val); });
    _homTransfBuf.transferPinnedMemToDev(false);

    auto kernel = OpenCLConfig::instance().kernel(CL_KERNEL_HOM2RADON);
    kernel->setArg(0, _homTransfBuf.devBuffer());
    kernel->setArg(1, _initialPlanesHomCoord);
    kernel->setArg(2, _transformedCoords);

    cl::Event event;
    _q.enqueueNDRangeKernel(*kernel, cl::NullRange, cl::NDRange(nbCoords()), cl::NullRange, nullptr,
                            &event);
    // wait for buffer is ready
    event.wait();

    return _transformedCoords;
}

const cl::Buffer& Radon3DCoordTransform::transform(const Matrix3x3& rotation,
                                                   const Vector3x1& translation) const
{
    return transform( Homography3D{ rotation, translation } );
}

std::vector<Radon3DCoord>
Radon3DCoordTransform::transformedCoords(const Matrix3x3& rotation,
                                         const Vector3x1& translation) const
{
    std::vector<Radon3DCoord> ret;

    transform(rotation, translation);

    auto nbCoords = this->nbCoords();
    ret.resize(nbCoords);
    _q.enqueueReadBuffer(_transformedCoords, CL_TRUE, 0, nbCoords * 3 * sizeof(float), ret.data());

    return ret;
}

std::vector<HomCoordPlaneNormalized> Radon3DCoordTransform::initialHomCoords() const
{
    std::vector<HomCoordPlaneNormalized> ret(nbCoords());

    _q.enqueueReadBuffer(_initialPlanesHomCoord, CL_TRUE, 0, nbCoords() * 4 * sizeof(float), ret.data());

    return ret;
}

void Radon3DCoordTransform::addKernels() const
{
    ClFileLoader clFileLoader;

    clFileLoader.setFileName("processing/" + CL_KERNEL_HOM2RADON + ".cl");
    OpenCLConfig::instance().addKernel(CL_KERNEL_HOM2RADON, clFileLoader.loadSourceCode());

    clFileLoader.setFileName("processing/" + CL_KERNEL_RADON2HOM + ".cl");
    OpenCLConfig::instance().addKernel(CL_KERNEL_RADON2HOM, clFileLoader.loadSourceCode());
}

size_t Radon3DCoordTransform::nbCoords() const
{
    return _initialPlanesRadonCoord.nbElements() / 3;
}

void Radon3DCoordTransform::recreateBuffers(size_t nbCoords)
{
    _initialPlanesRadonCoord = PinnedBufHostWrite<float>(nbCoords * 3, _q);
    _initialPlanesHomCoord = cl::Buffer(OpenCLConfig::instance().context(), CL_MEM_READ_WRITE,
                                        nbCoords * 4 * sizeof(float));
    _transformedCoords = cl::Buffer(OpenCLConfig::instance().context(), CL_MEM_READ_WRITE,
                                    nbCoords * 3 * sizeof(float));
}

void Radon3DCoordTransform::transformRadonToHom() const
{
    auto kernel = OpenCLConfig::instance().kernel(CL_KERNEL_RADON2HOM);

    kernel->setArg(0, _initialPlanesRadonCoord.devBuffer());
    kernel->setArg(1, _initialPlanesHomCoord);

    _q.enqueueNDRangeKernel(*kernel, cl::NullRange, cl::NDRange(_initialPlanesRadonCoord.nbElements() / 3));
}

} // namespace OCL
} // namespace CTL

namespace {

std::vector<bool> findTrucatedLines(const std::vector<CTL::Radon2DCoord>& radon2DCoords,
                                    const CTL::Chunk2D<float>& proj,
                                    float truncationThreshold)
{
    auto ret = std::vector<bool>(radon2DCoords.size());

    std::transform(radon2DCoords.cbegin(), radon2DCoords.cend(), ret.begin(),
                   [&proj, truncationThreshold](const CTL::Radon2DCoord& radon2DCoord) {
                       return isTruncatedLine(radon2DCoord, proj, truncationThreshold);
                   });

    return ret;
}

bool isTruncatedLine(const CTL::Radon2DCoord& radonCoords,
                     const CTL::Chunk2D<float>& proj,
                     float truncationThreshold)
{
    using CTL::mat::Matrix;
    using CTL::assist::interpolate2D;

    const auto cosMu = std::cos(radonCoords.angle());
    const auto sinMu = std::sin(radonCoords.angle());
    const auto s = radonCoords.dist()
        + Matrix<1, 2>{ cosMu, sinMu } * defaultOriginRadon2D(proj.dimensions()); // pixel frame
    const auto xMax = double(proj.dimensions().width - 1u);
    const auto yMax = double(proj.dimensions().height - 1u);
    constexpr auto eps = 1.0e-5f;

    if(std::abs(cosMu) > eps)
    {
        const auto xLiesInside = [xMax] (double x) {
            return x >= 0.0 && x <= xMax;
        };

        const auto x1 = double(s / cosMu);
        if(xLiesInside(x1) && interpolate2D(proj, x1, 0.0) > truncationThreshold)
            return true;

        const auto x2 = double((s - yMax * sinMu) / cosMu);
        if(xLiesInside(x2) && interpolate2D(proj, x2, yMax) > truncationThreshold)
            return true;
    }

    if(std::abs(sinMu) > eps)
    {
        const auto yLiesInside = [yMax] (double y) {
            return y >= 0.0 && y <= yMax;
        };

        const auto y1 = double(s / sinMu);
        if(yLiesInside(y1) && interpolate2D(proj, 0.0, y1) > truncationThreshold)
            return true;

        const auto y2 = double((s - xMax * cosMu) / sinMu);
        if(yLiesInside(y2) && interpolate2D(proj, xMax, y2) > truncationThreshold)
            return true;
    }

    return false;
}

CTL::mat::Matrix<2, 1> defaultOriginRadon2D(CTL::Chunk2D<float>::Dimensions projSize)
{
    return 0.5 * CTL::mat::Matrix<2, 1>(projSize.width - 1, projSize.height - 1);
}

template <typename T>
std::vector<T> selectUntruncatedValues(std::vector<T>&& values,
                                       const std::vector<bool>& truncatedIndices)
{
    auto pred = truncatedIndices.cbegin();
    values.erase(
        std::remove_if(values.begin(), values.end(), [&pred](const T&) { return *pred++; }),
        values.end());

    return std::move(values);
}

template<class T>
std::vector<T> randomSubset(std::vector<T>&& fullSamples, uint seed, float subsampleLevel)
{
    auto newNbElements = uint(std::ceil(subsampleLevel * float(fullSamples.size())));
    std::vector<T> ret(newNbElements);

    std::mt19937 rng;
    rng.seed(seed);

    std::vector<uint> indices(fullSamples.size());
    std::iota(indices.begin(), indices.end(), 0);

    std::shuffle(indices.begin(), indices.end(), rng);

    indices.resize(newNbElements);
    std::sort(indices.begin(), indices.end());

    for(uint smpl = 0; smpl < newNbElements; ++smpl)
        ret[smpl] = fullSamples[indices[smpl]];

    return ret;
}

} // unnamed namespace
