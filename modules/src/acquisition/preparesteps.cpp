#include <QDebug>

#include "components/allcomponents.h"
#include "preparesteps.h"

namespace CTL {
namespace prepare {

DECLARE_SERIALIZABLE_TYPE(GenericDetectorParam)
DECLARE_SERIALIZABLE_TYPE(GenericGantryParam)
DECLARE_SERIALIZABLE_TYPE(CarmGantryParam)
DECLARE_SERIALIZABLE_TYPE(TubularGantryParam)
DECLARE_SERIALIZABLE_TYPE(GantryDisplacementParam)
DECLARE_SERIALIZABLE_TYPE(SourceParam)
DECLARE_SERIALIZABLE_TYPE(XrayLaserParam)
DECLARE_SERIALIZABLE_TYPE(XrayTubeParam)

// ### ###  ### ###
// ### GANTRIES ###
// ### ###  ### ###

void TubularGantryParam::prepare(SimpleCTSystem& system) const
{
    auto gantryPtr = static_cast<TubularGantry*>(system.gantry());

    qDebug() << "PrepareTubularGantry --- preparing gantry\n"
             << "- rotation\t" << _newRotationAngle << "\n"
             << "- pitch\t\t" << _newPitchPosition << "\n"
             << "- tilt\t\t" << _newTiltAngle << "\n";

    if(_newRotationAngle.first)
        gantryPtr->setRotationAngle(_newRotationAngle.second);
    if(_newPitchPosition.first)
        gantryPtr->setPitchPosition(_newPitchPosition.second);
    if(_newTiltAngle.first)
        gantryPtr->setTiltAngle(_newTiltAngle.second);
}

bool TubularGantryParam::isApplicableTo(const CTSystem& system) const
{
    return system.isSimple() &&
            dynamic_cast<TubularGantry*>(system.gantries().front());
}

void TubularGantryParam::fromVariant(const QVariant &variant)
{
    QVariantMap varMap = variant.toMap();

    if(varMap.contains(QStringLiteral("rotation angle")))
        _newRotationAngle = { true, varMap.value(QStringLiteral("rotation angle")).toDouble() };
    if(varMap.contains(QStringLiteral("pitch position")))
        _newPitchPosition = { true, varMap.value(QStringLiteral("pitch position")).toDouble() };
    if(varMap.contains(QStringLiteral("tilt angle")))
        _newTiltAngle = { true, varMap.value(QStringLiteral("tilt angle")).toDouble() };
}

QVariant TubularGantryParam::toVariant() const
{
    QVariantMap ret = AbstractPrepareStep::toVariant().toMap();

    if(_newRotationAngle.first)
        ret.insert(QStringLiteral("rotation angle"),_newRotationAngle.second);
    if(_newPitchPosition.first)
        ret.insert(QStringLiteral("pitch position"),_newPitchPosition.second);
    if(_newTiltAngle.first)
        ret.insert(QStringLiteral("tilt angle"),_newTiltAngle.second);

    return ret;
}

std::shared_ptr<TubularGantryParam> TubularGantryParam::forRotationAngle(double rotation)
{
    auto ret = std::make_shared<TubularGantryParam>();
    ret->setRotationAngle(rotation);

    return ret;
}

std::shared_ptr<TubularGantryParam> TubularGantryParam::forPitchPosition(double pitch)
{
    auto ret = std::make_shared<TubularGantryParam>();
    ret->setPitchPosition(pitch);

    return ret;
}

std::shared_ptr<TubularGantryParam> TubularGantryParam::forTiltAngle(double tilt)
{
    auto ret = std::make_shared<TubularGantryParam>();
    ret->setTiltAngle(tilt);

    return ret;
}

void CarmGantryParam::prepare(SimpleCTSystem& system) const
{
    auto gantryPtr = static_cast<CarmGantry*>(system.gantry());

    qDebug() << "PrepareCarmGantry --- preparing gantry\n"
             << "- location\t" << _newLocation.first;
    qDebug() << (_newLocation.second.position.info() + _newLocation.second.rotation.info()).c_str()
             << "- span\t\t" << _newCarmSpan << "\n";

    if(_newLocation.first)
        gantryPtr->setLocation(_newLocation.second);
    if(_newCarmSpan.first)
        gantryPtr->setCarmSpan(_newCarmSpan.second);
}

bool CarmGantryParam::isApplicableTo(const CTSystem& system) const
{
    return system.isSimple() &&
           dynamic_cast<CarmGantry*>(system.gantries().front());
}

void CarmGantryParam::fromVariant(const QVariant &variant)
{
    QVariantMap varMap = variant.toMap();

    if(varMap.contains(QStringLiteral("location")))
    {
        mat::Location loc;
        loc.fromVariant(varMap.value(QStringLiteral("location")));
        _newLocation = { true, loc };
    }
    if(varMap.contains(QStringLiteral("c-arm span")))
        _newCarmSpan = { true, varMap.value(QStringLiteral("c-arm span")).toDouble() };
}

QVariant CarmGantryParam::toVariant() const
{
    QVariantMap ret = AbstractPrepareStep::toVariant().toMap();

    if(_newLocation.first)
        ret.insert(QStringLiteral("location"),_newLocation.second.toVariant());
    if(_newCarmSpan.first)
        ret.insert(QStringLiteral("c-arm span"),_newCarmSpan.second);

    return ret;
}

std::shared_ptr<CarmGantryParam> CarmGantryParam::forLocation(const mat::Location& location)
{
    auto ret = std::make_shared<CarmGantryParam>();
    ret->setLocation(location);

    return ret;
}

std::shared_ptr<CarmGantryParam> CarmGantryParam::forCarmSpan(double span)
{
    auto ret = std::make_shared<CarmGantryParam>();
    ret->setCarmSpan(span);

    return ret;
}

void GenericGantryParam::prepare(SimpleCTSystem& system) const
{
    auto gantryPtr = static_cast<GenericGantry*>(system.gantry());

    qDebug() << "PrepareGenericGantry --- preparing gantry\n"
             << "- detector location\t" << _newDetectorLocation.first;
    qDebug() << (_newDetectorLocation.second.position.info()
                 + _newDetectorLocation.second.rotation.info()).c_str()
             << "- X-ray source location \t" << _newSourceLocation.first;
    qDebug() << (_newSourceLocation.second.position.info()
                 + _newSourceLocation.second.rotation.info()).c_str();

    if(_newDetectorLocation.first)
        gantryPtr->setDetectorLocation(_newDetectorLocation.second);
    if(_newSourceLocation.first)
        gantryPtr->setSourceLocation(_newSourceLocation.second);
}

bool GenericGantryParam::isApplicableTo(const CTSystem& system) const
{
    return system.isSimple() &&
           dynamic_cast<GenericGantry*>(system.gantries().front());
}

void GenericGantryParam::fromVariant(const QVariant &variant)
{
    QVariantMap varMap = variant.toMap();

    if(varMap.contains(QStringLiteral("detector location")))
    {
        mat::Location loc;
        loc.fromVariant(varMap.value(QStringLiteral("detector location")));
        _newDetectorLocation = { true, loc };
    }
    if(varMap.contains(QStringLiteral("source location")))
    {
        mat::Location loc;
        loc.fromVariant(varMap.value(QStringLiteral("source location")));
        _newSourceLocation = { true, loc };
    }
}

QVariant GenericGantryParam::toVariant() const
{
    QVariantMap ret = AbstractPrepareStep::toVariant().toMap();

    if(_newDetectorLocation.first)
        ret.insert(QStringLiteral("detector location"), _newDetectorLocation.second.toVariant());
    if(_newSourceLocation.first)
        ret.insert(QStringLiteral("source location"), _newSourceLocation.second.toVariant());

    return ret;
}

std::shared_ptr<GenericGantryParam>
GenericGantryParam::forDetectorLocation(const mat::Location& location)
{
    auto ret = std::make_shared<GenericGantryParam>();
    ret->setDetectorLocation(location);

    return ret;
}

std::shared_ptr<GenericGantryParam>
GenericGantryParam::forSourceLocation(const mat::Location& location)
{
    auto ret = std::make_shared<GenericGantryParam>();
    ret->setSourceLocation(location);

    return ret;
}

void GantryDisplacementParam::prepare(SimpleCTSystem& system) const
{
    auto gantryPtr = system.gantry();

    qDebug() << "PrepareGantryDisplacements --- preparing gantry\n"
             << "- detector displacement\t" << _newDetectorDisplacement.first;
    qDebug() << (_newDetectorDisplacement.second.position.info()
                 + _newDetectorDisplacement.second.rotation.info()).c_str()
             << "- gantry displacement\t" << _newGantryDisplacement.first;
    qDebug() << (_newGantryDisplacement.second.position.info()
                 + _newGantryDisplacement.second.rotation.info()).c_str()
             << "- X-ray source location\t" << _newSourceDisplacement.first;
    qDebug() << (_newSourceDisplacement.second.position.info()
                 + _newSourceDisplacement.second.rotation.info()).c_str();

    if(_newDetectorDisplacement.first)
        gantryPtr->setDetectorDisplacement(_newDetectorDisplacement.second);
    if(_newGantryDisplacement.first)
        gantryPtr->setGantryDisplacement(_newGantryDisplacement.second);
    if(_newSourceDisplacement.first)
        gantryPtr->setSourceDisplacement(_newSourceDisplacement.second);
    if(_detectorDisplacementIncrement.first)
    {
        mat::Location fullDetDispl;
        auto prevDetDispl = gantryPtr->detectorDisplacement();
        fullDetDispl.position
            = prevDetDispl.position + _detectorDisplacementIncrement.second.position;
        fullDetDispl.rotation
            = _detectorDisplacementIncrement.second.rotation * prevDetDispl.rotation;
        gantryPtr->setDetectorDisplacement(fullDetDispl);
    }
    if(_sourceDisplacementIncrement.first)
    {
        mat::Location fullSrcDispl;
        auto prevSrcDispl = gantryPtr->sourceDisplacement();
        fullSrcDispl.position
            = prevSrcDispl.position + _sourceDisplacementIncrement.second.position;
        fullSrcDispl.rotation
            = prevSrcDispl.rotation * _sourceDisplacementIncrement.second.rotation;
        gantryPtr->setSourceDisplacement(fullSrcDispl);
    }
}

bool GantryDisplacementParam::isApplicableTo(const CTSystem& system) const
{
    return system.isSimple();
}

void GantryDisplacementParam::fromVariant(const QVariant &variant)
{
    QVariantMap varMap = variant.toMap();

    if(varMap.contains(QStringLiteral("detector displacement")))
    {
        mat::Location loc;
        loc.fromVariant(varMap.value(QStringLiteral("detector displacement")));
        _newDetectorDisplacement = { true, loc };
    }
    if(varMap.contains(QStringLiteral("gantry displacement")))
    {
        mat::Location loc;
        loc.fromVariant(varMap.value(QStringLiteral("gantry displacement")));
        _newGantryDisplacement = { true, loc };
    }
    if(varMap.contains(QStringLiteral("source displacement")))
    {
        mat::Location loc;
        loc.fromVariant(varMap.value(QStringLiteral("source displacement")));
        _newSourceDisplacement = { true, loc };
    }
    if(varMap.contains(QStringLiteral("detector displacement increment")))
    {
        mat::Location loc;
        loc.fromVariant(varMap.value(QStringLiteral("detector displacement increment")));
        _detectorDisplacementIncrement = { true, loc };
    }
    if(varMap.contains(QStringLiteral("source displacement increment")))
    {
        mat::Location loc;
        loc.fromVariant(varMap.value(QStringLiteral("source displacement increment")));
        _sourceDisplacementIncrement = { true, loc };
    }
}

QVariant GantryDisplacementParam::toVariant() const
{
    QVariantMap ret = AbstractPrepareStep::toVariant().toMap();

    if(_newDetectorDisplacement.first)
        ret.insert(QStringLiteral("detector displacement"), _newDetectorDisplacement.second.toVariant());
    if(_newGantryDisplacement.first)
        ret.insert(QStringLiteral("gantry displacement"), _newGantryDisplacement.second.toVariant());
    if(_newSourceDisplacement.first)
        ret.insert(QStringLiteral("source displacement"), _newSourceDisplacement.second.toVariant());
    if(_detectorDisplacementIncrement.first)
        ret.insert(QStringLiteral("detector displacement increment"), _detectorDisplacementIncrement.second.toVariant());
    if(_sourceDisplacementIncrement.first)
        ret.insert(QStringLiteral("source displacement increment"), _sourceDisplacementIncrement.second.toVariant());

    return ret;
}

std::shared_ptr<GantryDisplacementParam>
GantryDisplacementParam::forDetectorDisplacement(const mat::Location& displacement)
{
    auto ret = std::make_shared<GantryDisplacementParam>();
    ret->setDetectorDisplacement(displacement);

    return ret;
}

std::shared_ptr<GantryDisplacementParam>
GantryDisplacementParam::forGantryDisplacement(const mat::Location& displacement)
{
    auto ret = std::make_shared<GantryDisplacementParam>();
    ret->setGantryDisplacement(displacement);

    return ret;
}

std::shared_ptr<GantryDisplacementParam>
GantryDisplacementParam::forSourceDisplacement(const mat::Location& displacement)
{
    auto ret = std::make_shared<GantryDisplacementParam>();
    ret->setSourceDisplacement(displacement);

    return ret;
}

std::shared_ptr<GantryDisplacementParam>
GantryDisplacementParam::forDetectorDisplacementIncrement(const mat::Location& increment)
{
    auto ret = std::make_shared<GantryDisplacementParam>();
    ret->incrementDetectorDisplacement(increment);

    return ret;
}

std::shared_ptr<GantryDisplacementParam>
GantryDisplacementParam::forSourceDisplacementIncrement(const mat::Location& increment)
{
    auto ret = std::make_shared<GantryDisplacementParam>();
    ret->incrementSourceDisplacement(increment);

    return ret;
}

// ### ### ### ###
// ### SOURCES ###
// ### ### ### ###

void SourceParam::prepare(SimpleCTSystem& system) const
{
    auto sourcePtr = system.source();

    qDebug() << "PrepareAbstractSource --- preparing source\n"
             << "- energy range\t" << _energyRangeRestr.first
             << _energyRangeRestr.second.start() << "," << _energyRangeRestr.second.end() << "\n"
             << "- flux mod\t" << _newFluxModifier << "\n"
             << "- focal spot size\t" << _newFocalSpotSize << "\n"
             << "- focal spot pos\t" << _newSpotPosition.first;
    qDebug() << _newSpotPosition.second.info().c_str();

    if(_newFluxModifier.first)
        sourcePtr->setFluxModifier(_newFluxModifier.second);
    if(_newFocalSpotSize.first)
        sourcePtr->setFocalSpotSize(_newFocalSpotSize.second);
    if(_newSpotPosition.first)
        sourcePtr->setFocalSpotPosition(_newSpotPosition.second);
    if(_energyRangeRestr.first)
        sourcePtr->setEnergyRangeRestriction(_energyRangeRestr.second);
}

bool SourceParam::isApplicableTo(const CTSystem& system) const
{
    return system.isSimple();
}

void SourceParam::fromVariant(const QVariant &variant)
{
    QVariantMap varMap = variant.toMap();

    if(varMap.contains(QStringLiteral("focal spot position")))
    {
        auto fsPos = varMap.value(QStringLiteral("focal spot position")).toList();
        Vector3x1 fsPosVec(fsPos.at(0).toDouble(),
                           fsPos.at(1).toDouble(),
                           fsPos.at(2).toDouble());

        _newSpotPosition = { true, fsPosVec };
    }
    if(varMap.contains(QStringLiteral("focal spot size")))
    {
        auto fsSize = varMap.value(QStringLiteral("focal spot size")).toMap();
        QSizeF fsQSize;
        fsQSize.setWidth(fsSize.value(QStringLiteral("width")).toDouble());
        fsQSize.setHeight(fsSize.value(QStringLiteral("height")).toDouble());

        _newFocalSpotSize = { true, fsQSize };
    }
    if(varMap.contains(QStringLiteral("flux modifier")))
        _newFluxModifier = { true, varMap.value(QStringLiteral("flux modifier")).toDouble() };
}

QVariant SourceParam::toVariant() const
{
    QVariantMap ret = AbstractPrepareStep::toVariant().toMap();

    if(_newSpotPosition.first)
    {
        QVariantList fsPos;
        fsPos.append(_newSpotPosition.second.get<0>());
        fsPos.append(_newSpotPosition.second.get<1>());
        fsPos.append(_newSpotPosition.second.get<2>());
        ret.insert(QStringLiteral("focal spot position"), fsPos);
    }
    if(_newFocalSpotSize.first)
    {
        QVariantMap fsSize;
        fsSize.insert(QStringLiteral("width"), _newFocalSpotSize.second.width());
        fsSize.insert(QStringLiteral("height"), _newFocalSpotSize.second.height());
        ret.insert(QStringLiteral("focal spot size"), fsSize);
    }
    if(_newFluxModifier.first)
        ret.insert(QStringLiteral("flux modifier"),_newFluxModifier.second);

    return ret;
}

std::shared_ptr<SourceParam> SourceParam::forEnergyRangeRestriction(const Range<float>& range)
{
    auto ret = std::make_shared<SourceParam>();
    ret->setEnergyRangeRestriction(range);

    return ret;
}

std::shared_ptr<SourceParam> SourceParam::forFluxModifier(double modifier)
{
    auto ret = std::make_shared<SourceParam>();
    ret->setFluxModifier(modifier);

    return ret;
}

std::shared_ptr<SourceParam> SourceParam::forFocalSpotSize(const QSizeF& size)
{
    auto ret = std::make_shared<SourceParam>();
    ret->setFocalSpotSize(size);

    return ret;
}

std::shared_ptr<SourceParam> SourceParam::forFocalSpotPosition(const Vector3x1& position)
{
    auto ret = std::make_shared<SourceParam>();
    ret->setFocalSpotPosition(position);

    return ret;
}

void XrayLaserParam::prepare(SimpleCTSystem& system) const
{
    SourceParam::prepare(system);

    auto sourcePtr = static_cast<XrayLaser*>(system.source());

    qDebug() << "PrepareXrayLaser --- preparing source\n"
             << "- energy\t" << _newPhotonEnergy << "\n"
             << "- power\t" << _newPower << "\n";

    if(_newPhotonEnergy.first)
        sourcePtr->setPhotonEnergy(_newPhotonEnergy.second);
    if(_newPower.first)
        sourcePtr->setRadiationOutput(_newPower.second);
}

bool XrayLaserParam::isApplicableTo(const CTSystem& system) const
{
    return system.isSimple() &&
           dynamic_cast<XrayLaser*>(system.sources().front());
}

void XrayLaserParam::fromVariant(const QVariant &variant)
{
    SourceParam::fromVariant(variant);

    QVariantMap varMap = variant.toMap();

    if(varMap.contains(QStringLiteral("photon energy")))
        _newPhotonEnergy = { true, varMap.value(QStringLiteral("photon energy")).toDouble() };
    if(varMap.contains(QStringLiteral("power")))
        _newPower = { true, varMap.value(QStringLiteral("power")).toDouble() };
}

QVariant XrayLaserParam::toVariant() const
{
    QVariantMap ret = SourceParam::toVariant().toMap();

    if(_newPhotonEnergy.first)
        ret.insert(QStringLiteral("photon energy"),_newPhotonEnergy.second);
    if(_newPower.first)
        ret.insert(QStringLiteral("power"),_newPower.second);

    return ret;
}

std::shared_ptr<XrayLaserParam> XrayLaserParam::forPhotonEnergy(double energy)
{
    auto ret = std::make_shared<XrayLaserParam>();
    ret->setPhotonEnergy(energy);

    return ret;
}

std::shared_ptr<XrayLaserParam> XrayLaserParam::forPower(double power)
{
    auto ret = std::make_shared<XrayLaserParam>();
    ret->setPower(power);

    return ret;
}

void XrayTubeParam::prepare(SimpleCTSystem& system) const
{
    SourceParam::prepare(system);

    auto sourcePtr = static_cast<XrayTube*>(system.source());

    qDebug() << "PrepareXrayTube --- preparing source\n"
             << "- voltage\t" << _newTubeVoltage << "\n"
             << "- emission\t" << _newEmissionCurrent << "\n";

    if(_newTubeVoltage.first)
        sourcePtr->setTubeVoltage(_newTubeVoltage.second);
    if(_newEmissionCurrent.first)
        sourcePtr->setMilliampereSeconds(_newEmissionCurrent.second);
}

bool XrayTubeParam::isApplicableTo(const CTSystem& system) const
{
    return system.isSimple() &&
           dynamic_cast<XrayTube*>(system.sources().front());
}

void XrayTubeParam::fromVariant(const QVariant &variant)
{
    SourceParam::fromVariant(variant);

    QVariantMap varMap = variant.toMap();

    if(varMap.contains(QStringLiteral("tube voltage")))
        _newTubeVoltage = { true, varMap.value(QStringLiteral("tube voltage")).toDouble() };
    if(varMap.contains(QStringLiteral("emission current")))
        _newEmissionCurrent = { true, varMap.value(QStringLiteral("emission current")).toDouble() };
}

QVariant XrayTubeParam::toVariant() const
{
    QVariantMap ret = SourceParam::toVariant().toMap();

    if(_newTubeVoltage.first)
        ret.insert(QStringLiteral("tube voltage"),_newTubeVoltage.second);
    if(_newEmissionCurrent.first)
        ret.insert(QStringLiteral("emission current"),_newEmissionCurrent.second);

    return ret;
}

std::shared_ptr<XrayTubeParam> XrayTubeParam::forTubeVoltage(double voltage)
{
    auto ret = std::make_shared<XrayTubeParam>();
    ret->setTubeVoltage(voltage);

    return ret;
}

std::shared_ptr<XrayTubeParam> XrayTubeParam::forMilliampereSeconds(double mAs)
{
    auto ret = std::make_shared<XrayTubeParam>();
    ret->setMilliampereSeconds(mAs);

    return ret;
}

void GenericDetectorParam::prepare(SimpleCTSystem& system) const
{
    auto detectorPtr = static_cast<GenericDetector*>(system.detector());

    qDebug() << "PrepareGenericDetector --- preparing detector\n"
             << "- module locations\t" << _newModuleLocations.first << "\n"
             << "- number of modules\t" << _newModuleLocations.second.size() << "\n"
             << "- pixelSize\t" << _newPixelSize << "\n"
             << "- skew\t" << _newSkewAngle << "\n";

    if(_newModuleLocations.first)
        detectorPtr->setModuleLocations(_newModuleLocations.second);
    if(_newPixelSize.first)
        detectorPtr->setPixelSize(_newPixelSize.second);
    if(_newSkewAngle.first)
        detectorPtr->setSkewAngle(_newSkewAngle.second);
}

bool GenericDetectorParam::isApplicableTo(const CTSystem& system) const
{
    return system.isSimple() &&
           dynamic_cast<GenericDetector*>(system.detectors().front());
}

void GenericDetectorParam::fromVariant(const QVariant &variant)
{
    QVariantMap varMap = variant.toMap();

    if(varMap.contains(QStringLiteral("module locations")))
    {
        QVariantList moduleList = varMap.value(QStringLiteral("module locations")).toList();
        QVector<mat::Location> modLocs;
        modLocs.reserve(moduleList.size());
        for(const auto& mod : qAsConst(moduleList))
        {
            mat::Location loc;
            loc.fromVariant(mod);
            modLocs.append(loc);
        }

        _newModuleLocations = { true, modLocs };
    }
    if(varMap.contains(QStringLiteral("pixel size")))
    {
        auto pixSize = varMap.value(QStringLiteral("pixel size")).toMap();
        QSizeF pixQSize;
        pixQSize.setWidth(pixSize.value(QStringLiteral("width")).toDouble());
        pixQSize.setHeight(pixSize.value(QStringLiteral("height")).toDouble());

        _newPixelSize = { true, pixQSize };
    }
    if(varMap.contains(QStringLiteral("skew coefficient")))
        _newSkewAngle = { true, varMap.value(QStringLiteral("skew coefficient")).toDouble() };
}

QVariant GenericDetectorParam::toVariant() const
{
    QVariantMap ret;

    if(_newModuleLocations.first)
    {
        QVariantList moduleList;
        moduleList.reserve(_newModuleLocations.second.size());
        for(const auto& mod : _newModuleLocations.second)
            moduleList.append(mod.toVariant());
        ret.insert(QStringLiteral("module locations"), moduleList);
    }
    if(_newPixelSize.first)
    {
        QVariantMap pixSize;
        pixSize.insert(QStringLiteral("width"), _newPixelSize.second.width());
        pixSize.insert(QStringLiteral("height"), _newPixelSize.second.height());
        ret.insert(QStringLiteral("pixel size"), pixSize);
    }
    if(_newSkewAngle.first)
        ret.insert(QStringLiteral("skew coefficient"),_newSkewAngle.second);

    return ret;
}

std::shared_ptr<GenericDetectorParam>
GenericDetectorParam::forModuleLocations(QVector<mat::Location> moduleLocations)
{
    auto ret = std::make_shared<GenericDetectorParam>();
    ret->setModuleLocations(std::move(moduleLocations));

    return ret;
}

std::shared_ptr<GenericDetectorParam> GenericDetectorParam::forPixelSize(const QSizeF& size)
{
    auto ret = std::make_shared<GenericDetectorParam>();
    ret->setPixelSize(size);

    return ret;
}

std::shared_ptr<GenericDetectorParam> GenericDetectorParam::forSkewAngle(double skewAngle)
{
    auto ret = std::make_shared<GenericDetectorParam>();
    ret->setSkewAngle(skewAngle);

    return ret;
}


} // namespace prepare
} // namespace CTL
