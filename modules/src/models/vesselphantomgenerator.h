#ifndef CTL_VESSELPHANTOMGENERATOR_H
#define CTL_VESSELPHANTOMGENERATOR_H

#include "abstractphantomgenerator.h"
#include "mat/matrix_types.h"
#include <random>

namespace CTL {

class VesselPhantomGenerator : public AbstractPhantomGenerator
{
public: void drawPhantom(VoxelVolume<float>& volume) const override;

    struct WalkerState;

public:
    struct Parameters
    {
        Vector3x1 startingPoint{ 0.0, 0.0, 0.0 }; //!< point where the vessel starts growing [mm]
        double startDiameter{ 6.0 }; //!< initional thickness of the vessel [mm]
        double minimumDiameter{ 0.5 }; //!< minimal thickness of the vessel (after branching) [mm]
        double stepLength{ 0.05 }; //!< step length of a random walker (accuracy/scaling) [mm]
        double gaussianSigma{ 0.05 }; //!< s, where new direction is oldUnitDirection + (s,s,s)
        double branchingProb{ 0.002 }; //!< determines the branching frequency
        double linearNarrowing { 0.9999 }; //!< linear constriction per step
        float vesselValue{ 0.03f }; //!< attenuation value
    };

    uint64_t maxNbIter() const;
    Parameters& parameters();
    uint seed() const;
    void setMaxNbIter(uint64_t maxNbIter);
    void setSeed(uint seed);

private:
    void advanceWalker(WalkerState& walker) const;
    void drawDisk(const WalkerState& walker, VoxelVolume<float>& volume) const;
    void drawPoint(const WalkerState& walker, VoxelVolume<float>& volume) const;
    void paintIntoVoxel(const Vector3x1& voxelIdx, VoxelVolume<float>& volume) const;
    void performRandomWalk(WalkerState& walker, VoxelVolume<float>& volume) const;
    Vector3x1 randomDirection() const;
    void tryBranching(WalkerState& walker, VoxelVolume<float>& volume) const;
    void walk(WalkerState& walker, VoxelVolume<float>& volume) const;

    Parameters _p;
    uint64_t _maxNbIter{ std::numeric_limits<uint64_t>::max() }; //!< maximum number of iterations
    uint _seed{ std::random_device{}() };
    mutable std::mt19937 _uRNG{ _seed }; //!< uniform random generator
};

} // namespace CTL

#endif // CTL_VESSELPHANTOMGENERATOR_H
