#include "abstractvolumefilter.h"

#include <QDebug>
#include <typeinfo>

namespace CTL {

QVariant AbstractVolumeFilter::parameter() const { return QVariant(); }

void AbstractVolumeFilter::setParameter(const QVariant&) {}

// Use SerializationInterface::toVariant() documentation.
QVariant AbstractVolumeFilter::toVariant() const
{
    QVariantMap ret = SerializationInterface::toVariant().toMap();

    ret.insert(QStringLiteral("parameters"), parameter());

    return ret;
}

// Use SerializationInterface::fromVariant() documentation.
void AbstractVolumeFilter::fromVariant(const QVariant& variant)
{
    auto map = variant.toMap();
    if(map.value(QStringLiteral("type-id")).toInt() != type())
    {
        qWarning() << QString(typeid(*this).name())
                + "::fromVariant: Could not construct instance! "
                  "reason: incompatible variant passed";
        return;
    }

    setParameter(map.value(QStringLiteral("parameters")).toMap());
}

} // namespace CTL
