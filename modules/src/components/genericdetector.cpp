#include "genericdetector.h"

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(GenericDetector)

/*!
 * Constructs an empty object named \a name.
 */
GenericDetector::GenericDetector(const QString &name)
    : AbstractDetector(name)
{
}

/*!
 * Constructs a generic detector element with \a nbModules modules that have \a nbPixelPerModule pixels
 * (`channels` x `rows`) with. The moduleLocation will have default constructed locations
 * (position (0,0,0) and unity rotation matrix). Make sure to properly set the module locations using
 * setModuleLocations().
 * The pixel dimensions are initialized with default values of 1mm x 1mm (`width` x `height`).
 * If necessary, adjust them using setPixelSize().
 */
GenericDetector::GenericDetector(const QSize& nbPixelPerModule, uint nbModules, const QString& name)
    : GenericDetector(nbPixelPerModule, { 1.0, 1.0 }, QVector<ModuleLocation>(nbModules), name)
{
}

/*!
 * Constructs a generic detector element with modules that have \a nbPixelPerModule pixels
 * (`channels` x `rows`) with dimensions of \a pixelDimensions (`width` x `height`). The arrangement
 * of the individual modules with respect to the entire detector system are specified in \a
 * moduleLocations as a vector of ModuleLocation elements (each of which contain the modules
 * position and rotation).
 */
GenericDetector::GenericDetector(const QSize& nbPixelPerModule,
                                 const QSizeF& pixelDimensions,
                                 QVector<ModuleLocation> moduleLocations,
                                 const QString& name)
    : AbstractDetector(nbPixelPerModule, pixelDimensions, name)
    , _moduleLocations(std::move(moduleLocations))
{
}

/*!
 * Returns a formatted string with information about the object.
 *
 * In addition to the information from the base class, the info string contains the following
 * details: \li Nb. of detector modules \li Nb. of pixels per module \li Pixel dimensions.
 */
QString GenericDetector::info() const
{
    QString ret(AbstractDetector::info());

    // clang-format off
    ret +=
       typeInfoString(typeid(this)) +
       "\tNb. of detector modules: "   + QString::number(nbDetectorModules()) + "\n"
       "\tNb. of pixels per module: "  + QString::number(_nbPixelPerModule.width()) + " x " +
                                         QString::number(_nbPixelPerModule.height()) + "\n"
       "\tPixel dimensions: "          + QString::number(_pixelDimensions.width()) + " mm x " +
                                         QString::number(_pixelDimensions.height()) + " mm\n";

    ret += (this->type() == GenericDetector::Type) ? QLatin1String("}\n") : QLatin1String("");
    // clang-format on

    return ret;
}

/*!
 * Returns the default name for the component: "Generic detector".
 */
QString GenericDetector::defaultName()
{
    const QString defName(QStringLiteral("Generic detector"));
    static uint counter = 0;
    return counter++ ? defName + " (" + QString::number(counter) + ")" : defName;
}

// Use SerializationInterface::fromVariant() documentation.
void GenericDetector::fromVariant(const QVariant& variant)
{
    AbstractDetector::fromVariant(variant);

    _moduleLocations.clear();
    QVariantMap varMap = variant.toMap();
    auto locations = varMap.value(QStringLiteral("module locations")).toList();
    for(const auto& var : qAsConst(locations))
    {
        mat::Location loc;
        loc.fromVariant(var);

        _moduleLocations.append(loc);
    }

}

// Use SerializationInterface::toVariant() documentation.
QVariant GenericDetector::toVariant() const
{
    QVariantMap ret = AbstractDetector::toVariant().toMap();

    QVariantList modLocs;
    for(const auto& mod : _moduleLocations)
        modLocs.append(mod.toVariant());

    ret.insert(QStringLiteral("module locations"), modLocs);

    return ret;
}

std::unique_ptr<GenericDetector>
GenericDetector::cloneWithOtherModuleLocations(QVector<ModuleLocation> moduleLocations) const
{
    auto ret = makeComponent<GenericDetector>(*this);
    ret->_moduleLocations = std::move(moduleLocations);

    return ret;
}

std::unique_ptr<GenericDetector> GenericDetector::fromOther(const AbstractDetector& other)
{
    // if other is GenericDetector, use copy-ctor; otherwise: extract relevant data
    if(other.type() == GenericDetector::Type)
        return makeComponent<GenericDetector>(static_cast<const GenericDetector&>(other));

    auto ret = makeComponent<GenericDetector>(other.nbPixelPerModule(),
                                              other.pixelDimensions(),
                                              other.moduleLocations(),
                                              other.name() + "_convertedToGeneric");

    ret->setSkewAngle(other.skewAngle());
    if(other.hasSaturationModel())
        ret->setSaturationModel(other.saturationModel()->clone(), other.saturationModelType());
    if(other.hasSpectralResponseModel())
        ret->setSpectralResponseModel(other.spectralResponseModel()->clone());

    return ret;
}

// use documentation of GenericComponent::clone()
SystemComponent* GenericDetector::clone() const { return new GenericDetector(*this); }

/*!
 * Returns the vector that stores the module locations.
 *
 * Each ModuleLocation object contains the position of the module in world coordinates as well as a
 * rotation matrix that represents the transformation from the module's coordinate system to the
 * CT-system (i.e. the coordinate system of the detector as a whole).
 */
QVector<AbstractDetector::ModuleLocation> GenericDetector::moduleLocations() const
{
    return _moduleLocations;
}

/*!
 * Sets the module locations to \a moduleLocations.
 */
void GenericDetector::setModuleLocations(QVector<ModuleLocation> moduleLocations)
{
    if(moduleLocations.size() != _moduleLocations.size())
        throw std::domain_error("number of modules must not change");
    _moduleLocations = std::move(moduleLocations);
}

/*!
 * Sets the pixel size to \a size.
 *
 * Pixel sizes always refer to the pixel's rectangular (i.e. unskewed) shape.
 */
void GenericDetector::setPixelSize(const QSizeF &size)
{
    _pixelDimensions = size;
}

/*!
 * Sets the skew angle to \a skewAngle. The skew angle refers to the angle between the oblique pixel
 * edge and the (imaginary) edge of a corresponding rectangular pixel.
 *
 * This is zero if the detector coordinate system is orthogonal.
 */
void GenericDetector::setSkewAngle(double skewAngle)
{
    _skewAngle = skewAngle;
}

} // namespace CTL
