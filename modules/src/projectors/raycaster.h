#ifndef CTL_RAYCASTER_H
#define CTL_RAYCASTER_H

#include "ocl/oclcppbindings.h"
#include "mat/matrix_types.h"

namespace CTL {
namespace OCL {
namespace external {

class RayCaster
{
public:
    RayCaster();
    std::vector<float> project(const std::vector<ProjectionMatrix>& Pmats,
                               const std::vector<float>& volume);

    void setDetectorSize(uint nbRows, uint nbColumns);
    void setIncrement(float incrementMM);
    void setVolumeOffset(const float (&offset)[3]);
    void setVolumeInfo(const uint (&nbVoxel)[3], const float (&voxSize)[3]);

private:
    cl::Context context;
    cl::Program program;
    std::vector<cl::Device> device;

    cl_uint detectorColumns;
    cl_uint detectorRows;
    cl_float increment_mm;
    cl::size_t<3> volDim;
    cl_float3 volOffset;
    cl_float3 voxelSize;

    void initOpenCL();

    cl_float3 volumeCorner() const;
};

} // namespace external
} // namespace OCL
} // namespace CTL

#endif // CTL_RAYCASTER_H
