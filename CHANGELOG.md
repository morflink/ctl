# Changelog

## 2022-08-30 (v.0.5.3)

FOCUS: Multi-view 2D/3D registration

### Added
- 2D/3D registration
  - add virtual method `AbstractRegistration2D3D::optimize` that accepts a `std::vector<Chunk2D<float>>` of projection images
  - add implementation `GrangeatRegistration2D3D::optimize` that performs a Grangeat-based 2D/3D registration w.r.t. multpile projection images
- add iterator interface to `Chunk2D` template class by analogy with `VoxelVolume`, i.e. there are standard `begin`/`end` methods and it can be used in range-based for loops to iterate over pixels; the iterator also provides the pixel index, i.e. (x, y) indices, by calling the method `pixelIndex` on the iterator
- add function overloads `convertTo<ToType>` for conversion of `Chunk2D<FromType>` to `Chunk2D<ToType>` as well as `VoxelVolume<FromType>` to `VoxelVolume<ToType>`, example usage: `convertTo<float>(intVol)` or using a conversion callable: `convertTo<int>(floatVol, std::roundf)`.
- `NrrdFileIO`: `BaseTypeIO::readVolume` can now deduce voxel sizes from the NRRD field `space directions` (as used by 3D Slicer)

### Fixes/Changes
- remove non-const method `Chunk2D::data`, since a modifyable access to the internal data vector is error-prone (changing the size of the vector can invalidate a Chunk2D instance); use the new iterator interface instead (see above)
- fix typo in method name: `RadiationEncoder::detectiveQuantumEfficieny` -> `RadiationEncoder::detectiveQuantumEfficiency`


## 2022-05-04 (v.0.5.2)

FOCUS: Small fixes

### Fixes/Changes
- pre-compiled lib ("lib/ctl.pro"):
  - **[BUG]** fix deserialization with `AbstractSerializer` (such as `JsonSerializer`) when using the pre-compiled library. Before, this could fail, since the registration in the factory maps of the `SerializationHelper` might be skipped due to linker optimizations
  - only under Windows a static library, otherwise a dynamic library
- `StandardPipeline`: change return type of `StandardPipeline::settings<...>` methods. Now, references to setting objects are returned instead of value types. The old usage in client code is still valid, but ABI is broken.
- `StandardPipeline`: all move operations, including move assignment, are now enabled
- `RayCasterProjector`: in case of an unperformant setting, output a warning instead of changing the volume upsampling setting

### Added
- `FDKReconstructor`: footprint type of `SFPBackprojector` can be set in `FDKReconstructor::useSFPBackprojector` method and requested by `FDKReconstructor::footprintType`


## 2022-01-10 (v.0.5.1)

FOCUS: Small fixes

### Added
- member function `SimpleCTSystem::removeBeamModifiers`

### Fixes
- fix crash of `VolumeViewer` when setting new data
- fix return type of `AbstractCTSystemBlueprint::modifiers`
- MSVC compatibility


## 2021-12-01 (v.0.5)

FOCUS: CMake and Qt6 interoperability

### Added
- the project file `lib/ctl.pro`, which can be used to compile the CTL as a static library; NOTE: currently, the `ctl_nlopt` module is not included (you need to manually uncomment it)
- the script file `lib/ctl.cmake` can be included in CMake to make the target `CTL` available for linking
- full compatibility with Qt6
- `ClFileLoader::loadExternalSourceCode`: allows using OpenCL kernel files that are not located in the `modules/src/ocl/cl_src` folder

### Fixes
- `BackprojectorBase` now releases GPU memory directly after reconstruction (not only after destruction)


## 2021-09-24 (v.0.4.2)

FOCUS: Convenience

### Added
- command line app that computes the mTRE (registration error) from rigid parameters (estimated + ground truth)
- example app that computes the mean and effective attenuation of water from a specific CT system
- `NumericalIntegrationDecorator` converts a non-integrable data model into an integrable data model
- `io::RawDataIO` for reading raw data files
- `HilbertFilter` as an OpenCL projection filter
- arithmetic operators for integrable data models
- overload of `gui::plot` for `std::vector<float>`
- `IntermedGen2D2D`/`IntermedGen2D3D`: add compensation methods for projection truncation as well as their integration into the 2D/3D - registration app
- non-const accessors for `SingleViewGeometry`/`FullGeometry`
- conversion functions `muToHUWithMuWater` and `huToMuWithMuWater`

### Changes
- GenericOCLProjectionFilter/GenericOCLVolumeFilter: a custom path to a .cl file can be specified
- separate nested classes of Chunk2D and VoxelVolume (e.g. `VoxelVolumeDimensions`)

### Bug Fixes
- `ProjectorExtension::release` now disconnects the notifier of the nested projector
- serialization of `AttenuationFilter`s
- several minor fixes and compatibility improvements with MSVC


## 2021-05-28 (v.0.4.1)

FOCUS: Improvements of volume classes and general fixes

### Added
- volume conversion functions `muToHU` and `huToMu`

### Changed/Fixed
- `SpectralVolumeData::transformToAttenuationCoeff` can transform mu volumes as well (not only density volumes)
- more flexibility with empty volumes (arithmetic operations, `VolumeViewer` throws exception)
- rename `XrayTubeParam::setEmissionCurrent` to `XrayTubeParam::setMilliampereSeconds`
- compatibility with older compilers: in-/decrement operators of `VoxelVolume<T>::VoxelIterator`, macros in `oclcppbindings.h`
- minor fixes in `XrayTube`, `ARTReconstructor`, `ModuleLayout`, `AttenuationFilter::from/toVariant`, `CTL_TYPE_ID` macro
- [new URL](https://ctl-tutorials.github.io/) for the tutorials


## 2021-04-29 (v.0.4)

FOCUS: Reconstruction

### Added core functionality
- New major feature: reconstruction
	- `AbstractReconstructor`: provides the general interface
	- two backprojection approaches: `SimpleBackprojector` (CPU and GPU version) and `SFPBackprojector`
	- `FKDReconstructor`: implementation of FDK algorithm for reconstruction of (flat-panel) cone-beam projections
	- `ARTReconstructor`: implementation of the algebraic reconstruction technique; incl. options for ordered-subsets, various stopping criteria and regularization
- subset generation: sub-divide projection data (see also `ProjectionDataView`) into multiple smaller sets
    - definition of interface: `AbstractSubsetGenerator` (and `AbstractFixedSizeSubsetGenerator`)
    - implementations: 
        - `SimpleSubsetGenerator`: provides options for subsets with random view selection, consecutive blocks of views and simple orthogonal patterns (assuming 180° or 360° trajectories)
        - `OrthogonalSubsetGenerator`: subset generator that collects most orthogonal views into a subset, based on actual acquisition geometry
        - `TransitionSchemeExtension`: extension (wrapper) for other subset generators that creates a sequence of increasingly populated subsets in subsequent iterations (i.e. starting with many small subjects, progressing towards few big subsets)
        - `DefaultSubsetGenerator`: convenience class, combining the benefits of all the above within one class
- projection filters:
    - definition of interface: `AbstractProjectionFilter`
    - convenience class for simple OpenCL usage: `GenericOCLProjectionFilter` - create filters simply from an .cl file including the kernel function
    - implementations: `CosineWeighting`, `ParkerWeightsRev` ('revised' Parker weights), and `ApodizationFilter`
- volume filters:
    - definition of interface: `AbstractVolumeFilter`
    - convenience class for simple OpenCL usage: `GenericOCLVolumeFilter` - create filters simply from an .cl file including the kernel function
    - implementations (called regularizers here): `IdentityRegularizer`, `TVRegularizer` (CPU and GPU), `HuberRegularizer` (CPU and GPU), `MedianFilterRegularizer` (CPU only)
- phantom generators:
    - definition of interface: `AbstractPhantomGenerator`
    - example implementation: `VesselPhantomGenerator` (artificial vessel tree)
    - example of dynamic phantom: `GrowingVesselPhantom` (simulating blood in-flow)
- new data 'type' `ProjectionDataView`: an object holding a (read-only) reference to actual projection data along with a set of IDs of those views that are included in the `ProjectionDataView` (e.g. to represent subsets); conceptually similar to stringview
--> used in reconstruction tasks

### Further additions
- added several tomosynthesis trajectories: `TomosynthesisLinearTrajectory`, `TomosynthesisCircleTrajectory`, `TomosynthesisCrossTrajectory`, `TomosynthesisMultiArcTrajectory<N>`, `TomosynthesisEllipticalTrajectory` (C++17 and above)
- added two-dimensional data models:
    - definition of interface: `AbstractDataModel2D`
    - provide some implementations: `ConstantModel2D`, `RectModel2D`, `GaussianModel2D`
    - `SeparableProductModel`: special 2D model that takes two 1D models (`AbstractDataModel`) and represents the 2D product between those two
    - added a plot/inspection wigdet for 2D models (`DataModel2DViewer`)
- largely increased support for de-/serialization; almost everything is de-/serializable now
- `AbstractDetector` now provides two convenience methods to directly sample the input response (from its saturation model) and the spectral response (from the spectral response model); this is similar to how the spectrum can directly be sampled (from the spectrum model) in source components.
- added converter from specific system component types to their generic version (e.g. `XrayTube` --> `GenericSource`)
- added single parameter factory methods for prepare steps
- started to establish a collection of free functions in namespace `assist`
- enabled move-operations for projectors

### Changed
- large refactoring of data series and data models:
    - overall improvements to convenience and increased functionality for several data models
    - `PointSeriesBase` no longer has public data write access; this needs to be granted in derived classes, depending on the intended use case
    - `XYDataSeries` now allows full manipluation of the stored data
    - `IntervalDataSeries`
        - code refactoring / simplification, added some further convenience methods 
        - now discriminates two options of stored data: bin integral values or mean values; all relevant methods (e.g. `integral()` and `sum()`) consider this
    - added `GaussianModel1D`
    - promoted some models from `AbstractDataModel` to `AbstractIntegrableDataModel` (i.e. implemented `binIntegal()`)
    - `LineSeriesView`: now operates directly on `PointSeriesBase` (more flexibility)
    - changed some tags in serialization of parameters of some models (mainly upper/lower case consistency) - pay attention when using JSON files generated with previous versions
- enabled compatibility with OpenCL2
- `DataModelViewer`: now takes a copy of the visualized model (by means of `clone()`) - This brings this viewer in line with all other visualizers in the CTL that already owned the data they visualize.
- `RayCasterProjector`: non-interpolating case now uses OpenCL image types by default; version without image objects can be enabled through `useImageTypes` flag in settings.
- `GeometryDecoder`: decoding of detector location now uses location of a particular module (i.e. reference module) as reference - That means the full information about the location is no longer included only in the individual module locations but in the global detector location (as far as possible). Decoded systems should now behave way more like regular systems (where module locations are typically fixed and the detector location is what changes from view to view).
- `AbstractDetector`: changed skew coefficient to skew angle; this includes changes to how this is considered in `GeometryEncoder`
- `GenericDetector`: add `cloneWithOtherModuleLocations()` - This allows to create another GenericDetector with the same properties but changed module locations (this can e.g. be used when the number of modules needs to be changed)
- changed the order of parameters in some trajectories' ctors to make the more consistent with the others
- improved capabilities of the `Range<T>` class (e.g. `expspace()` and the option to exclude endpoints in `linspace()`/`expspace()`); use it more consistently throughout the CTL
- continued to add/extend documentation for several classes
- `ClFileLoader` now in 'global' `CTL` namespace (no longer in `CTL::OCL::`)
- global constants `PI`, `PI_2`, `PI_4` and `PIl` now in `CTL` namespace (no longer in root `::`)
- added `OpenCLFunction` and `OpenCLFunctions` classes for convenient (re-)use of smaller (assistive) functions within the OpenCL environment
- fixes:
    - `ShortScanTrajectory`: fix mistake in computation of fan angle
    - `CylindricalDetector`: fix behavior of `moduleLocations()` for very small module angulations (uses flat panel approximation now)
    - `AbstractDetector`: added missing enum value 'photoncounting' to meaning string for correct json serializer output
    - `PoissonNoiseExtension`: added checks for zero mean and skip affected values (avoids assert failure in MSVC)
    - `AbstractSource`: fix bug in `energyRange()` that could result in return values with end < start
    - `AbstractSource`: skip normalization for "collapsed" energy ranges (ie. start == end) to suppress warning
    - `PointSeriesBase`: `normalizeByMaxAbsVal()` now divides by the absolute value of the entry with largest absolute value


## 2020-10-13 (v.0.3.3)

FOCUS: Forward projection methods

### Added core functionality
- `SFPProjector`: separable footprint projector, a voxel-based forward projection method (as proposed by [Long, Fessler, and Balter](doi.org/10.1109/TMI.2010.2050898))
    - implementation of TR and TT footprint shape
    - provides atomic int and atomic float kernel versions: int version with internal conversion -> potential scaling issues but faster runtimes; float version is more flexible but can be slightly slower
    - features a novel generalized version `TT_Generic` that provides accurate results for a broader variety of geometries (e.g. twisted detector); also supports anisotropic voxel size (in all three dimensions)
    - sparse projection feature: can project sparse volume data with very high speed
- `SparseVoxelVolume`: container to hold sparse volume data; can convert to regular `VoxelVolume`
- `AbstractVolumeSparsifier` and `ThresholdVolumeSparsifier` - general interface and a simple threshold-based tool to generate a sparse version of a `VoxelVolume` object
- `AbstractProjector`: additions to interface
    - `configureAndProject()` - convenience method that fuses the calls to `configure()` and `project()` into a single step
    - `projectSparse()` - projection functionality for sparse volume data; default implementation available that converts to regular `VoxelVolume`
- `RayCasterProjectorCPU`: pure CPU version of the constant step length ray caster - now allows to generate projections with the CTL without the requirement for OpenCL (note, however, that using the CPU version may take a long time...)
- (bi-directional) iterators for `VoxelVolume`, `SingleViewData`, `ProjectionData`, and `SparseVoxelVolume`
- applications: added `ProjectionRegistration2D3D`: a class for DRR-based (rigid) 2D/3D registration; also added to the example project "regist2d3d".


### Changed / fixed
- changed the abstract interface of error metrics to raw pointer (instead of `std::vector`): this allows for more flexible use and eliminates some smaller performance drawbacks
- changed the meaning of "skew" in detector classes: it now represents the skew angle, i.e. the angle between the oblique pixel edge w.r.t. the corresponding rectangular shape. Before, this value directly stored the skew coefficient of the camera intrinsics, which is conceptually ill-posed, as this value scales with the source-detector distance, which is unknown to the detector component alone.
- fixed misspelling in `AbstractSerializer` method "deserializeAquisitionSetup" to "deserializeAcquisitionSetup" (missing 'c');
also fixed in all derived classes
- fixed mistake in computation of correlation error (`CorrelationError::operator()`)
- fixed mistake in computation of relative RMSE (`RelativeRMSE::operator()`)
- fixed geometry computation in `TubularGantry` class when using non-zero tilt angles in combination with table pitch
- renamed `RelativeGemanMcClure` error metric class to `NormalizedGemanMcClure`
- example classes `RayCaster` and `RayCasterAdapter` have been moved into namespace `external`
- `Range` class has been moved into separate file
- replaced deprecated `qrand()` by `std::rand()` in all tests

Other changes and additions:
GUI:
- `Chunk2DView`
    - can now directly plot `Matrix` objects
    - fixed potential crash that could occur when using zoom and pointing the cursor near the edge of the image
    - improved outcome of `autoResize()` in some situations
- `CTSystemView` now also visualizes the focal spot position (same for `AcquisitionSetupView`)
- increased maximum value for zoom that can be entered/displayed in the spinbox of the `ZoomControlWidget` from 25 to 99.9 (used in several plot widgets) - it was already possible to achieve higher zoom by use of Ctrl+Wheel command, but the zoom factor displayed in the widget was capped at 25.
Core:
- added limited-angle trajectories (tomosynthesis): (multi-) arc and circular versions
- `Matrix` class: add a few convenience methods: now features reshaping, `eye()` for non-square matrices, `fill()`, dot product with vectors, and a Hadamard (i.e. element-wise) product and division.
- added iterator overload of `operator()` to implemented sub-classes of `AbstractErrorMetric`: this allows convenient use of metrics  e.g. on `ProjectionData` objects (using the newly introduced iterators)
- added some convenience methods to `VoxelVolume` that allow querying of voxel indices (from world coordinates) and vice versa
- refactored `MessageHandler`
    - added features: whitelisting, `blacklistAll()`, `writeLogFileAndClear()`
    - added documentation
OpenCL:
- made improvements to pinned memory classes, added `PinnedBufHostReadWrite`
- made slight behavioral improvements to `VolumeSlicer`: "jumping planes" should now no longer occur


## 2020-04-23 (v.0.3.2)

FOCUS: GUI improvements

### Added core functionality
- added `Chunk2DView`: widget for visualization of `Chunk2D` data, also used for further widgets
- added new submodule 'gui_widgets_charts.pri' containing widgets for several "plot-like" visualizations:
  - `LineSeriesView`: visualizer for `XYDataSeries` data - a classic "x-y-style" plot
  - `IntervalSeriesView`: visualizer for `IntervalSeries` data - a "bar-style" plot
  - `DataModelViewer`: complex viewer for inspection of data models (any `AbstractDataModel` subclass) ; provides several visualization and manipulation options.
- included several interaction options with visualizers (e.g. windowing with mouse-gestures, plotting of contrast lines, saving screenshots); refer to the documentation of each class for full details.
- all visualizations can be invoked using a common function: `gui::plot()` with the input argument that shall be visualized, for maximal user convenience and slim code

### Changed
- moved all GUI classes to namespace `CTL::gui`
- fully reworked widget for visualization of projection data: `ProjectionViewer` (replaces previous ProjectionView)
- fully reworked widget for visualization of volume data: `VolumeViewer` (replaces previous VoxelVolumeView); now allows visualization of different volume types (i.e. `VoxelVolume<T>`, `SpectralVolumeData`, `CompositeVolume`)
- refactored widget for computation and visualization of arbitrary slices through volume data: `VolumeSliceViewer` (replaces previous VolumeSlicerWidget)
- renamed PipelineComposerWidget to `PipelineComposer`, added `DynamicProjectorExtension` as a possible extension
- renamed AcquisitionVisualizerWidget to `AcquisitionSetupView`
- renamed SystemVisualizerWidget to `CTSystemView`


Other changes and additions:
- changed spelling of `CTsystem` to `CTSystem` in all its occurrances (i.e. also in member methods and other classes such as `SimpleCTsystem`); old spelling will be kept as deprecated for a while, please consider renaming
- added `AbstractDynamicVolumeData::timeCurve()` method to extract values for all time points of a single voxel ("time-attenuation-curve")
- added `BasisFunctionVolume`: describes time dynamic by a linear combination of time-dependent basis functions 
- added convenience function `makeCTsystem()` and `makeSimpleCTsystem()` for creating systems from blueprints; add `AcquisitionSetup` constructors accepting `unique_ptr`
- implemented parallelization of arithmetic operations with `VoxelVolume<T>`
- added meta headers for each main module - you can now include a single meta header (e.g. ctl.h) to include all headers from the corresponding ctl module.

Note for developers who have already been using the dev_v0.3.2 branch: the namespace `CTL::gui::assist`, which previously contained the plot functions, has been dropped for the sake of simplicity. All `plot()` overloads can now be found in `CTL::gui` namespace.


## 2020-03-20 (v.0.3.1)

**Important change for existing users:**

`AbstractProjector::configure()` now only takes one parameter ('AcquisitionSetup'). All previous code must be adjusted accordingly - sorry for that!
Settings for the actual projector are now done using corresponding setter methods of the projector class itself.

### Added core functionality
FOCUS: Projector and Extension refactoring

Major convenience features:
- added a substantial amount of documentation and example code to many classes
- added two new convenience classes: `ProjectionPipeline` and `StandardPipeline`

    `ProjectionPipeline` provides a simple means to manage a projector along with an arbitrary number of `ProjectorExtension` objects. It allows for manipulations of the processing pipeline in a list-like fashion.
    `StandardPipeline` goes even one step further and provides a pre-defined pipeline that is easily configurable using some well-defined setter methods.

- added a fully-functional GUI widget (`PipelineComposerWidget`) to compose a `ProjectionPipeline` incl. all configuration options. This class also provides a simple dialog option (`PipelineComposerWidget::fromDialog()`), allowing you to create your projection pipeline on demand.

Overall, putting together your desired simulation settings should now be considerably simpler than before!

### Changed
Changes to projectors and extensions:
- corrected several errors
- all projectors and extensions are now serializable
- all extensions now provide constructors with direct member initialization
- removed `AbstractProjectorConfig` parameter from `AbstractProjector::configure()` - Specific settings for projectors must now be set by means of dedicated setter methods, similar to how it was done already with extensions.
- `ProjectorNotifier` now supports sending signals with general information as `QString`; included several such information messages in current extensions
- `SpectralEffectsExtension`
    - fully refactored code
    - enabled full compatibility for volume data without spectral information (this holds true also for `CompositeVolume` with mixed subvolumes, i.e. some containing spectral information and some not)
    - delegated extraction of spectral information to `RadiationEncoder`
- `PoissonNoiseExtension`
    - add Gaussian approximation for large photon counts - This change increases computation speed for large counts and avoids some numerical issues with the Poisson distribution of extemely large mean.
- `ArealFocalSpotExtension`
    - now correctly defined to be non-linear
    - added option to perform linear approximation (useful for projections with small extinction gradients)
- `DynamicProjectorExtension` (formerly DynamicProjector)
    - refactored: now of base class `ProjectorExtension`
    - can now properly handle different types of volume data and nested projectors
    - supports CompositeVolumes with mixed subvolumes (i.e. some containing temporal dynamics and some static volumes)

Other changes:
- refactoring of volume data classes:
   - volume classes are now polymorphic
   - added more factory methods to create volume objects with basic geometrical shapes
   - refactored `CompositeVolume`: now much more flexible to use
   - refactored `AbstractDynamicVolumeData` (formerly AbstractDynamicVoxelVolume): now of base class `SpectralVolumeData`
   - added `LinearDynamicVolume`: simple class allowing for linear relation of temporal dependency of attenuation in each voxel
- refactored `TASMIPSpectrumModel`: now more performant and memory-saving
- `RadiationEncoder` can now extract spectral information for an entire setup
- new submodule (gui_widgets_ocl.pri) containing widgets with OpenCL dependency
- added Gaussian filter methods
- add method to directly query the mean energy of a source's spectrum
- add option to connect messages from (Qt) signals to the `MessageHandler`


## 2020-02-03 (v.0.3)

### Added core functionality
FOCUS: Image processing
- Radon transformation of images and volumes on GPU (RadonTransform2D, RadonTransform3D) [reference paper: SPIE Medical Imaging 2020 - ID 11313-87; available soon]
- methods for differentiation and filtering of images and volumes (e.g. Gaussian blur, median, Ram-Lak, central difference, Savitzky-Golay, spectral derivatives)
- fast resampling of images and volume on GPU (ImageResampler, VolumeResampler)
- Grangeat consistency measures:
    - intermediate function computation
    - intermediate function pair generation (projection-volume, projection-projection)
- error metrics: compute different error measures between two 1D datasets (L1, L2, RMSE, correlation, cosine similarity, Geman-McClure)
- VolumeSlicer: allows slicing of voxelized volumes along arbitrary planes
- VolumeDecomposer: decompose voxelized volumes of attenuation coefficients into (multiple) volumes of density for different materials
    - generic interface + specific implementation using data models (ModelBasedVolumeDecomposer)
    - simple threshold based segmentation for two materials (TwoMaterialThresholdVolumeDecomposer)

Polychromatic simulations
- AttenuationFilter class: beam modifier applying monomaterial Lambert-Beer attenuation (incl. full consideration in projections)
- RadiationEncoder class: manages information on radiation - ie. flux and spectrum - of CT system as a whole (analogue to GeometryEncoder for system geometry)
- TASMIP X-ray spectrum model (realistic X-ray spectra for tungsten tubes up to 140 keV)
- spectral response for detector (incl. consideration in projector extension)
- add option to (artificially) restrict energy range of source components (main use in spectral projections)

Miscellaneous
- MessageHandler: fully customize messages (control verbosity, appearance etc.) in CTL; can log to file
- more data models: step functions (e.g. constant, rect)
- operators on data models (arithmetics, concatenation etc.)
- database now includes material densities
- homography class (matrix subclass)
- range & coordinate classes

### Changed
- new structure for CTL modules: strictly dependency-based main modules + semantically-grouped sub modules
- refactored AbstractBeamModifier class: new interface with separate modification of flux and spectrum
- refactored XrayTube class: now always uses a TASMIP model for spectrum and flux estimation
- refactored SpectralVolumeData: can now retain spectral information when managing mu values instead of material densities
- renamed SpectralProjectorExtension to SpectralEffectsExtension, as it now considers several spectral effects

### Widgets and Apps 
- VolumeSlicerWidget: GUI widget to conveniently make use of new VolumeSlicer class (incl. visualization of the slice location)
- ProjectorAssemblyWidget: provides assessment of (physical) meaningfulness/exactness of projector extension sequences; can generate corresponding source code
- [regist2d3d.pro] 2D/3D registration app: full toolchain to register geometry of a 2D projection image to a given volume intermediate function (Grangeat representation of a voxelized volume) [reference paper: currently under review]
- [radon3d.pro] 3D intermediate function (pre-)computation app: transforms a voxelized volume to its (Grangeat) intermediate space


## 2019-05-07 (v.0.2)

### Added
- major
    - SpectralProjectorExtension: allows for computation of polychromatic projections
    - NRRD file support (read and write)
    - physical volume objects 
        - SpectralVolumeData: combines density data with a model for spectrally-dependent absorption coefficient
        - CompositeVolume: container for multiple SpectralVolumeData objects
    - projectComposite() option in projectors: can compute projections from composite volumes
    - database with many tabulated absorption spectra and an exemplary X-ray spectrum
- minor
    - global gantry displacement
    - GenericDetector class now supports changing pixel size and skew coefficient
    - dedicated classes for SingleViewGeoemetry and FullGeometry
    - HeuristicCubicSpectrumModel: simple model for approximate representation of Xray tube spectra
    - fixed seed option in PoissonNoiseExtension
    - sources now provide the energy range of their emitted radiation

### Changed
- redesigned spectrum queries from source components
- new design for ProjectorExtension
- reworked calculation of photon flux in source components: now uses realistic properties of tube and laser


## 2019-02-20

### Added
- complete de-/serialization of AcquisitionSetup and all related classes
- JSON & binary serializer
- detector saturation models
- flying focal spot protocol
- axial scan trajectory
- comparison of projection matrices wrt the projection error: PMatComparator
- abstract IO for writing SingleView & SingleViewGeometry

### Changed
- structure of data models and series


## 2019-01-21

### Added
- RayCasterProjector supports Multi-OpenCL-Device acceleration

### Changed
- RayCasterProjector::Config: has a device list instead of a device ID
