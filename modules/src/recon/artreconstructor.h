#ifndef CTL_ARTRECONSTRUCTOR_H
#define CTL_ARTRECONSTRUCTOR_H

#include "abstractreconstructor.h"
#include "abstractsubsetgenerator.h"
#include "acquisition/acquisitionsetup.h"
#include "processing/abstractvolumefilter.h"
#include "projectors/abstractprojector.h"
#include "defaultsubsetgenerator.h"

#include <memory>

namespace CTL {

/*!
 * \class ARTReconstructor
 * \brief The ARTReconstructor class is an implementation of the algebraic reconstruction technique
 * that can utilize user-defined forward and backprojectors.
 *
 * This class implements image reconstruction by means of the algebraic reconstruction technique
 * (ART). In brief, the routine alternates between computation of forward projections from the
 * current volume estimate, comparing these with the projection data that shall be reconstructed,
 * and backproject the difference between both dataset into the volume domain to achieve a better
 * estimate. The routine is repeated until a stopping criterion is reached.
 *
 * Both the forward and the backprojector that are used within the iterative scheme can be provided
 * by the user to grant full flexibility. By default, ARTReconstructor uses OCL::RayCasterProjector
 * and OCL::SimpleBackprojector as its forward and backprojector, respectively.
 * In addition, the ART scheme can be used in combination  with the so-called ordered subsets (OS)
 * routine. When using OS, instead of performing the  aforementioned alternating pattern of forward
 * and backprojection on the entire set of projections, a subset of projections is selected and used
 * to perform the update step. This is done for all available subsets (usually constructed such that
 * all projections are processed once). The generation of subsets for the OS routine is done by a
 * subset generator, which can be a custom type provided by the user as well. Alternatively, a
 * default subset generator, featuring a wide range of possibilities, is also available (see
 * defaultSubsetGenerator() and DefaultSubsetGenerator).
 *
 * The fraction of the difference between simulated and actual projection data that gets
 * backprojected is controlled with the so-called relaxation parameter. This parameter can either
 * be provided manually (see setRelaxation()) or estimated using an approach based on the so-called
 * power method. By default, relaxation parameter estimation is enabled. This can be changed by
 * means of setRelaxationEstimationEnabled().
 * For details on the estimation procedure, please refer to computeRelaxationEstimate().
 *
 * ARTReconstructor also supports regularization. A custom regularizer can be set via
 * setRegularizer(). The regularization operation (i.e. volume filtering) is applied after each
 * backprojection operation. Use of regularization can be enabled/disabled be means of
 * setRegularizationEnabled().
 *
 * By default, ARTReconstructor uses a positivity constraint on the volume estimate that is applied
 * immediately after (a potential) regularization, i.e. after backprojecting a subset. This
 * constraint means that all values in the volume estimate that are negative will be set to zero.
 * You can enable/disable the use of the positivity constraint by means of
 * setPositivityConstraintEnabled().
 *
 * **Stopping criteria:**
 * The ARTReconstructor supports six different stopping criteria, namely
 * - ARTReconstructor::MaximumNbIterations,
 * - ARTReconstructor::MaximumTime,
 * - ARTReconstructor::ProjectionErrorChange,
 * - ARTReconstructor::VolumeDomainChange,
 * - ARTReconstructor::RelativeProjectionError, and
 * - ARTReconstructor::NormalEquationSatisfied.
 *
 * The individual criteria are explained in their respective documentation (see ARTReconstructor::StoppingCriterion).
 * Parameters for
 * individual criteria can be set through a range of dedicated setter and the corresponding
 * criteria can be enabled/disabled as desired (see setStoppingCriterionEnabled()). It is also
 * possible to use any combination of stopping criteria (see setStoppingCriteria()). Stopping
 * criteria are evaluated at the end of a full iteration (i.e. after processing all subsets). As
 * soon as (at least) one enabled criterion is reached, the reconstruction is stopped. If required,
 * for special purposes, reconstruction can also be carried out without any stopping criterion
 * enabled at all. Note that calls to reconstruct() (or any other version of reconstruction call)
 * will not terminate in such a setting.
 */
class ARTReconstructor : public AbstractReconstructor
{
    CTL_TYPE_ID(200)
    Q_GADGET

public:
    enum StoppingCriterion
    {
        NoStoppingCriterion     = 0,

        MaximumNbIterations     = 1 << 0,
        MaximumTime             = 1 << 1,
        ProjectionErrorChange   = 1 << 2,
        VolumeDomainChange      = 1 << 3,
        RelativeProjectionError = 1 << 4,
        NormalEquationSatisfied = 1 << 5,

        AllStoppingCriteria = MaximumNbIterations |
                              MaximumTime |
                              ProjectionErrorChange |
                              VolumeDomainChange |
                              RelativeProjectionError |
                              NormalEquationSatisfied
    };
    Q_DECLARE_FLAGS(StoppingCriteria, StoppingCriterion)
    Q_FLAG(StoppingCriteria)

    ARTReconstructor();
    ARTReconstructor(std::unique_ptr<AbstractProjector> forwardProjector,
                     std::unique_ptr<AbstractReconstructor> backprojector);
    ARTReconstructor(AbstractProjector* forwardProjector,
                     AbstractReconstructor* backprojector);

    public: void configure(const AcquisitionSetup& setup) override;
    public: bool reconstructToPlain(const ProjectionDataView& projections,
                                    VoxelVolume<float>& targetVolume) override;
    public: bool reconstructToSpectral(const ProjectionDataView& projections,
                                       SpectralVolumeData& targetVolume) override;
public:
    // SerializationInterface interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;
    QVariant toVariant() const override;

    static float computeRelaxationEstimate(const AcquisitionSetup& setup,
                                           const VoxelVolume<float>& targetVolume,
                                           std::shared_ptr<AbstractProjector> forwardProjector,
                                           std::shared_ptr<AbstractReconstructor> backprojector,
                                           uint nbPowerIter = 1u);
    static float computeRelaxationEstimate(const AcquisitionSetup& setup,
                                           const VoxelVolume<float>& targetVolume,
                                           AbstractProjector* forwardProjector = nullptr,
                                           AbstractReconstructor* backprojector = nullptr,
                                           uint nbPowerIter = 1u);

    // setter
    void setPositivityConstraintEnabled(bool enabled);
    void setRegularizationEnabled(bool enabled);
    void setRelaxationEstimationEnabled(bool enabled);

    // relaxation setters -> deactivate automatic estimation when used
    void setRelaxation(float relax);
    void setRelaxationByEstimation(const VoxelVolume<float>& targetVolume,
                                   uint nbPowerIter = 1u);
    void setRelaxationByEstimation(const AcquisitionSetup& setup,
                                   const VoxelVolume<float>& targetVolume,
                                   uint nbPowerIter = 1u);

    void setRegularizer(AbstractVolumeFilter* regularizer, bool enableRegularization = true);
    void setRegularizer(std::unique_ptr<AbstractVolumeFilter> regularizer,
                        bool enableRegularization = true);
    void setSubsetGenerator(AbstractSubsetGenerator* generator);
    void setSubsetGenerator(std::unique_ptr<AbstractSubsetGenerator> generator);
    void setForwardProjector(std::unique_ptr<AbstractProjector> projector);
    void setBackprojector(std::unique_ptr<AbstractReconstructor> projector);
    void setForwardProjector(AbstractProjector* projector);
    void setBackprojector(AbstractReconstructor* projector);
    void useCustomSubsetGenerator();
    void useDefaultSubsetGenerator();

    // stopping criteria
    void setMaxNbIterations(uint nbIt, bool enableCriterion = true);
    void setMaxTime(float seconds, bool enableCriterion = true);
    void setMinChangeInVolumeDomain(float minRelativeChange, bool enableCriterion = true);
    void setMinChangeInProjectionError(float minRelativeChange, bool enableCriterion = true);
    void setMinRelativeProjectionError(float minRelativeError, bool enableCriterion = true);
    void setNormalEqTolerance(float relativeTol, bool enableCriterion = true);
    void setStoppingCriteria(int criteria);
    void setStoppingCriterionEnabled(StoppingCriterion criterion, bool enabled = true);


    // getter
    bool isCustomSubsetGeneratorInUse() const;
    bool isDefaultSubsetGeneratorInUse() const;
    bool isPositivityConstraintEnabled() const;
    bool isRelaxationEstimationEnabled() const;
    bool isRegularizationEnabled() const;
    bool isStoppingCriterionEnabled(int criteria) const;
    float relaxation() const;
    int stoppingCriteria() const;
    AbstractVolumeFilter& regularizer() const;
    AbstractSubsetGenerator& customSubsetGenerator() const;
    DefaultSubsetGenerator& defaultSubsetGenerator() const;
    AbstractSubsetGenerator& subsetGenerator() const;

private:
    AcquisitionSetup _setup;
    std::unique_ptr<AbstractProjector> _fp;
    std::unique_ptr<AbstractReconstructor> _bp;
    std::unique_ptr<AbstractVolumeFilter> _regularizer;
    std::unique_ptr<AbstractSubsetGenerator> _customSubsetGen;
    std::unique_ptr<DefaultSubsetGenerator> _defaultSubsetGen;
    AbstractSubsetGenerator* _activeSubsetGen;

    // parameters
    uint _maxNbIterations         = 5;
    bool _usePositivityConstraint = true;
    bool _useRegularization       = false;
    bool _useRelaxationEstimation = true;
    float _relax                  = 1.0e-6f; //!< relaxation times number of views in setup (will be divided by subset size)
    float _terminateNormalEqTol   = 0.001f;  //!< relative change
    float _terminateVolChange     = 0.001f;  //!< relative change
    float _terminateProjErrChange = 0.01f;   //!< relative change
    float _terminateProjErrRel    = 0.01f;   //!< relative error (wrt. input data)
    uint  _terminateMaxTime       = 60000u;  //!< in ms
    int _stoppingCriteria = MaximumNbIterations;

    bool consistencyChecks(const ProjectionDataView& projections) const;

    // stopping criteria methods
    bool hasAnyStoppingCriterion() const;
    bool stoppingCriterionReached(const VoxelVolume<float>& oldVol,
                                  const VoxelVolume<float>& newVol,
                                  float oldProjError,
                                  float newProjError,
                                  float inputProjNorm,
                                  float normalEqNormalization,
                                  int msSpent,
                                  uint iteration) const;
    bool terminateByComputationTime(int msSpent) const;
    bool terminateByIterationCount(uint iteration) const;
    bool terminateByProjError(float projError, float inputProjNorm) const;
    bool terminateByProjErrorChange(float oldProjError, float newProjError) const;
    bool terminateByVolumeChange(const VoxelVolume<float>& oldVol,
                                 const VoxelVolume<float>& newVol) const;
    bool terminateByNormalEqTol(const VoxelVolume<float>& oldVol,
                                const VoxelVolume<float>& newVol,
                                float normalization) const;

    bool reconstructData(const ProjectionDataView& projections,
                         SpectralVolumeData& targetVolume,
                         AbstractProjector& fp,
                         AbstractReconstructor& bp);

    static float computeRelaxationEstimate(const AcquisitionSetup& setup,
                                           const VoxelVolume<float>& targetVolume,
                                           AbstractProjector& fp,
                                           AbstractReconstructor& bp,
                                           uint nbPowerIter);
    static QMetaEnum metaEnum();
};

Q_DECLARE_OPERATORS_FOR_FLAGS(ARTReconstructor::StoppingCriteria)

/*!
 * \class ARTReconstructorSFP
 * \brief The ARTReconstructorSFP class is a convenience class that creates an instance of
 * ARTReconstructor with forward and backprojectors based on the separable footprint method (SFP).
 */
class ARTReconstructorSFP : public ARTReconstructor
{
public:
    enum FootprintType { TR, TT, TT_Generic };
    explicit ARTReconstructorSFP(FootprintType footprintType = TR);
    ARTReconstructorSFP(FootprintType footprintTypeFP, FootprintType footprintTypeBP);
};

namespace assist {
void enforcePositivity(VoxelVolume<float>& volume);
} // namespace assist
} // namespace CTL

#endif // CTL_ARTRECONSTRUCTOR_H
