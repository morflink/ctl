#ifndef CTL_POINTSERIESBASE_H
#define CTL_POINTSERIESBASE_H

#include <QList>
#include <QPointF>
#include <vector>

namespace CTL {

template<typename> class Range;

/*!
 * \brief The PointSeriesBase class is the base class used to represent a series of data tuples.
 *
 * This class stores data tuples (sampling point and the corresponding value) as a QList<QPointF>.
 * This is done to achieve straightforward compatability with QCharts for visualization purposes.
 *
 * Data managed by this class cannot be modified through its public interface. Manipulation options,
 * e.g. the possibility to add/remove data points, needs to be provided by dedicated sub-classes.
 *
 * A number of typical 'inspection' methods is provided. These methods operate on the values (i.e.
 * 'y' part) stored int the tuples. The collection includes min() / max() queries and computation of
 * sum() and weightedSum(), as well as individual queries for the full range covered by either the
 * sampling points (i.e. 'x' part) or the values in the series, see samplingRange() / valueRange().
 *
 * Protected member methods are available to scale and normalize values in the series. Consider
 * turning them public in sub-classes if appropriate.
 *
 * Currently, the CTL contains two dedicated sub-classes: XYDataSeries and IntervalDataSeries.
 * Please refer to the individual documentation for their specific purpose.
 */
class PointSeriesBase
{
public:
    // getter methods
    const QList<QPointF>& data() const;

    // other methods
    float max() const;
    float min() const;
    uint nbSamples() const;
    uint size() const;
    float sum() const;
    float sum(const std::vector<float>& weights) const;
    float weightedSum(const std::vector<float>& weights) const;

    // Sampling points ("x values")
    float samplingPoint(uint sampleNb) const;
    std::vector<float> samplingPoints() const;
    Range<float> samplingRange() const;

    // Values ("y values")
    float value(uint sampleNb) const;
    std::vector<float> values() const;
    Range<float> valueRange() const;

protected:
    // ctors
    PointSeriesBase() = default;
    explicit PointSeriesBase(const QList<QPointF>& pointSeries);
    explicit PointSeriesBase(QList<QPointF>&& pointSeries);

    // data manipulation
    void normalizeByMaxAbsVal();
    void normalizeByMaxVal();
    void scaleValues(float factor);

    QList<QPointF> _data;
};



} // namespace CTL

#endif // CTL_POINTSERIESBASE_H
