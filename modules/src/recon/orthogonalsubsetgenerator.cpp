#include "orthogonalsubsetgenerator.h"
#include "acquisition/acquisitionsetup.h"
#include "components/abstractgantry.h"

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(OrthogonalSubsetGenerator)

/*!
 * \brief Creates an OrthogonalSubsetGenerator that generates \a nbSubsets subsets.
 *
 * The number of subsets to create may be changed later on through setNbSubsets().
 * Please note that this subset generator requires a valid AcquisitionSetup that corresponds to the
 * projections that shall be partitioned into subsets before requesting the subsets.
 *
 * Example: creating four subsets for a set of 36 views from a WobbleTrajectory
 * \code
 *  constexpr uint nbViews = 36;
 *
 *  // first, we create the AcquisitionSetup with 'nbViews'
 *  AcquisitionSetup setup(makeSimpleCTSystem<blueprints::GenericCarmCT>(), nbViews);
 *  // here, we decide for a WobbleTrajectory as our acquisition geometry
 *  setup.applyPreparationProtocol(protocols::WobbleTrajectory(700.0));
 *
 *  // create some dummy projections (the actual dimensions are irrelevant here)
 *  ProjectionData dummyProjections(1,1,1);
 *  dummyProjections.allocateMemory(nbViews); // dummyProjections now contains 'nbViews' views
 *
 *  // create our subset generator with four subsets
 *  OrthogonalSubsetGenerator subsetGen(4);
 *
 *  // pass both projection data and the correponding setup to the generator
 *  subsetGen.setData(dummyProjections, setup);
 *
 *  // create the subsets (note: iteration '0' is entirely arbitrary and has no particular effect)
 *  const auto subsets = subsetGen.generateSubsets(0);
 *
 *  // print the ids of the views that have been assigned to our four subsets
 *  for(const auto& subset : subsets)
 *      qInfo() << subset.viewIds();
 *
 *      // output:
 *      // std::vector(31, 14, 29, 12, 30, 6, 32, 4, 3)
 *      // std::vector(0, 16, 26, 10, 23, 8, 1, 17, 25)
 *      // std::vector(33, 15, 22, 5, 21, 7, 27, 13, 28)
 *      // std::vector(35, 19, 9, 24, 2, 18, 34, 20, 11)
 * \endcode
 */
OrthogonalSubsetGenerator::OrthogonalSubsetGenerator(uint nbSubsets)
    : AbstractFixedSizeSubsetGenerator(nbSubsets)
{
}

/*!
 * \brief Implementation of the subset generation routine.
 *
 * This generates the subsets for the data that have been set through setData() (or individual calls
 * of setSetup() and setProjections()). Please note that setting a valid AcquisitionSetup that
 * corresponds to the projections is required before this method is called (i.e. before requesting
 * the subsets).
 *
 * Generates the same subsets independent of the \a iteration input.
 *
 * The subset generation routine implemented by this class depends on the acquisition geometry
 * referring to the projections that shall be divided into subsets:
 * In order to assign individual views to a particular subset, the degree of orthogonality between
 * the view directions (i.e. the source-to-detector vector) of all views already included in the
 * subset and all remaining candidates (i.e. views from the full projection data that are not yet
 * assigned to any subset) is computed. The candidate maximizing the average orthogonality is then
 * subsequnetly added to the subset under construction and the full procedure is repeated until the
 * required number (subject to the requested number of subsets) of views is reached.
 *
 * Note that this procedure is solely based on the orthogonality of the direction vectors. This
 * means, in particular, that shifted view positions are not considered at all when determining
 * their information content for the purpose of their assignment to a subset.
 */
std::vector<ProjectionDataView> OrthogonalSubsetGenerator::generateSubsetsImpl(uint) const
{
    // check if configured number of views contains all IDs occuring in the projections.
    const auto& viewIds = _fullProjections.viewIds();
    if(std::any_of(viewIds.cbegin(), viewIds.cend(),
                   [this] (uint viewID) { return viewID >= _directionVectors.size(); } ))
    {
       throw std::domain_error("OrthogonalSubsetGenerator::generateSubsetsImpl(): Subset generation "
                               "aborted. Reason: projection data view contains a view ID that "
                               "exceeds the number of views in the configured setup.");
    }

    std::vector<Candidate> candidates;
    candidates.reserve(_fullProjections.nbViews());

    auto locIdx = 0u;
    for(auto id : _fullProjections.viewIds())
        candidates.push_back( { _directionVectors[id], locIdx++ } );

    std::vector<ProjectionDataView> subsets;
    subsets.reserve(_nbSubsets);

    for(auto size : assist::subsetSizes(_fullProjections.nbViews(), _nbSubsets))
        subsets.push_back(createSubset(candidates, size));

    return subsets;
}

/*!
 * \copydoc AbstractSubsetGenerator::setSetup
 *
 * This class extracts only the view directions, i.e. the vector pointing from the source position
 * towards the detector position, from \a setup.
 */
void OrthogonalSubsetGenerator::setSetup(const AcquisitionSetup& setup)
{
    _directionVectors.clear();
    _directionVectors.resize(setup.nbViews());

    AcquisitionSetup setupCopy(setup);
    for(auto view = 0u, nbViews = setup.nbViews(); view < nbViews; ++view)
    {
        setupCopy.prepareView(view);
        const auto gantryPtr = setupCopy.system()->gantry();
        const auto srcDetVec = gantryPtr->sourcePosition() - gantryPtr->detectorPosition();

        _directionVectors[view] = srcDetVec.normalized();
    }
}

/*!
 * \brief Creates a subset from the data managed by this instance.
 *
 * This selects \a subsetSize views from the list of available \a candidates, w.r.t. the maximum
 * orthogonality approach described in generateSubsetsImpl().
 */
ProjectionDataView OrthogonalSubsetGenerator::createSubset(std::vector<Candidate>& candidates,
                                                           uint subsetSize) const
{
    if(candidates.size() < subsetSize)
    {
        qWarning() << "OrthogonalSubsetGenerator: Trying to create subset with more views than "
                      "available. Returning all remaining views.";
        std::vector<uint> viewIds;
        viewIds.reserve(candidates.size());
        for(const auto& cand : candidates)
            viewIds.push_back(cand.viewID);

        return _fullProjections.subset(viewIds);
    }

    std::vector<uint> memberViewIds;
    std::vector<mat::Matrix<3,1>> memberDirections;
    memberViewIds.reserve(subsetSize);
    memberDirections.reserve(subsetSize);

    auto scalarProdSum = [&memberDirections] (const Candidate& first, const Candidate& second)
    {
        double sum1 = 0.0, sum2 = 0.0;
        for(const auto& member : memberDirections)
        {
            sum1 += std::pow(mat::dot(member, first.direction), 2.0);
            sum2 += std::pow(mat::dot(member, second.direction), 2.0);
        }

        return sum1 < sum2;
    };

    // add first element from candidates to current subset
    memberDirections.push_back(candidates.front().direction);
    memberViewIds.push_back(candidates.front().viewID);
    // remove this element from list of candidates (same effect as erase)
    candidates.front() = candidates.back(); candidates.pop_back();

    // consecutively, add candidate that is most orthogonal to current members of the subset
    for(auto s = 0u; s < subsetSize - 1; ++s)
    {
        auto mostOrthoCand = std::min_element(candidates.begin(), candidates.end(), scalarProdSum);
        memberDirections.push_back(mostOrthoCand->direction);
        memberViewIds.push_back(mostOrthoCand->viewID);
        // remove this element from list of candidates (same effect as erase)
        *mostOrthoCand = candidates.back(); candidates.pop_back();
    }

    return _fullProjections.subset(memberViewIds);
}

} // namespace CTL
