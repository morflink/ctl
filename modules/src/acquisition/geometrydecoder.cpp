#include "geometrydecoder.h"
#include "acquisition/preparesteps.h"
#include "components/allgenerictypes.h"

namespace CTL {

/*!
 * Constructs a GeometryDecoder object.
 */
GeometryDecoder::GeometryDecoder(const QSize &pixelPerModule, const QSizeF &pixelDimensions)
    : _pixelPerModule(pixelPerModule)
    , _pixelDimensions(pixelDimensions)
{
}

GeometryDecoder::GeometryDecoder(const QSize &pixelPerModule, PhysicalDimension physicalDimension, double mm)
    : _pixelPerModule(pixelPerModule)
    , _physicalDimensionReference(qMakePair(physicalDimension , mm))
{

}

/*!
 * See
 * decodeFullGeometry(const FullGeometry& geometry, const QSize& pixelPerModule, const QSizeF& pixelDimensions).
 * It uses the internal `_pixelPerModule` and `_pixelDimensions` of the object.
 */
AcquisitionSetup GeometryDecoder::decodeFullGeometry(const FullGeometry& geometry) const
{
    return _pixelDimensions.isValid() ? decodeFullGeometry(geometry, _pixelPerModule,
                                                           _pixelDimensions,
                                                           _referenceModule)
                                      : decodeFullGeometry(geometry, _pixelPerModule,
                                                           _physicalDimensionReference.first,
                                                           _physicalDimensionReference.second,
                                                           _referenceModule);
}

SimpleCTSystem
GeometryDecoder::decodeSingleViewGeometry(const SingleViewGeometry& singleViewGeometry,
                                          const QSize& pixelPerModule,
                                          GeometryDecoder::PhysicalDimension physicalDimension,
                                          double mm,
                                          int referenceModuleID)
{
    const auto referencePmat = singleViewGeometry.first();
    const auto pixelDimensions = computePixelSize(referencePmat, physicalDimension, mm);

    // X-ray source position
    const auto srcPos = referencePmat.sourcePosition();

    // detector module locations
    auto moduleLocations = computeModuleLocations(
        singleViewGeometry, srcPos, pixelPerModule, pixelDimensions.width());

    // gantry contains source location and detector location of reference module
    const auto srcRot = computeSourceRotation(moduleLocations, srcPos);
    const auto sourceLocation = mat::Location(srcPos, srcRot);
    const auto refLoc = referenceModuleLocation(moduleLocations, referenceModuleID);

    GenericGantry gantry(sourceLocation, refLoc);

    // detector has information about the relative location of its modules wrt. the reference module
    adaptModuleLocationsToReference(moduleLocations, refLoc);
    GenericDetector detector(pixelPerModule, pixelDimensions, std::move(moduleLocations));
    detector.setSkewAngle(referencePmat.skewAngle());

    return { std::move(detector), std::move(gantry), GenericSource() };
}

/*!
 * Returns matrix that rotates source perpendicular to the centroid of detector locations
 */
Matrix3x3 GeometryDecoder::computeSourceRotation(const QVector<mat::Location>& moduleLocations,
                                                 const Vector3x1& sourcePosition)
{
    // centroid of detector locations
    Vector3x1 centroid(0.0);
    for(const auto& modLoc : moduleLocations)
        centroid += modLoc.position;
    centroid /= moduleLocations.length();

    // vector from source to detector centroid
    Vector3x1 src2centroid = centroid - sourcePosition;
    src2centroid.normalize();

    // construct a rotation matrix that rotates the z-axis to `src2centroid` vector, i.e. it needs
    // to have the form
    // [ r1 r2 src2centroid ]
    // with r1 and r2 perpendicular to src2centroid

    // find axis that is as most as perpendicular to this vector
    uint axis = std::fabs(src2centroid.get<0>()) < std::fabs(src2centroid.get<1>()) ? 0 : 1;
    axis = std::fabs(src2centroid(axis)) < std::fabs(src2centroid.get<2>()) ? axis : 2;

    // init 1st vector with this axis
    Vector3x1 r1(0.0);
    r1(axis) = 1.0;

    // define canonical unit vectors
    mat::Matrix<1, 3> e1(1,0,0);
    mat::Matrix<1, 3> e2(0,1,0);
    mat::Matrix<1, 3> e3(0,0,1);

    // prepare cross product
    auto M = mat::vertcat(src2centroid.transposed(), r1.transposed());
    // 1st cross product
    r1 = { mat::det(mat::vertcat(e1,M)), mat::det(mat::vertcat(e2,M)), mat::det(mat::vertcat(e3,M)) };

    // prepare cross product
    M = mat::vertcat(src2centroid.transposed(), r1.transposed());
    // 2nd cross product
    Vector3x1 r2{ mat::det(mat::vertcat(e1,M)), mat::det(mat::vertcat(e2,M)), mat::det(mat::vertcat(e3,M)) };

    return mat::horzcat(r1, mat::horzcat(r2, src2centroid));
}

mat::Location GeometryDecoder::referenceModuleLocation(const QVector<mat::Location>& moduleLocations,
                                                       int referenceModuleID)
{
    // automatic choice (central module)
    if(referenceModuleID < 0)
        referenceModuleID = static_cast<int>(moduleLocations.size() / 2);

    if(referenceModuleID >= moduleLocations.size())
        throw std::runtime_error("GeometryDecoder::applyReferenceModule: reference module id "
                                 "exceeds number of modules in singleViewGeometry.");

    return moduleLocations.at(referenceModuleID);
}

void GeometryDecoder::adaptModuleLocationsToReference(QVector<mat::Location>& moduleLocations,
                                                      const mat::Location& referenceModule)
{
    const auto refRot = referenceModule.rotation;

    // invert encoding process, considering the following:
    /* referenceModule.location -> detectorPos
     * referenceModule.rotation -> detectorRot
     * loc.position -> modulePos
     * loc.rotation -> totalRot
     *
     * This is the original encoding (to be inverted):
     *  | Vector3x1WCS modulePos = detectorPos + detectorRot_T * modLoc.position;
     *  | Matrix3x3 totalRot = modLoc.rotation * detectorRot;
    */
    for(auto& loc : moduleLocations)
    {
        loc.position = refRot * (loc.position - referenceModule.position);
        loc.rotation = loc.rotation * refRot.transposed();
    }
}

/*!
 * Decodes the set of projection matrices in \a geometry and constructs a GenericAcquisitionSetup
 * that represents all the geometry information that has been extracted.
 *
 * Besides projection matrices, information about the detector dimensions (i.e. number of pixels
 * in each module and their (physical) size) is required. The number of individual flat panel
 * modules is automatically readout from \a geometry.
 *
 * This method constructs a SimpleCTSystem consisting of a GenericSource, GenericDetector and a
 * GenericGantry component.
 *
 * Some remarks on system configuration: All source settings remain at default values (i.e. source
 * spectrum, focal spot size and focal spot position). Consider changing these afterwards, if
 * required. The full geometry information regarding the detector is stored in the location
 * specification of the individual detector modules. In particular, this means that the (global)
 * detector positioning - as queried for example by GenericGantry::detectorPosition() - will carry
 * no information (position defaults to (0,0,0) and rotation to identity matrix). Additionally, the
 * rotation of the source component cannot be determined without further information. Hence, it
 * remains at default value (i.e. identity matrix).
 */
AcquisitionSetup GeometryDecoder::decodeFullGeometry(const FullGeometry& geometry,
                                                     const QSize& pixelPerModule,
                                                     const QSizeF& pixelDimensions,
                                                     int referenceModuleID)
{
    // construct a generic system
    CTSystem theSystem;

    uint nbModules = 0;
    if(geometry.size() == 0u)
        qWarning() << "empty geometry passed (no views available)";
    else
        nbModules = geometry.first().nbModules();

    auto detector = makeComponent<GenericDetector>(pixelPerModule, nbModules);
    detector->setPixelSize(pixelDimensions);
    auto source = makeComponent<GenericSource>(QSizeF(0.0, 0.0), Vector3x1(0.0));
    auto gantry = makeComponent<GenericGantry>();

    theSystem << std::move(source) << std::move(detector) << std::move(gantry);

    AcquisitionSetup ret(theSystem);

    // extract geometry information
    for(const SingleViewGeometry& view : geometry)
    {
        auto srcPos = view.first().sourcePosition();
        auto moduleLocations = computeModuleLocations(view, srcPos, pixelPerModule,
                                                      pixelDimensions.width());

        // prepare steps gantry
        auto gantrySetter = std::make_shared<prepare::GenericGantryParam>();
        auto srcRot = computeSourceRotation(moduleLocations, srcPos);
        gantrySetter->setSourceLocation(mat::Location(srcPos, srcRot));

        // note: refer position/rotation information of entire detector to reference module
        const auto refLoc = referenceModuleLocation(moduleLocations, referenceModuleID);
        gantrySetter->setDetectorLocation(refLoc);
        adaptModuleLocationsToReference(moduleLocations, refLoc);

        // detector prepare steps
        auto detectorSetter = std::make_shared<prepare::GenericDetectorParam>();
	    detectorSetter->setSkewAngle(view.first().skewAngle());
        /*
         * use a module location setter only if more than one module is present
         * (location would be zero anyways, and it's included in the detecor location already)
         */
        if(moduleLocations.size() > 1)
            detectorSetter->setModuleLocations(std::move(moduleLocations));

        // create AcquisitionSetup::View
        AcquisitionSetup::View viewSetting;
        viewSetting.setTimeStamp(ret.nbViews());
        viewSetting.addPrepareStep(gantrySetter);
        viewSetting.addPrepareStep(detectorSetter);

        ret.addView(viewSetting);
    }

    ret.prepareView(0);

    return ret;
}

AcquisitionSetup GeometryDecoder::decodeFullGeometry(const FullGeometry &geometry,
                                                     const QSize &pixelPerModule,
                                                     PhysicalDimension physicalDimension,
                                                     double mm,
                                                     int referenceModuleID)
{
    // construct a generic system
    CTSystem theSystem;

    uint nbModules = 0;
    if(geometry.size() == 0u)
        qWarning() << "empty geometry passed (no views available)";
    else
        nbModules = geometry.first().nbModules();

    auto detector = makeComponent<GenericDetector>(pixelPerModule, nbModules);
    auto source = makeComponent<GenericSource>(QSizeF(0.0, 0.0), Vector3x1(0.0));
    auto gantry = makeComponent<GenericGantry>();

    theSystem << std::move(source) << std::move(detector) << std::move(gantry);

    AcquisitionSetup ret(theSystem);

    // extract geometry information
    for(const SingleViewGeometry& view : geometry)
    {
        const auto referencePmat = view.first();
        auto srcPos = referencePmat.sourcePosition();
        auto pixelDimensions = computePixelSize(referencePmat, physicalDimension, mm);
        auto moduleLocations = computeModuleLocations(view, srcPos, pixelPerModule,
                                                      pixelDimensions.width());

        // prepare steps gantry
        auto gantrySetter = std::make_shared<prepare::GenericGantryParam>();
        auto srcRot = computeSourceRotation(moduleLocations, srcPos);
        gantrySetter->setSourceLocation(mat::Location(srcPos, srcRot));

        // note: refer position/rotation information of entire detector to reference module
        const auto refLoc = referenceModuleLocation(moduleLocations, referenceModuleID);
        gantrySetter->setDetectorLocation(refLoc);
        adaptModuleLocationsToReference(moduleLocations, refLoc);

        // detector prepare steps
        auto detectorSetter = std::make_shared<prepare::GenericDetectorParam>();
        detectorSetter->setPixelSize(pixelDimensions);
        detectorSetter->setSkewAngle(referencePmat.skewAngle());
        /*
         * use a module location setter only if more than one module is present
         * (location would be zero anyways, and it's included in the detecor location already)
         */
        if(moduleLocations.size() > 1)
            detectorSetter->setModuleLocations(std::move(moduleLocations));

        // create AcquisitionSetup::View
        AcquisitionSetup::View viewSetting;
        viewSetting.setTimeStamp(ret.nbViews());
        viewSetting.addPrepareStep(gantrySetter);
        viewSetting.addPrepareStep(detectorSetter);

        ret.addView(viewSetting);
    }

    ret.prepareView(0);

    return ret;
}

QSizeF GeometryDecoder::computePixelSize(const mat::ProjectionMatrix& pMat,
                                         PhysicalDimension physicalDimension,
                                         double mm)
{
    mat::Matrix<2, 1> focalLength = pMat.focalLength();

    QSizeF pixelDimensions;
    switch(physicalDimension)
    {
    case PhysicalDimension::PixelWidth:
        pixelDimensions.setWidth(mm);
        pixelDimensions.setHeight(mm * focalLength.get<0>() / focalLength.get<1>());
        break;
    case PhysicalDimension::PixelHeight:
        pixelDimensions.setWidth(mm * focalLength.get<1>() / focalLength.get<0>());
        pixelDimensions.setHeight(mm);
        break;
    case PhysicalDimension::SourceDetectorDistance:
        pixelDimensions.setWidth(mm / focalLength.get<0>());
        pixelDimensions.setHeight(mm / focalLength.get<1>());
        break;
    default:
        throw std::domain_error("invalid value for the physical dimension");
    }

    return pixelDimensions;
}

/*!
 * Computes the physical center of the modules in world coordinates.
 */
QVector<mat::Location> GeometryDecoder::computeModuleLocations(const SingleViewGeometry& singleViewGeometry,
                                                               const Vector3x1& sourcePosition,
                                                               const QSize& pixelPerModule,
                                                               double pixelWidth)
{
    QVector<mat::Location> ret;
    ret.reserve(singleViewGeometry.length());

    for(const ProjectionMatrix& modulePmat : singleViewGeometry)
    {
        mat::Location modLoc;
        modLoc.position = modulePmat.directionSourceToPixel(0.5 * (pixelPerModule.width() - 1),
                                                            0.5 * (pixelPerModule.height() - 1),
                                                            mat::ProjectionMatrix::NormalizeByX);

        modLoc.position *= pixelWidth;     // scale to physical dimensions
        modLoc.position += sourcePosition; // offset by position of the source

        modLoc.rotation = modulePmat.rotationMatR();

        ret.append(modLoc);
    }

    return ret;
}

/*!
 * Returns the number of pixels per module of the detector that the geometry decoder shall assume
 * for the system.
 */
const QSize& GeometryDecoder::pixelPerModule() const
{
    return _pixelPerModule;
}

/*!
 * Returns the pixel dimensions of the detector that the geometry decoder shall assume for the
 * system.
 */
const QSizeF& GeometryDecoder::pixelDimensions() const
{
    return _pixelDimensions;
}

/*!
 * Sets the number of pixels per module of the detector that the geometry decoder shall assume for
 * the system to \a value.
 */
void GeometryDecoder::setPixelPerModule(const QSize& value)
{
    _pixelPerModule = value;
}

/*!
 * Sets the pixel dimensions of the detector that the geometry decoder shall assume for the system
 * to \a value.
 */
void GeometryDecoder::setPixelDimensions(const QSizeF& value)
{
    _pixelDimensions = value;
}

/*!
 * Sets the reference module that the geometry decoder shall use to compute the detector location
 * to \a moduleID. If \a moduleID < 0, automatic selection will be performed, i.e. the central
 * module ID (\c nbModules / 2) will be used.
 *
 * Note that \a moduleID must be a valid id in the data to be decoded
 * (i.e. \a moduleID < \c nbModules).
 */
void GeometryDecoder::setReferenceModuleID(int moduleID)
{
    _referenceModule = moduleID;
}

} // namespace CTL
