#ifndef CTL_ABSTRACTPROJECTOR_H
#define CTL_ABSTRACTPROJECTOR_H

#include "img/projectiondata.h"
#include "img/spectralvolumedata.h"
#include "io/serializationinterface.h"

#include <memory>

namespace CTL {

// forward declarations
class AcquisitionSetup;
class CompositeVolume;
class SparseVoxelVolume;
typedef SpectralVolumeData VolumeData;

/*!
 * \class AbstractProjector
 *
 * \brief The AbstractProjector class is the abstract base class defining the interfaces for forward
 * projectors.
 *
 * This class defines the interface every forward projection implementation needs to satisfy. This
 * comes down to two methods that need to be provided:
 * - configure(): This method takes the AcquisitionSetup to be used for the simulation. All
 * necessary information to prepare the actual forward projection should be gathered here. This
 * usually contains all geometry and system information (which can be retrieved from the passed
 * AcquisitionSetup). Implementation specific parameters (e.g. accuracy settings), however, shall be
 * set using dedicated setter methods.
 * - project(): This method must provide the actual forward projection functionality. It takes the
 * voxelized volume that shall be projected and must return the full set of forward projections
 * that have been requested by the AcquisitionSetup set in the configure() step.
 *
 * Two structurally different ways of how such an implementation can be realized are given by
 * the examples external::RayCasterAdapter and RayCasterProjector. Both can be found in the OpenCL
 * module (ocl_routines.pri).
 */

/*!
 * \class ProjectorNotifier
 *
 * \brief Helper class that can emit signals during calculations of a certain projector.
 *
 * This class uses Qt's signal/slot principle to allow communication of a projector class with other
 * program parts. To do so, connect the signals of this object with the desired receiver objects.
 *
 * The ProjectorNotifier offers the following signals:
 * - projectionFinished(int): intended to be emitted when a projection is fully processed.
 * - information(QString): used to communicate status information.
 */

class ProjectorNotifier : public QObject
{
public:
    void connectToMessageHandler(bool includeProgress);

    Q_OBJECT
signals:
    void projectionFinished(int viewNb);
    void information(QString info);

private:
    QMetaObject::Connection _progressConnection;
};

class AbstractProjector : public SerializationInterface
{
    CTL_TYPE_ID(0)

    // abstract interface
    public:virtual void configure(const AcquisitionSetup& setup) = 0;
    public:virtual ProjectionData project(const VolumeData& volume) = 0;

public:
    AbstractProjector() = default;
    AbstractProjector(const AbstractProjector&) = delete;
    AbstractProjector(AbstractProjector&&) = default;
    AbstractProjector& operator=(const AbstractProjector&) = delete;
    AbstractProjector& operator=(AbstractProjector&&) = default;
    ~AbstractProjector() override = default;

    virtual bool isLinear() const;
    virtual ProjectionData projectComposite(const CompositeVolume& volume);
    virtual ProjectionData projectSparse(const SparseVoxelVolume& volume);

    ProjectionData configureAndProject(const AcquisitionSetup& setup, const VolumeData& volume);
    ProjectionData configureAndProject(const AcquisitionSetup& setup, const CompositeVolume& volume);
    ProjectionData configureAndProject(const AcquisitionSetup& setup, const SparseVoxelVolume& volume);

    void fromVariant(const QVariant& variant) override;
    QVariant toVariant() const override;
    virtual QVariant parameter() const;
    virtual void setParameter(const QVariant& parameter);

    virtual ProjectorNotifier* notifier();
    void connectNotifierToMessageHandler(bool includeProgress = false);

private:
    std::unique_ptr<ProjectorNotifier> _notifier{
        new ProjectorNotifier
    }; //!< The notifier object used for signal emission.
};

// factory function `makeProjector`
template <typename ProjectorType, typename... ConstructorArguments>
auto makeProjector(ConstructorArguments&&... arguments) ->
    typename std::enable_if<std::is_convertible<ProjectorType*, AbstractProjector*>::value,
                            std::unique_ptr<ProjectorType>>::type
{
    return std::unique_ptr<ProjectorType>(new ProjectorType(
                                              std::forward<ConstructorArguments>(arguments)...));
}

} // namespace CTL

/*! \file */
///@{
/*!
 * \typedef CTL::VolumeData
 *
 * \brief Alias name for CTL::SpectralVolumeData.
 *
 * Also serves as a placeholder for potential future changes to the concept of volume data.
 *
 * \relates CTL::AbstractProjector
 */

/*!
 * \fn std::unique_ptr<ProjectorType> CTL::makeProjector(ConstructorArguments&&... arguments)
 * \relates AbstractProjector
 *
 * Global (free) make function that creates a new Projector from possible constructor \a arguments.
 * The component is returned as a `std::unique_ptr<ProjectorType>`, whereas `ProjectorType` is the
 * template argument of this function that needs to be specified.
 *
 * Example:
 * \code
 * auto projector = makeProjector<RayCasterProjectorCPU>();
 * \endcode
 */
///@}

#endif // CTL_ABSTRACTPROJECTOR_H
