#include "ctl.h"
#include "ctl_ocl.h"
#include "ctl_nlopt.h"

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QElapsedTimer>

namespace {

struct Output
{
    bool R;
    bool H;
    bool P;
};

struct OptimizerSettings
{
    CTL::imgproc::GemanMcClure metric{ 50.0 };
    double lowerBounds = -30.0;
    double upperBounds = 30.0;
    float maxLineDistance = 1.0f;
    float subSampling = 0.1f;
    float truncationThreshold = 0.0f;
    bool disableSubsampling = false;
    bool drrBasedRegist = false;
    bool global = false;
    bool useTruncationSieve = false;
};

std::unique_ptr<CTL::io::AbstractProjectionMatrixIO> projMatIO(const QString& fn);
std::unique_ptr<CTL::io::AbstractProjectionDataIO> projDataIO(const QString& fn);
std::unique_ptr<CTL::io::AbstractVolumeIO<float>> volumeIO(const QString& fn);
OptimizerSettings parseOptimizerSettings(const QCommandLineParser& parser);
std::unique_ptr<CTL::AbstractNloptRegistration2D3D> getOptimizer(const OptimizerSettings& settings);
QString getFilenameSuffix(const OptimizerSettings& settings);
Output determineOutput(const QCommandLineParser& parser);

} // unnamed namespace

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    QCoreApplication::setApplicationVersion(QStringLiteral("1.8"));

    QCommandLineParser parser;
    parser.setApplicationDescription("Grangeat-based 2D/3D registration with pre-computed "
                                     "derivative of the 3D Radon space or conventional CT volume "
                                     "(DRR-based 2D/3D registration).");
    parser.addVersionOption();
    parser.addHelpOption();

    parser.addOptions({
        { { "p", "proj-mat" }, "File with projection matrices P0 [NRRD/DEN].", "path" },
        { { "i", "proj-imgs" }, "File with 2D projection images [NRRD/DEN].", "path" },
        { { "r", "radon3d" }, "File with pre-computed 3D Radon space [NRRD].", "path" },
        { { "V", "volume" }, "File with a conventional CT volume [NRRD].", "path" },

        { { "d", "diff" }, "Performes a central difference filter on the 3D Radon space w.r.t. the distance dimension." },

        { { "g", "global" }, "Enable global optimization." },
        { { "n", "no-subsampling" }, "Disable subsampling (same as '-s 1.0')." },
        { { "s", "sub-sampling" }, "Fraction of the sampled subset of available values. Default: 0.1", "fraction" },
        { { "l", "lower-bound" }, "Lower bound of opimization [mm/deg]. Default: -30.0", "value" },
        { { "u", "upper-bound" }, "Upper bound of opimization [mm/deg]. Default: 30.0", "value" },
        { { "m", "gmc-metric" }, "Parameter of the GMC metric. Default: 50.0", "value" },
        { { "t", "max-line-distance" }, "Crop maximum used line distance (0,1], e.g. in presence of truncation. Default: 1.0", "value" },
        { { "T", "truncation-threshold" }, "Threshold for boundary extinctions for ignoring truncated lines on the detector. "
                                           "Only active if set (e.g. '-T 4.0'). Default: disabled", "value" },

        { { "j", "device-number" }, "Use only a specific OpenCL device with index 'number'.", "number" },

        { { "R", "output-rigid" }, "Outputs estimated rigid parameters, ie. rotational axis r and translation vector t for each image. "
                                   "This is the default if no output flag is specified. [DEN]" },
        { { "H", "output-homo" }, "Outputs estimated homography H ~ [R|t], with rotation matrix R (from rotationals axis r) "
                                  "and translation vector t, for each image. [DEN]" },
        { { "P", "output-proj-mat" }, "Outputs estimated projection matrix P ~ P0 * H for each image. [DEN]" },
        { { "A", "output-all" }, "Outputs all three of the above results, ie. same as -RHP." },
        { { "o", "output" }, "Output directory.", "path" }
    });

    // Process the actual command line arguments given by the user
    parser.process(app);

    // Check consistency and integrity
    if(parser.isSet(QStringLiteral("r")) && parser.isSet(QStringLiteral("V")))
    {
        qCritical("radon3d (r) and volume (V) flag are mutually exclusive.");
        return -1;
    }

    if(!(parser.isSet(QStringLiteral("p")) && parser.isSet(QStringLiteral("i"))
         && (parser.isSet(QStringLiteral("r")) || parser.isSet(QStringLiteral("V")))))
    {
        qCritical("Projection matrices, projection images and 3D volume must be specified."
                  "See p, i and v options in help.");
        return -1;
    }

    if(parser.isSet(QStringLiteral("t")))
    {
        if(parser.isSet(QStringLiteral("V")))
        {
            qCritical("Option '-V' and '-t' are mutually exclusive. See '--help' option.");
            return -1;
        }
        if(parser.value(QStringLiteral("t")).toFloat() <= 0.0f)
        {
            qCritical("Value for '-t' must be a floating point number within the interval (0,1].");
            return -1;
        }
    }

    // Use only specific device
    if(parser.isSet(QStringLiteral("j")))
        CTL::OCL::OpenCLConfig::instance().setDevices( {
            CTL::OCL::OpenCLConfig::instance().devices().at(parser.value("j").toUInt()) } );

    // Input paths
    const auto fnProjMat{ parser.value(QStringLiteral("p")) };
    const auto fnProjImg{ parser.value(QStringLiteral("i")) };
    const auto fnRadon{ parser.isSet(QStringLiteral("r"))
                        ? parser.value(QStringLiteral("r"))
                        : parser.value(QStringLiteral("V")) };

    // Init IOs
    const auto ioProjMat = projMatIO(fnProjMat);
    const auto ioProjImg = projDataIO(fnProjImg);
    const auto ioVol = volumeIO(fnRadon); if(!ioVol) return -1;

    // Load all Pmats
    const auto Ps = ioProjMat->readFullGeometry(fnProjMat, 1);

    // Perform some checks
    if(Ps.nbViews() < 1)
    {
        qCritical("No projection matrices.");
        return -1;
    }
    if(Ps.at(0).nbModules() != 1)
    {
        qCritical("Only one detector module is supported.");
        return -1;
    }
    // check dimensions of projections
    const auto projImgDims = ioProjImg->metaInfo(fnProjImg).value(
                CTL::io::meta_info::dimensions).value<CTL::io::meta_info::Dimensions>();
    const auto nbViews = projImgDims.nbDim < 4 ? projImgDims.dim3 : projImgDims.dim4;
    if(Ps.nbViews() != nbViews)
    {
        qCritical("Number of projection matrices and number of projections do not match.");
        return -1;
    }

    // Read volume intermediate function (3D Radon space; Grangeat-based) or CT volume (DRR-based)
    if(parser.isSet(QStringLiteral("r")))
        qInfo("Load 3D Radon space...");
    else
        qInfo("Load 3D CT volume...");
    auto volumeData = ioVol->readVolume(fnRadon);

    // Derivative, if required
    if(parser.isSet(QStringLiteral("d")))
    {
        qInfo("Derivative of 3D Radon space...");
        CTL::imgproc::diff<2>(volumeData);
        volumeData /= volumeData.voxelSize().z; // divide by distance spacing
    }

    // Init resampler of volume data
    const CTL::OCL::VolumeResampler volumeResampler{ volumeData };

    // Init optimizer
    const auto settings = parseOptimizerSettings(parser);
    const auto reg = getOptimizer(settings);

    // Optimization
    qInfo("Start optimization...");
    QVector<double> rot, transl, homo;
    CTL::FullGeometry estPmats;
    QElapsedTimer time; time.start();

    for(auto v = 0u; v < nbViews; ++v)
    {
        const auto proj = ioProjImg->readSingleView(fnProjImg, v, 1).module(0);
        const auto& pMat = Ps.view(v).module(0);

        const auto optHomo = reg->optimize(proj, volumeResampler, pMat);
        const auto rotAxis = CTL::mat::rotationAxis(optHomo.subMat<0,2, 0,2>());
        const auto translVec = optHomo.subMat<0,2, 3,3>();

        qInfo().noquote() << "\nRegistration result of view" << v << ':';
        qInfo().noquote() << QString::fromStdString(rotAxis.info(  "rotational axis [rad] "));
        qInfo().noquote() << QString::fromStdString(translVec.info("translation vec. [mm] "));
        qInfo().noquote() << "----------------\n";

        rot.append(CTL::mat::toQVector(rotAxis));
        transl.append(CTL::mat::toQVector(translVec));
        homo.append(CTL::mat::toQVector(optHomo));
        estPmats.append(CTL::SingleViewGeometry{ { CTL::ProjectionMatrix(pMat * optHomo).normalized() } });
    }
    qInfo() << "Elapsed time [ms]:" << time.elapsed();

    // Save result                
    auto outPath = QStringLiteral(".");
    if(parser.isSet(QStringLiteral("o")))
    {
        outPath = parser.value(QStringLiteral("o"));
        if(outPath.right(1) == QLatin1String("/") || outPath.right(1) == QLatin1String("\\"))
            outPath.chop(1);
    }

    // determine output format
    const auto output = determineOutput(parser);
    const auto fnSuffix = getFilenameSuffix(settings);
    // save estimated rigid parameters (rotational axes and translation vectors)
    if(output.R)
    {
        CTL::io::den::save(rot, outPath + "/reg_rot" + fnSuffix, int(nbViews), 3);
        CTL::io::den::save(transl, outPath + "/reg_trans" + fnSuffix, int(nbViews), 3);
    }
    // save estimated homographies
    if(output.H)
    {
        CTL::io::den::save(homo, outPath + "/homo" + fnSuffix, 4, 4, int(nbViews));
    }
    // save estimated projection matrices
    if(output.P)
    {
        CTL::io::BaseTypeIO<CTL::io::DenFileIO>{}.write(estPmats, outPath + "/est_pmat" + fnSuffix);
    }

    return 0;
}

namespace {

using namespace CTL::io;
using NrrdIO = BaseTypeIO<NrrdFileIO>;
using DenIO = BaseTypeIO<DenFileIO>;

bool isNrrd(const QString& fn)
{
    return NrrdIO{}.metaInfo(fn).contains(QStringLiteral("nrrd version"));
}

std::unique_ptr<AbstractProjectionMatrixIO> projMatIO(const QString& fn)
{
    std::unique_ptr<AbstractProjectionMatrixIO> ret;

    if(isNrrd(fn))
        ret = NrrdIO::makeProjectionMatrixIO();
    else
        ret = DenIO::makeProjectionMatrixIO();

    return ret;
}

std::unique_ptr<AbstractProjectionDataIO> projDataIO(const QString& fn)
{
    std::unique_ptr<AbstractProjectionDataIO> ret;

    if(isNrrd(fn))
        ret = NrrdIO::makeProjectionDataIO();
    else
        ret = DenIO::makeProjectionDataIO();

    return ret;
}

std::unique_ptr<AbstractVolumeIO<float>> volumeIO(const QString& fn)
{
    if(!isNrrd(fn))
    {
        qCritical("Only NRRD format is supported for pre-computed 3D Radon space.");
        return nullptr;
    }

    return NrrdIO::makeVolumeIO<float>();
}

OptimizerSettings parseOptimizerSettings(const QCommandLineParser& parser)
{
    OptimizerSettings ret;

    if(parser.isSet(QStringLiteral("m")))
        ret.metric = CTL::imgproc::GemanMcClure{ parser.value(QStringLiteral("m")).toDouble() };

    if(parser.isSet(QStringLiteral("l")))
        ret.lowerBounds = parser.value(QStringLiteral("l")).toDouble();

    if(parser.isSet(QStringLiteral("u")))
        ret.upperBounds = parser.value(QStringLiteral("u")).toDouble();

    if(parser.isSet(QStringLiteral("t")))
        ret.maxLineDistance = parser.value(QStringLiteral("t")).toFloat();

    if(parser.isSet(QStringLiteral("T")))
    {
        ret.truncationThreshold = parser.value(QStringLiteral("T")).toFloat();
        ret.useTruncationSieve = true;
    }

    if(parser.isSet(QStringLiteral("s")))
        ret.subSampling = parser.value(QStringLiteral("s")).toFloat();

    ret.disableSubsampling = parser.isSet(QStringLiteral("n"));
    ret.drrBasedRegist = parser.isSet(QStringLiteral("V"));
    ret.global = parser.isSet(QStringLiteral("g"));

    return ret;
}

std::unique_ptr<CTL::AbstractNloptRegistration2D3D> getOptimizer(const OptimizerSettings& settings)
{
    std::unique_ptr<CTL::AbstractNloptRegistration2D3D> ret;

    if(settings.drrBasedRegist)
    {
        ret.reset(new CTL::NLOPT::ProjectionRegistration2D3D);
    }
    else
    {
        const auto grangeatRegist = new CTL::NLOPT::GrangeatRegistration2D3D;
        grangeatRegist->setMaxLineDistance(settings.maxLineDistance);
        ret.reset(grangeatRegist);
    }

    // metric
    ret->setMetric(&settings.metric);

    // sub-sampling
    if(!settings.disableSubsampling)
        ret->setSubSamplingLevel(settings.subSampling);

    // truncation
    ret->setTruncationSieve(settings.truncationThreshold, settings.useTruncationSieve);

    // init algorithm
    auto& opt = ret->optObject();

    // global or local
    if(settings.global)
    {
        opt = nlopt::opt{ nlopt::algorithm::GN_CRS2_LM, 6u };
        opt.set_maxtime(1000.0);
        opt.set_population(500u);
    }
    else
    {
        opt.set_xtol_rel(-1.0);
        opt.set_initial_step({ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 });
    }
    opt.set_xtol_abs(0.001);

    // set bounds
    opt.set_lower_bounds(settings.lowerBounds);
    opt.set_upper_bounds(settings.upperBounds);

    return ret;
}

QString getFilenameSuffix(const OptimizerSettings& settings)
{
    return (settings.global
                ? QStringLiteral("_global")
                : QStringLiteral("_local")) +
            "_sub" + (settings.disableSubsampling
                ? QStringLiteral("1.0")
                : QString::number(settings.subSampling)) +
            (settings.useTruncationSieve
                ? QStringLiteral("_trunc") + QString::number(settings.truncationThreshold)
                : QStringLiteral("")) +
            "_gmc" + QString::number(settings.metric.parameter()) +
            (!qFuzzyIsNull(settings.maxLineDistance - 1.0f)
                ? QStringLiteral("_maxDist") + QString::number(settings.maxLineDistance)
                : QStringLiteral("")) +
            (settings.drrBasedRegist
                ? QStringLiteral("_drr")
                : QStringLiteral("")) +
            ".den";
}

Output determineOutput(const QCommandLineParser& parser)
{
    // output
    bool R{ false }, H{ false }, P{ false };

    if(!parser.isSet(QStringLiteral("R")) && !parser.isSet(QStringLiteral("H"))
       && !parser.isSet(QStringLiteral("P")) && !parser.isSet(QStringLiteral("A")))
    {
        R = true;
    } else
    {
        if(parser.isSet(QStringLiteral("R")))
            R = true;
        if(parser.isSet(QStringLiteral("H")))
            H = true;
        if(parser.isSet(QStringLiteral("P")))
            P = true;
        if(parser.isSet(QStringLiteral("A")))
            R = H = P = true;
    }

    return { R, H, P };
}

} // unnamed namespace
