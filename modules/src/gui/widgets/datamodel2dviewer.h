#ifndef CTL_DATAMODEL2DVIEWER_H
#define CTL_DATAMODEL2DVIEWER_H

#include <QWidget>
#include "models/abstractdatamodel2d.h"

namespace Ui {
class DataModel2DViewer;
}

class QGridLayout;

namespace CTL {
namespace gui {

class Chunk2DView;

/*!
 * \class DataModel2DViewer
 *
 * \brief The DataModel2DViewer class provides a visualization tool for 2D data model types.
 *
 * This class can be used to visualize data models of subclasses of AbstractDataModel2D. For
 * convenience, the plot() method can be used to achieve a  one-line solution, creating a widget
 * that will be destroyed once it is closed by the user.
 *
 * Data is visualized as an image (see Chunk2DView for details).
 * The number of values that are sampled from the model (and visualized in the viewport) can be
 * adjusted via the GUI or using the corresponding slots setNumberOfSamplesX(),
 * increaseSamplingDensityX(), and reduceSamplingDensityX() (or corresponding ___Y() methods).
 *
 * In case the visualized data model has parameters, these can be adjusted directly within this
 * viewer's GUI. The parameter GUI can be hidden using hideParamterGUI(), if desired. It becomes
 * hidden automatically in case the model does not have any parameters.
 * Modifying model parameters from outside the viewer will not automatically update the plot. If
 * such a setting is required, use updatePlot() to enforce redrawing of the plot. (Note that this
 * will not update the entries in the parameter GUI.)
 * The GUI offers an option (checkbox, enabled by default) to automatically adjust data windowing
 * to a min/max window.
 *
 * The following IO operations are supported by this class:
 * Within the viewport of the current slice:
 * - Zooming:
 *    - Hold CTRL + scroll mouse wheel up/down to zoom in/out.
 * - Data windowing:
 *    - Hold left mouse button + move up/down to raise/lower the center (or level) of the window.
 *    - Hold left mouse button + move left/right to narrow/broaden the width of the window.
 *    - Double-click left to request automatic windowing (ie. min/max-window).
 * - Plotting a contrast line:
 *    - Hold right mouse button + drag mouse to draw a line.
 *    - Press 'K' button to create a contrast line plot of the current line (requires
 * ctl_gui_charts.pri submodule).
 *    - Press CTRL + C to copy the currently drawn contrast line coordinates to the clipboard
 *    - Press CTRL + V to set a contrast line based on previously copied coordinates from the
 * clipboard. The coordinates can also be copied from another window or widget.
 * - Read-out live pixel data under cursor:
 *    - Mouse movements: Live pixel data is shown under the bottom right corner of the image.
 *
 * Anywhere in the widget:
 * - Save to image:
 *    - Press CRTL + S to open a dialog for saving the current figure to a file.
 *    - Press 'K' button to create a contrast line plot of the current line (requires
 * ctl_gui_charts.pri submodule).
 *
 * The following example shows how to visualize a data model with the DataModel2DViewer class:
 * \code
 * // create a twodimension Gaussian model
 * auto gaussian = std::make_shared<GaussianModel2D>();
 *
 * // (static version) using the plot() command
 * gui::DataModel2DViewer::plot(gaussian); // or simply gui::plot(gaussian);
 *
 *
 * // (property-based version) alternatively
 * auto viewer = new gui::DataModel2DViewer; // needs to be deleted at an appropriate time
 * viewer->setData(gaussian);
 * // set a used-defined sensitivity for windowing with mouse gestures
 * viewer->chunkView()->setMouseWindowingScaling(0.01, 0.01);
 * viewer->resize(750,400);
 * viewer->show();
 * \endcode
 *
 * ![Resulting visualization from the (static version) example above.](gui/DataModel2DViewer.png)
 *
 * As can be seen from the example, the property-based version allows accessing the viewport, and
 * thereby, grants control over its specific settings.
 * The viewport is accessible via chartView().
 *
 * You can change the colormap used to visualize data values in the GUI, by means of the public slot
 * changeColormap(), or directly via the viewport (use the latter for custom colormaps).
 *
 * The DataModel2DViewer can also be used conveniently to inspect compositions of multiple models. The
 * following example illustrates that for a sum of a 2d rectangular model and a Gaussian blob:
 * \code
 * auto gaussian = std::make_shared<GaussianModel2D>(42.0f, 0.0f, 2.0f);
 * auto rect     = std::make_shared<RectModel2D>(0.5f, -5.0f, 5.0f, -2.0f, 2.0f);
 *
 * // visualize (with 300x300 samples)
 * gui::plot(rect, 300);
 * gui::plot(gaussian, 300);
 * gui::plot(rect + gaussian, 300);
 * \endcode
 *
 * ![Resulting visualization from the composition example. (a) only model 'rect', (b)  only model 'gaussian', (c) sum of 'rect' and 'gaussian'.](gui/DataModelViewer2D_composition.png)
 */

class DataModel2DViewer : public QWidget
{
    Q_OBJECT

public:
    explicit DataModel2DViewer(QWidget *parent = nullptr);
    ~DataModel2DViewer();

    Chunk2DView* chunkView() const;
    void setData(std::shared_ptr<AbstractDataModel2D> model);
    void setData(const AbstractDataModel2D& model);

    static void plot(std::shared_ptr<AbstractDataModel2D> model, uint nbSamples = 100);
    static void plot(const AbstractDataModel2D& model, uint nbSamples = 100);

public slots:
    void changeColormap(int mapIdx);
    void increaseSamplingDensityX();
    void increaseSamplingDensityY();
    void hideParameterGUI(bool hide = true);
    void reduceSamplingDensityX();
    void reduceSamplingDensityY();
    void setNumberOfSamplesX(int nbSamples);
    void setNumberOfSamplesY(int nbSamples);
    void setSamplingRangeX(float from, float to);
    void setSamplingRangeY(float from, float to);

protected:
    void keyPressEvent(QKeyEvent* event) override;

private:
    Ui::DataModel2DViewer* ui;

    std::unique_ptr<AbstractDataModel2D> _model;

    void setModelParameter(QVariant parameter);

private slots:
    void updatePixelInfo(int x, int y, float value);
    void windowingUpdate();
    void updatePlot();
};


// free function plot()
inline void plot(std::shared_ptr<AbstractDataModel2D> model, uint nbSamples = 100)
{ DataModel2DViewer::plot(std::move(model), nbSamples); }

inline void plot(const AbstractDataModel2D& model, uint nbSamples = 100)
{ DataModel2DViewer::plot(model, nbSamples); }

} // namespace gui
} // namespace CTL

#endif // CTL_DATAMODEL2DVIEWER_H
