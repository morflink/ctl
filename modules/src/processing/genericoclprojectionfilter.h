#ifndef CTL_GENERICOCLPROJECTIONFILTER_H
#define CTL_GENERICOCLPROJECTIONFILTER_H

#include "abstractprojectionfilter.h"
#include "ocl/openclconfig.h"

namespace CTL {
namespace OCL {

/*!
 * \class GenericOCLProjectionFilter
 *
 * \brief The GenericOCLProjectionFilter class is a facility class that allows easy construction of
 * a projection filter from an existing file containing the OpenCL kernel code for the filter
 * operation.
 *
 * This class provides a means of simple construction of a projection filter that executes the filter
 * method implemented in an OpenCL kernel, based on the file containing the corresponding kernel
 * code. To construct an instance, pass the file name of the .cl file containing the OpenCL kernel
 * code of the filter to this class' constructor. The \a clFileName (incl. path) is relative to the
 * runtime cl_src folder of the CTL (see ClFileLoader::openCLSourceDir())., i.e. files placed
 * directly in that folder can be addressed simply by their file name (see also 'Notes' below for
 * more details).
 * In addition, you might pass a vector containing an arbitrary number of \c float values. Each of
 * these values will be passed to the kernel as an additional argument.
 *
 * Notes:
 * - The OpenCL kernel must have the following signature:
 * `kernel void filter( read_only image3d_t, global float*, uint, ...)`
 *   - The first argument contains the input projection data (channels x rows x modules) of the
 *  currently processed view (see third argument).
 *   - The second argument is a pointer to the buffer into which the output result (filtered
 * projections) must be written. The buffer is a one-dimensional memory block representing the
 * projections in row-major order. You might consider using the facility method provided by
 * OpenCLFunctions::write_bufferf() for simple writes to the buffer; it will be added to the kernel
 * automatically.
 *   - The third argument holds the index of the view that is to be processed by the kernel instance.
 *   - The '...' stands for an arbitrary number of individual \c float parameters. Note that when
 * using additional parameters in the kernel, the same amount of parameters needs to be passed to
 * the ctor of this class (or set through setAdditionalKernelArgs()) for the filter to be
 * functional.
 * - The .cl file should be placed somewhere inside the cl_src folder of the CTL:
 * [...]/ctl/modules/src/ocl/cl_src. It will then be copied to the target directory automatically
 * when building the program and the GenericOCLProjectionFilter class is able to find it.
 * Alternatively, the .cl file can also be copied to the runtime OpenCL source code folder
 * (see ClFileLoader::openCLSourceDir()) manually.
 *
 * Each kernel call must process an entire view of the input data, i.e. all pixels of all modules
 * (should there be multiple). By default, projections are automatically combined, i.e. all modules
 * are concatenated horizontally (such that rows join together) before being processed by the
 * kernel. This behavior can be changed by creating a sub-class and re-implementing autoCombine() to
 * return \c false.
 * The degree of parallelization (i.e. global and local worksizes) is defined by globalWorksize()
 * and localWorksize(). The global worksize defaults to the total number of channels and rows
 * (2D worksize) in a single module of the projections. The local worksize is a NullRange by
 * default. Re-implement the corresponding methods in sub-classes to change this behavior.
 * Note that it might be particularly advisable to change the global worksize to a 3D range
 * (including the number of modules as a third dimension) if you implement a sub-class without
 * automatic combining---as long as autoCombine()==\c true (the default), there is only a single
 * module anyways.
 * The kernel will automatically be called for each view when filter() is executed.
 *
 * Note that this class always uses the first OpenCL device found in
 * OpenCLConfig::instance().devices(). Configure the device list accordingly before instantiating
 * this class if you want to utilize a particular device.
 *
 * Example:
 *
 * We want to demonstrate the use of this class by the example of a kernel that simply adds a
 * constant value (defined as one additional kernel parameter) to the pixels of the input data.
 *
 * Our kernel implementation looks like this:
 * \include examples/simple_projection_filter.cl
 *
 * We first get the global IDs to identify which voxel we are supposed to process. Note two things:
 * 1. The view index that is passed to the kernel as the third argument can be ignored here,
 * since our approach has no dependency on the view that is processed.
 * 2. When we specify the pixel index, we can simply use 0 as the module index (third index in
 * 'pixIdx', because we have only a single module (autoCombine() == \c true; and the data has only
 * a single module anyways).
 * We continue by reading out the value of the pixel in the input data. We add the value that we
 * specified as additional kernel argument (i.e. 'param') to the pixel and write the result to the
 * output buffer. For convenience, we utilize the `write_bufferf` method
 * (see OpenCLFunctions::write_bufferf()) to easily write the result value to the output buffer. In
 * doing so, we do not need to compute the correct 1D lookup for the pixel ourselves.
 *
 * In the .cpp code, using our kernel in a GenericOCLProjectionFilter looks something like this:
 * \code
 *  // create a projection data set with 100 x 100 pixels (single module)
 *  auto projections = ProjectionData(100, 100, 1);
 *  projections.allocateMemory(10); // gives 'projections' 10 views
 *
 *  // fill the pixels with random values from [0, 1.5]
 *  std::generate(projections.begin(), projections.end(),
 *                []() { return QRandomGenerator::global()->bounded(1.5f); });
 *
 *  qInfo() << projections.max();
 *  // output: 1.49994
 *
 *  // create the GenericOCLProjectionFilter with additional parameter 2.5
 *  OCL::GenericOCLProjectionFilter filt("simple_projection_filter.cl", { 2.5f });
 *
 *  // we now filter our volume (this should add 2.5 to the pixels)
 *  filt.filter(projections);
 *
 *  qInfo() << projections.max();
 *  // output: 3.99994
 * \endcode
 *
 * Note that for this to work, you need to copy the example kernel file (can be found in
 * ([...]/ctl/doc/examples/simple_projection_filter.cl) into the cl_src folder of the CTL:
 * [...]/ctl/modules/src/ocl/cl_src.
 * The .cl file will then be copied to the target directory automatically when building the
 * program. Alternatively, you can manually copy the .cl file directly to the runtime OpenCL
 * source code folder (which should be located in your build directory; see also
 * ClFileLoader::openCLSourceDir()).
 *
 * Tip: wrapping the construction and filtering in a try-catch block can help a lot in figuring
 * out issues
 * \code
 *  try {
 *      OCL::GenericOCLProjectionFilter filt("simple_projection_filter.cl", { 2.5f });
 *      filt.filter(projections);
 *  } catch (const std::exception& err) {
 *      qCritical() << err.what();
 *  }
 * \endcode
 */
class GenericOCLProjectionFilter : public AbstractProjectionFilter
{
    CTL_TYPE_ID(3100)

    public: void filter(ProjectionData& projections) override;

public:
    GenericOCLProjectionFilter(const std::string& clFileName,
                               const std::vector<float>& arguments = {});

    void setAdditionalKernelArg(float argument);
    void setAdditionalKernelArgs(const std::vector<float>& arguments);

    // AbstractProjectionFilter interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

    virtual ~GenericOCLProjectionFilter() = default;

protected:
    // only for serialization
    std::string _clFileName;
    std::vector<float> _additionalArgs;

    cl::Kernel* _kernel;
    cl::CommandQueue _queue;

    GenericOCLProjectionFilter();
    explicit GenericOCLProjectionFilter(cl::Kernel* kernel,
                                        const std::vector<float>& arguments = {});

    virtual cl::NDRange globalWorksize(const ProjectionData& projections) const;
    virtual cl::NDRange localWorksize(const ProjectionData& projections) const;
    virtual bool autoCombine() const;

private:
    static cl::Kernel* addKernelFromFile(const std::string& clFileName);
    static cl::CommandQueue getCommandQueue();
};

} // namespace OCL
} // namespace CTL

#endif // CTL_GENERICOCLPROJECTIONFILTER_H
