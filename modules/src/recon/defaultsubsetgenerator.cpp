#include "defaultsubsetgenerator.h"
#include "orthogonalsubsetgenerator.h"
#include "simplesubsetgenerator.h"

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(DefaultSubsetGenerator)

DefaultSubsetGenerator::DefaultSubsetGenerator()
    : TransitionSchemeExtension(new SimpleSubsetGenerator)
{
}

/*!
 * \brief Sets the number of subsets in each iteration to \a nbSubsets; disables size transition.
 *
 * If \a nbSubsets is larger than the number of projections in the data that has been set, the
 * number of subsets is reduced to the total count of available projections (i.e. subsets with a
 * single view each will be generated).
 *
 * Same as:
 * \code
 *  setTransitionPeriod(0);
 *  setMaximumNbSubsets(nbSubsets);
 * \endcode
 *
 * Example: generating a fixed number of 3 subsets
 * \code
 *  // create some dummy projections (the actual dimensions are irrelevant here)
 *  ProjectionData dummyProjections(1,1,1);
 *  dummyProjections.allocateMemory(20); // dummyProjections now contains 20 views
 *
 *  // create our subset generator
 *  DefaultSubsetGenerator subsetGen;
 *
 *  // we choose the 'Random' pattern ...
 *  subsetGen.setOrder(DefaultSubsetGenerator::Random);
 *  // ... and set our desired fixed number of 3 subsets
 *  subsetGen.setFixedNumberOfSubsets(3);
 *
 *  // pass projection data to the generator
 *  subsetGen.setProjections(dummyProjections);
 *
 *  // generate (and inspect) subsets for the first 4 iterations
 *  for(uint it = 0; it < 4; ++it)
 *  {
 *      const auto subsets = subsetGen.generateSubsets(it);
 *
 *      qInfo() << "Iteration" << it << "with" << subsets.size() << "subset(s)";
 *
 *      // print the ids of the views included in the two subsets
 *      for(const auto& subset : subsets)
 *          qInfo() << subset.viewIds();
 *  }
 *
 *  // output:
 *  // Iteration 0 with 3 subset(s)
 *  // std::vector(2, 17, 11, 3, 18, 12, 0)
 *  // std::vector(16, 7, 14, 4, 13, 10)
 *  // std::vector(15, 9, 5, 19, 1, 6, 8)
 *
 *  // Iteration 1 with 3 subset(s)
 *  // std::vector(9, 7, 17, 12, 16, 13)
 *  // std::vector(5, 11, 18, 3, 19, 8, 6)
 *  // std::vector(1, 4, 15, 0, 2, 10, 14)
 *
 *  // Iteration 2 with 3 subset(s)
 *  // std::vector(7, 3, 11, 15, 14, 12, 5)
 *  // std::vector(13, 16, 10, 4, 6, 17, 19)
 *  // std::vector(18, 8, 9, 2, 1, 0)
 *
 *  // Iteration 3 with 3 subset(s)
 *  // std::vector(9, 0, 17, 4, 5, 8)
 *  // std::vector(19, 13, 12, 16, 10, 2, 6)
 *  // std::vector(11, 15, 1, 14, 3, 18, 7)
 * \endcode
 */
void DefaultSubsetGenerator::setFixedNumberOfSubsets(uint nbSubsets)
{
    setTransitionPeriod(0);
    setMaximumNbSubsets(nbSubsets);
}

/*!
 * \brief Sets the selection order (creation pattern) for the creation of subsets to \a order.
 *
 * The following generation patterns are supported:
 * - Random         - randomly selects views for a subset
 * - Adjacent       - puts adjacent views into a subset
 * - Orthogonal180  - selects sets of most orthogonal views, assuming the total scan range was 180 degrees
 * - Orthogonal360  - selects sets of most orthogonal views, assuming the total scan range was 360 degrees
 * - RealOrthogonal - selection based on orthogonality between real view directions (i.e. the source-to-detector vector)
 *
 * All patterns except RealOrthogonal are provided by a SimpleSubsetGenerator. These cases do not
 * require an AcquisitionSetup being set for the generation of subsets. The case RealOrthogonal uses
 * OrthogonalSubsetGenerator and is based on the actual acquisition geometry; thus, requiring the
 * corresponding setup (see setSetup()).
 *
 * Calling this method always results in a reset of the nested subset generator. This might be
 * important in case a custom generator has been installed previously through
 * TransitionSchemeExtension::setSubsetGenerator().
 *
 * Example: see detailed class description
 */
void DefaultSubsetGenerator::setOrder(DefaultSubsetGenerator::Order order)
{
    std::unique_ptr<AbstractFixedSizeSubsetGenerator> nestedGen;

    if(order == RealOrthogonal)
        nestedGen.reset(new OrthogonalSubsetGenerator);
    else
        nestedGen.reset(new SimpleSubsetGenerator(1u, SimpleSubsetGenerator::Order(order)));

    setSubsetGenerator(std::move(nestedGen));
}

/*!
 * \enum DefaultSubsetGenerator::Order
 * Enumeration for subset selection orders.
 *
 * The selection order decides which views will end up in a particular subset.
 *
 * Implemented through SimpleSubsetGenerator.
 */

/*!
 * \var DefaultSubsetGenerator::Random
 *
 * Randomly selects views for a subset.
 *
 * Implemented through SimpleSubsetGenerator.
 */

/*!
 * \var DefaultSubsetGenerator::Adjacent
 *
 * Puts adjacent views into a subset.
 *
 * Implemented through SimpleSubsetGenerator.
 */

/*!
 * \var DefaultSubsetGenerator::Orthogonal180
 *
 * Selects sets of most orthogonal views, assuming a circular scan orbit with an agular range of
 * 180 degrees.
 *
 * Implemented through SimpleSubsetGenerator.
 */

/*!
 * \var DefaultSubsetGenerator::Orthogonal360
 *
 * Selects sets of most orthogonal views, assuming a circular scan orbit with an agular range of
 * 360 degrees.
 *
 * Implemented through SimpleSubsetGenerator.
 */

/*!
 * \var DefaultSubsetGenerator::RealOrthogonal
 *
 * Selects sets of most orthogonal views based on the actual acquisition geometry.
 *
 * Implemented through OrthogonalSubsetGenerator.
 */

} // namespace CTL
