#include "abstractregistration2d3d.h"
#include "mat/homography.h"

namespace CTL {

mat::Homography3D AbstractRegistration2D3D::optimize(
        const std::vector<Chunk2D<float>>&        /* projectionImages */,
        const OCL::VolumeResampler&               /* volume */,
        const std::vector<mat::ProjectionMatrix>& /* pMats */)
{
    throw std::runtime_error{ "Multiview registration not implemented!" };

    return {};
}

} // namespace CTL
