#include "ctl.h"
#include "ctl_ocl.h"
#include <QCoreApplication>
#include <QCommandLineParser>

using namespace CTL::io;

namespace {

QString getOutFile(const QStringList& args);
std::unique_ptr<AbstractVolumeIO<float>> getVolumeIO(const QString& fn);
CTL::VoxelVolume<float>::VoxelSize getVoxelSize(const QCommandLineParser& parser,
                                                const QMap<QString, QVariant> metaInfo);
CTL::VoxelVolume<float>::Offset getVolumeOffset(const QCommandLineParser& parser,
                                                const QMap<QString, QVariant>& metaInfo);
CTL::FullGeometry readFullGeometry(const QString& fn);
void setProjectorQuality(CTL::OCL::RayCasterProjector& projector, int q);

} // unnamed namespace

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("Forward Projector");
    //parser.addVersionOption();
    parser.addHelpOption();

    parser.addPositionalArgument("volume", "Input file path to volume data. [NRRD/DEN].");
    parser.addPositionalArgument("matrices", "Input file path to projection matrices [NRRD/DEN].");
    parser.addPositionalArgument("output path",
                                 "Folder, where 'forwardProjectorOutput.den' will be stored. (Default: '.')",
                                 "[output path]");

    parser.addOptions({
        { { "u", "detector-size-x" }, "Number of detector pixel in X/U. (Default: 1200)", "int" },
        { { "v", "detector-size-y" }, "Number of detector pixel in Y/V. (Default: 920)", "int" },
        { { "x", "voxel-size-x" }, "X size of voxels [mm].", "float" },
        { { "y", "voxel-size-y" }, "Y size of voxels [mm].", "float" },
        { { "z", "voxel-size-z" }, "Z size of voxels [mm].", "float" },
        { { "i", "isotropic-voxel-size" }, "Isotropic voxels size [mm].", "float" },
        { { "q", "quality-level" }, "Degree of accuracy from low (0) to high (3). (Default: 0)", "int" },
        { { "Q", "auto-quality" }, "Set auto-quality mode, ie. trade-off between accuracy and performance. (Not the default)" },
        { { "1", "offset-x" }, "Offsets the volume in X [mm].", "float" },
        { { "2", "offset-y" }, "Offsets the volume in Y [mm].", "float" },
        { { "3", "offset-z" }, "Offsets the volume in Z [mm].", "float" },
        { { "j", "device-number" }, "Use only one OpenCL device with specific index.", "int" } });

    parser.process(app);
    const QStringList args = parser.positionalArguments();

    // Use only specific device
    if(parser.isSet("j"))
        CTL::OCL::OpenCLConfig::instance().setDevices( {
            CTL::OCL::OpenCLConfig::instance().devices().at(parser.value("j").toUInt()) } );

    // File paths
    const auto volumePath = args.at(0);
    const auto matricesPath = args.at(1);
    const auto outFile = getOutFile(args);

    // Detectot dimensions
    int detSizeX = 1200;
    int detSizeY = 920;
    if(parser.isSet("u")) detSizeX = parser.value("u").toInt();
    if(parser.isSet("v")) detSizeY = parser.value("v").toInt();
    qInfo() << "detector size:" << detSizeX << " x " << detSizeY;

    // Voxel size
    const auto volumeIO = getVolumeIO(volumePath);
    const auto volumeMetaInfo = volumeIO->metaInfo(volumePath);
    const auto voxSize = getVoxelSize(parser, volumeMetaInfo);
    const auto volOffset = getVolumeOffset(parser, volumeMetaInfo);
    qInfo() << "voxel size:   " << voxSize.info().c_str();
    qInfo() << "volume offset:" << volOffset.info().c_str();

    try
    {
        // load projection matrices
        const auto pMats = readFullGeometry(matricesPath);
        qInfo() << "number of views:" << pMats.nbViews();

        // load volume
        auto volume = volumeIO->readVolume(volumePath);
        volume.setVoxelSize(voxSize);
        volume.setVolumeOffset(volOffset);

        // decode to acquisition setup
        const auto setup = CTL::GeometryDecoder::decodeFullGeometry(pMats, QSize(detSizeX, detSizeY));
        if(!setup.isValid())
        {
            qCritical() << "Error: Setup not valid!";
            return -1;
        }

        // configure a projector and project volume
        CTL::OCL::RayCasterProjector projector;
        if(parser.isSet("Q"))
            projector.settings() = CTL::OCL::RayCasterProjector::Settings::optimizedFor(
                        volume, *setup.system()->detector());
        else
            setProjectorQuality(projector, parser.value("q").toInt());
        const auto projections = projector.configureAndProject(setup, volume);

        // save projections
        CTL::io::BaseTypeIO<CTL::io::DenFileIO> outputIO;
        qInfo() << "write " + outFile << outputIO.write(projections, outFile);
    }
    catch(const std::exception& e)
    {
        qCritical() << "exception caught:\n" << e.what();
        return -1;
    }

    qInfo() << "end of program";
    return 0;
}

namespace {

using NrrdIO = BaseTypeIO<NrrdFileIO>;
using DenIO = BaseTypeIO<DenFileIO>;

QString getOutFile(const QStringList& args)
{
    auto outPath = QStringLiteral(".");
    if(args.size() >= 3)
    {
        outPath = args.at(2);
        if(outPath.right(1) == "/" || outPath.right(1) == "\\")
            outPath.chop(1);
    }
    return outPath + "/forwardProjectorOutput.den";
}

bool isNrrd(const QString& fn)
{
    return NrrdIO{}.metaInfo(fn).contains("nrrd version");
}

std::unique_ptr<AbstractVolumeIO<float>> getVolumeIO(const QString& fn)
{
    std::unique_ptr<AbstractVolumeIO<float>> ret;

    if(isNrrd(fn))
        ret = NrrdIO::makeVolumeIO<float>();
    else
        ret = DenIO::makeVolumeIO<float>();

    return ret;
}

CTL::VoxelVolume<float>::VoxelSize getVoxelSize(const QCommandLineParser& parser,
                                                const QMap<QString, QVariant> metaInfo)
{
    CTL::VoxelVolume<float>::VoxelSize ret{
            metaInfo.value(CTL::io::meta_info::voxSizeX).toFloat(),
            metaInfo.value(CTL::io::meta_info::voxSizeY).toFloat(),
            metaInfo.value(CTL::io::meta_info::voxSizeZ).toFloat() };

    if(parser.isSet("i"))
    {
        ret.x = parser.value("i").toFloat();
        ret.y = parser.value("i").toFloat();
        ret.z = parser.value("i").toFloat();
    }
    if(parser.isSet("x")) ret.x = parser.value("x").toFloat();
    if(parser.isSet("y")) ret.y = parser.value("y").toFloat();
    if(parser.isSet("z")) ret.z = parser.value("z").toFloat();

    return ret;
}

CTL::VoxelVolume<float>::Offset getVolumeOffset(const QCommandLineParser& parser,
                                                const QMap<QString, QVariant>& metaInfo)
{
    CTL::VoxelVolume<float>::Offset ret{
        metaInfo.value(CTL::io::meta_info::volOffX).toFloat(),
        metaInfo.value(CTL::io::meta_info::volOffY).toFloat(),
        metaInfo.value(CTL::io::meta_info::volOffZ).toFloat() };

    if(parser.isSet("1")) parser.value("1").toFloat();
    if(parser.isSet("2")) parser.value("2").toFloat();
    if(parser.isSet("3")) parser.value("3").toFloat();

    return ret;
}

std::unique_ptr<AbstractProjectionMatrixIO> getProjMatIO(const QString& fn)
{
    std::unique_ptr<AbstractProjectionMatrixIO> ret;

    if(isNrrd(fn))
        ret = NrrdIO::makeProjectionMatrixIO();
    else
        ret = DenIO::makeProjectionMatrixIO();

    return ret;
}

CTL::FullGeometry readFullGeometry(const QString& fn)
{
    return getProjMatIO(fn)->readFullGeometry(fn);
}

void setProjectorQuality(CTL::OCL::RayCasterProjector& projector, int q)
{
    const auto nbRays = q + 1;
    const auto stepLength = 0.3f * std::pow(10.f, float(-q));
    projector.settings().interpolate = (q == 0);
    projector.settings().raysPerPixel[0] = nbRays;
    projector.settings().raysPerPixel[1] = nbRays;
    projector.settings().raySampling = stepLength;
}

} // unnamed namespace
