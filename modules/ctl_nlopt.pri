# CTL NLopt module
# ================
#
# Includes CTL submodules with NLopt dependency.
#
# Install NLopt library:
#   git clone git://github.com/stevengj/nlopt
#   cd nlopt
#   mkdir build
#   cd build
#   cmake ..
#   cmake --build . --target install
#
# Note that this module depends on the CTL OpenCL module.

!REGIST_2D3D_MODULE: include(submodules/regist_2d3d.pri)

HEADERS += \
    $$PWD/src/ctl_nlopt.h \
    $$PWD/src/io/printnloptmsg.h
