#include "abstractxrayspectrummodel.h"

namespace CTL {

// ____________________________
// # AbstractXraySpectrumModel
// ----------------------------
/*!
 * \brief Sets the energy parameter to \a parameter.
 *
 * \a parameter can be either a single \c float (i.e. the energy in keV) or a QVariantMap that
 * contains the key-value-pair ("energy", [\c float] value_to_set).
 *
 * Note that, when used within an actual source component (AbstractSource::setSpectrumModel()), you
 * do not need to deal with setting this parameter. This should be done automatically by the
 * specific source component class.
 */
void AbstractXraySpectrumModel::setParameter(const QVariant& parameter)
{
    if(parameter.canConvert(QMetaType::Float))
        _energy = parameter.toFloat();
    else
        _energy = parameter.toMap().value(QStringLiteral("energy")).toFloat();
}

// use base class documentation
QVariant AbstractXraySpectrumModel::parameter() const
{
    return QVariantMap{ { "energy", _energy } };
}

} // namespace CTL
