#ifndef CTL_GENERICOCLVOLUMEFILTER_H
#define CTL_GENERICOCLVOLUMEFILTER_H

#include "ocl/openclconfig.h"
#include "processing/abstractvolumefilter.h"

namespace CTL {

template<typename>
class VoxelVolume;

namespace OCL {

/*!
 * \class GenericOCLVolumeFilter
 *
 * \brief The GenericOCLVolumeFilter class is a facility class that allows easy construction of a
 * volume filter from an existing file containing the OpenCL kernel code for the filter operation.
 *
 * This class provides a means of simple construction of a volume filter that executes the filter
 * method implemented in an OpenCL kernel, based on the file containing the corresponding kernel
 * code. To construct an instance, pass the file name of the .cl file containing the OpenCL kernel
 * code of the filter to this class' constructor. The \a clFileName (incl. path) is relative to the
 * runtime cl_src folder of the CTL (see ClFileLoader::openCLSourceDir())., i.e. files placed
 * directly in that folder can be addressed simply by their file name (see also 'Notes' below for
 * more details).
 * In addition, you might pass a vector containing an arbitrary number of \c float values. Each of
 * these values will be passed to the kernel as an additional argument.
 *
 * Notes:
 * - The OpenCL kernel must have the following signature:
 * `kernel void filter( read_only image3d_t, global float*, uint, ...)`
 *   - The first argument contains the input volume.
 *   - The second argument is a pointer to the buffer into which the output result (filtered volume)
 * must be written. The buffer is a one-dimensional memory block representing the volume in
 * row-major order. You might consider using the facility method provided by
 * OpenCLFunctions::write_bufferf() for simple writes to the buffer; it will be added to the kernel
 * automatically.
 *   - The third argument holds the index of the slice that is to be processed by the kernel instance.
 *   - The '...' stands for an arbitrary number of individual \c float parameters. Note that when
 * using additional parameters in the kernel, the same amount of parameters needs to be passed to
 * the ctor of this class (or set through setAdditionalKernelArgs()) for the filter to be
 * functional.
 * - The .cl file should be placed somewhere inside the cl_src folder of the CTL:
 * [...]/ctl/modules/src/ocl/cl_src. It will then be copied to the target directory automatically
 * when building the program and the GenericOCLProjectionFilter class is able to find it.
 * Alternatively, the .cl file can also be copied to the runtime OpenCL source code folder
 * (see ClFileLoader::openCLSourceDir()) manually.
 *
 * Each kernel call must process an entire z-slice of the input volume. The degree of
 * parallelization (i.e. global and local worksizes) are defined by globalWorksize() and
 * localWorksize(). The global worksize defaults to the total number of voxels in x and y direction
 * in the input volume (2D worksize), whereas the local worksize is a NullRange by default.
 * Re-implement the corresponding methods in sub-classes to change this behavior. The kernel will
 * automatically be called for each z-slice when filter() is executed.
 *
 * Note that this class always uses the first OpenCL device found in
 * OpenCLConfig::instance().devices(). Configure the device list accordingly before instantiating
 * this class if you want to utilize a particular device.
 *
 * Example:
 *
 * We want to demonstrate the use of this class by the example of a kernel that simply multiplies
 * the input volume by a constant factor that we define as one additional kernel parameter.
 *
 * Our kernel implementation looks like this:
 * \include examples/simple_volume_filter.cl
 *
 * We first get the global IDs to identify which voxel we are supposed to process. Note that the
 * z-coordinate is fixed within a kernel call and is passed to it as the third argument.
 * We continue by reading out the value of the voxel in the input volume. This value will be
 * multiplied by our scaling factor that gets passed to the kernel as its fourth argument.
 * Finally, we utilize the `write_bufferf` method (see OpenCLFunctions::write_bufferf()) to easily
 * write the result value to the output buffer.
 *
 * In the .cpp code, using our kernel in a GenericOCLVolumeFilter looks something like this:
 * \code
 *  // create a simple cylinder volume filled with value 1.0
 *  auto volume = VoxelVolume<float>::cylinderZ(10.0f, 10.0f, 1.0f, 1.0f);
 *
 *  qInfo() << volume.min() << volume.max();
 *  // output: 0 1
 *
 *  // optionally in a try-catch block (see below)
 *
 *  // create the GenericOCLVolumeFilter with additional parameter 2.0
 *  OCL::GenericOCLVolumeFilter filt("simple_volume_filter.cl", { 2.0f });
 *      // alternative(s):
 *      // filt.setAdditionalKernelArg(2.0f);
 *      // filt.setAdditionalKernelArgs({ 2.0f });
 *
 *  // we now filter our volume
 *  filt.filter(volume);
 *
 *  qInfo() << volume.min() << volume.max();
 *  // output: 0 2
 * \endcode
 *
 * Note that for this to work, you need to copy the example kernel file (can be found in
 * ([...]/ctl/doc/examples/simple_volume_filter.cl) into the cl_src folder of the CTL:
 * [...]/ctl/modules/src/ocl/cl_src.
 * The .cl file will then be copied to the target directory automatically when building the
 * program. Alternatively, you can manually copy the .cl file directly to the runtime OpenCL
 * source code folder (which should be located in your build directory; see also
 * ClFileLoader::openCLSourceDir()).
 *
 * Tip: wrapping the construction and filtering in a try-catch block can help a lot in figuring
 * out issues
 * \code
 *  try {
 *      OCL::GenericOCLVolumeFilter filt("simple_volume_filter.cl", { 2.0f });
 *      filt.filter(volume);
 *  } catch (const std::exception& err) {
 *      qCritical() << err.what();
 *  }
 * \endcode
 */
class GenericOCLVolumeFilter : public AbstractVolumeFilter
{
    CTL_TYPE_ID(2100)

public: void filter(VoxelVolume<float>& volume) override;

public:
    explicit GenericOCLVolumeFilter(const std::string& clFileName,
                                    const std::vector<float>& arguments = {});

    void setAdditionalKernelArg(float argument);
    void setAdditionalKernelArgs(const std::vector<float>& arguments);

    // AbstractVolumeFilter interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

protected:
    GenericOCLVolumeFilter();
    explicit GenericOCLVolumeFilter(cl::Kernel* kernel, const std::vector<float>& arguments = {});

    // only for serialization
    std::string _clFileName;
    std::vector<float> _additionalArgs;

    cl::Kernel* _kernel;
    cl::CommandQueue _queue;

    virtual cl::NDRange globalWorksize(const VoxelVolume<float>& volume) const;
    virtual cl::NDRange localWorksize(const VoxelVolume<float>& volume) const;

private:
    static cl::Kernel* addKernelFromFile(const std::string& clFileName);
    static cl::CommandQueue getCommandQueue();
};

} // namespace OCL
} // namespace CTL

#endif // CTL_GENERICOCLVOLUMEFILTER_H
