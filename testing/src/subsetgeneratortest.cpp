#include "subsetgeneratortest.h"
#include "acquisition/systemblueprints.h"
#include "acquisition/trajectories.h"

// subset generators
#include "recon/defaultsubsetgenerator.h"
#include "recon/orthogonalsubsetgenerator.h"
#include "recon/simplesubsetgenerator.h"
#include "recon/transitionschemeextension.h"

using namespace CTL;



SubsetGeneratorTest::SubsetGeneratorTest()
    : _dummyData(ProjectionData(1,1,1))
{
}

void SubsetGeneratorTest::initTestCase()
{
    _dummyData.allocateMemory(10);

    _setup = AcquisitionSetup(makeCTSystem<blueprints::GenericCarmCT>(),10);
    _setup.applyPreparationProtocol(protocols::ShortScanTrajectory(750));
}

void SubsetGeneratorTest::testSerialization()
{
    qInfo() << "Test DefaultSubsetGenerator";
    DefaultSubsetGenerator defGen;
    QVERIFY(runSerializationCheck(defGen));

    qInfo() << "Test OrthogonalSubsetGenerator";
    OrthogonalSubsetGenerator orthoGen;
    orthoGen.setNbSubsets(2);
    QVERIFY(runSerializationCheck(orthoGen));

    qInfo() << "Test SimpleSubsetGenerator";
    SimpleSubsetGenerator simpleGen;
    simpleGen.setNbSubsets(2);
    QVERIFY(runSerializationCheck(simpleGen));

    qInfo() << "Test TransitionSchemeExtension";
    auto simpleGenCopy = new SimpleSubsetGenerator(simpleGen);
    TransitionSchemeExtension transGen(simpleGenCopy);
    QVERIFY(runSerializationCheck(transGen));
}
