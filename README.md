CTL - Computed Tomography Library
=================================

**Current version: 0.5.3**

A C++11 toolkit for CT imaging with minimal dependencies.

This early release enables to simluate freely customizable cone-beam X-ray
imaging setups.
Typical settings like helical CT or C-arm CT with curved or flat panel
detector are included as well. Conversion of the geometric information to/from
projection matrices allowes to easily collaborate with other tools supporting 
them.

Publications
------------

Reference publication:  
[1] Tim Pfeiffer, Robert Frysch, Richard N. K. Bismark, and Georg Rose,
"CTL: modular open-source C++-library for CT-simulations,"
Proc. SPIE 11072,
15th International Meeting on Fully Three-Dimensional Image Reconstruction in Radiology and Nuclear Medicine, 110721L (28 May 2019);
[doi:10.1117/12.2534517](https://doi.org/10.1117/12.2534517)

Radon transform:  
[2] Robert Frysch, Tim Pfeiffer, Georg Rose, 
"A generalized method for computation of n-dimensional Radon transforms,"
Proc. SPIE, Medical Imaging 2020: Image Processing, Volume 11313, 2020, S. 610-616;
[doi:10.1117/12.2549586](https://doi.org/10.1117/12.2549586) 

Grangeat-based 2D/3D registration:  
[3] Robert Frysch, Tim Pfeiffer, Georg Rose,
"A novel approach to 2D/3D registration of X-ray images using Grangeat’s relation,"
Medical Image Analysis, Volume 67, 2021, 101815, ISSN 1361-8415;
[doi:10.1016/j.media.2020.101815](https://doi.org/10.1016/j.media.2020.101815)  
-> See example programs [radon3d](examples/radon3d) to pre-compute the *volume intermediate function* and [regist2d3d](examples/regist2d3d) to perform the actual 2D/3D registration.

**Presentations on the recent Fully3D Meeting (19 - 23 July 2021, [see the abstract book](https://kuleuvencongres.be/fully3d-2021/fully3d2021-online-abstract-book)) showing CTL features:**  
[4] Fatima Saad et al., "Spherical Ellipse Scan Trajectory for Tomosynthesis-Assisted Interventional Bronchoscopy", p. 38, *oral talk*  
[5] Tim Pfeiffer et al., "Two extensions of the separable footprint forward projector", pp. 61-62, *poster presentation*

--------------

Teaser: Simulating projections
-----------------------------

The following example code uses a predefined C-arm system and a predefined
trajectory (a trajectory is a specific acquisition protocol) in order to
project a volume, which is read from a file. This serves to show how the CTL
may work out of the box. However, CT systems or acquisition protocols (or even
preparations of single views) can be freely configured. Moreover, certain
projector extensions can "decorate" the used forward projector in order to
include further geometric/physical/measuring effects.

```cpp
#include "ctl.h"
#include "ctl_ocl.h"
#include "ctl_qtgui.h"
#include <QApplication>

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);

    // create a cylinder as a volume
    const float radius = 60.0, height = 100.0, voxelSize = 0.5, fillValue = 0.03;
    const CTL::VoxelVolume<float> volume = CTL::VoxelVolume<float>::cylinderX(radius, height,
                                                                              voxelSize, fillValue);

    // create an acquisition setup using a predefined system
    const uint nbViews = 100;
    CTL::AcquisitionSetup myCarmSetup{ CTL::makeCTSystem<CTL::blueprints::GenericCarmCT>(),
                                       nbViews };
    // add a predefined trajectory to the setup
    const double sourceToIsocenter = 750.0; // mm is the standard unit for length dimensions
    const double startAngle = 42.0_deg; // floating-point literal _deg converts to rad
    myCarmSetup.applyPreparationProtocol(CTL::protocols::WobbleTrajectory{ sourceToIsocenter,
                                                                           startAngle });
    if(!myCarmSetup.isValid())
        return -1;

    // plot setup
    CTL::gui::plot(myCarmSetup);

    // configure a projector and project volume
    CTL::OCL::RayCasterProjector myProjector; // an ideal projector with default settings
    const auto projections = myProjector.configureAndProject(myCarmSetup, volume);

    // plot projections
    CTL::gui::plot(projections);

    return a.exec();
}

/*
 * NOTE: the project file (.pro) needs to include the following modules to be
 * able to compile this example program:
 *
 *  include(path/to/ctl.pri)
 *  include(path/to/ctl_ocl.pri)
 *  include(path/to/ctl_qtgui.pri)
 */
 ```
The graphical output should look like this:

<img src="doc/img/readme-wobble-setup.png" height="332">
<img src="doc/img/cylinder-projection.png" height="332">

 A corresponding qmake project for this example can be found in
 [examples/readme-example](https://gitlab.com/tpfeiffe/ctl/tree/master/examples/readme-example).

--------------

[**Click here for the full documentation of the source code.**](https://www.stimulate.de/ctl/)  
If you use the [Qt Creator](https://www.qt.io/product/development-tools) as your IDE, you can directly integrate the documentation by adding [this help file](https://www.stimulate.de/ctl/ctl_doc.qch) via
`Tools`->`Options...`->`Help`->`Documentation`->`Add...`.
In the editor, you can then just press `F1` on a symbol (e.g. a class name) to get the corresponding help.

To get you started, you can also have a look at our [series of video tutorials](https://ctl-tutorials.github.io/).

Moreover, you may find useful information in the
[Wiki](https://gitlab.com/tpfeiffe/ctl/wikis/home),
especially if you want to contribute to the project as a developer.

In case you are looking for some Python bindings check out [PyCTL](https://github.com/phernst/pyctl) by Philipp Ernst.

Setup of the development environment
-----------------------------------

The following installation guide has been tested with Kubuntu 18.04 LTS.  
You may also try a preconfigured Docker image. In this case, you can skip the 
following three steps and go directly to [Use a Docker image](#use-a-docker-image).

### Install compiler and build tools (GCC, make, ...)

```console
sudo apt install build-essential
```

### Install Qt

The [Qt Core module](https://doc.qt.io/qt-5/qtcore-index.html) is the only
library required for the main module
"[CTL core](https://gitlab.com/tpfeiffe/ctl/blob/master/modules/ctl.pri)" of this
project.

1. Qt libraries

    ```console
    sudo apt install qt5-default
    ```
    
2. Qt3D (optional, for
[3D GUI widgets](https://gitlab.com/tpfeiffe/ctl/blob/master/modules/submodules/gui_widgets_3d.pri))
    
    ```console
    sudo apt install qt3d5-dev
    ```
    If you are on another platform, note that a Qt3D version is needed that 
    requires a Qt version >= 5.9.
3. QtCharts (optional, for [chart-based widgets](https://gitlab.com/tpfeiffe/ctl/blob/dev_v0.3.2/modules/submodules/gui_widgets_charts.pri))
    ```console
    sudo apt install libqt5charts5-dev
    ```

### Install OpenCL

OpenCL and its C++-API is required for compiling the
[CTL OCL](https://gitlab.com/tpfeiffe/ctl/blob/master/modules/ctl_ocl.pri)
module. To run code depending on this module, a device (CPU/GPU) that supports
an OpenCL version > 1.1 is required.  
This is an example how to set up
[OpenCL](https://github.khronos.org/OpenCL-CLHPP/) for a NVIDIA GPU.

1. install official NVIDIA driver using Driver Manager
(tested with driver version 390.48 + GTX1080 Ti) --> reboot

2. install Nvidia OpenCL development package

    ```console
    sudo apt install nvidia-opencl-dev
    ```
    For other vendors
    ([AMD](https://linuxconfig.org/install-opencl-for-the-amdgpu-open-source-drivers-on-debian-and-ubuntu),
    [Intel](https://software.intel.com/en-us/intel-opencl/download),
    ...),  you need to search for an appropriate
    package that provides you an OpenCL driver.
    
3. install clinfo (optional, for checking available OpenCL devices)

    ```console
    sudo apt install clinfo
    ```
    Then, you can just type
    ```console
    clinfo
    ```
    in order to get a summary of your OpenCL devices.
    You can make sure that the device you intend to use appears in the list.

4. install OpenCL headers (might be already there)

    ```console
    sudo apt install opencl-headers
    ```

No matter what kind of OpenCL device you want to use, you should end up with a 
*libOpenCL.so* (usually symbolic link) in the library folder
(/usr/lib/) that points to a valid ICD loader. The ICD loader should have been
installed with your vendor driver. If this has not happend you will not be able
to link against it (linker error when using `LIBS += -lOpenCL` in your qmake
project file). Then, you can install an ICD loader by your own, e.g. the package
*mesa-opencl-icd*.

It might happen on some Debian platforms, that the `CL/cl.hpp` header is missing
even when you have installed the 'opencl-headers' package. In this case, you can
manually download the header file from the
[khronos group](https://www.khronos.org/registry/OpenCL/api/2.1/cl.hpp).
Also newer versions of this header such as `CL/cl2.hpp` or `CL/opencl.hpp` are
supported.

Use a Docker image
------------------

Instead of installing the above libraries, you may also start directly by using
a Docker image.
There are two prepared Docker images that you can use as a development/testing
environment:
 * `frysch/ubuntu:ocl-nvidia` for Nvidia GPUs
 * `frysch/ubuntu:ocl-intel` for Intel CPUs

### Run a GUI example inside Docker
Give Docker the rights to access the X-Server:
```console
xhost +local:docker
```
Run Docker in interactive mode (`-it`) and shared X11 socket (
`-e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix` - required only for 
showing GUIs):
```console
docker run -it -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix frysch/ubuntu:ocl-intel bash
```
Inside the Docker container run
```console
git clone https://gitlab.com/tpfeiffe/ctl.git
cd ctl/examples/simTool/
qmake && make -j 4
./bin/SimulationTool
```
in order to compile and run the GUI example "SimulationTool".


For further details about the Docker images, see
[this Wiki page](https://gitlab.com/tpfeiffe/ctl/wikis/Docker-Images).


Develop your own C++ apps or modules
====================================

Use modules that you need
-------------------------

The CTL provides several modules. According to your needs, you can select only a
subset of modules. Each module has a corresponding .pri file that you can
include into your qmake project (.pro file) using the syntax
`include(example_module.pri)` (see also the *examples* or *testing* folder).

The following modules are available:
 * ctl.pri: the core library and file IO
 * ctl_ocl.pri: OpenCL routines and OpenCL environment
 * ctl_qtgui.pri: widgets for visualization purposes
 * ctl_nlopt.pri: NLopt-dependent submodules; currently, Grangeat-based 2D/3D registration


Compile a project
-----------------

```console
cd /path/to/source/where/the/.pro/file/lives
mkdir build
cd build
qmake ..
make
```


Linking in a CMake project
--------------------------
Here, we assume Qt6 has been installed. If you want to compile the CTL with Qt5 you need to run `qmake` instead of `qmake6` in the following guide. Furthermore, occurrences of 'Qt6' must be replaced by 'Qt5' in the "lib/ctl.cmake" file.

- First, compile the CTL into a static library:
```console
cd lib
qmake6 && make
```
- Add the following to your "CMakeLists.txt" file:
```console
include("path/to/lib/ctl.cmake")
target_link_libraries(myTarget CTL)
```
- Now you can use all the CTL header files in your target's source files like the meta headers
```cpp
#include <ctl.h>
#include <ctl_ocl.h>
#include <ctl_qtgui.h>
```


Common pitfalls
--------------

**The integrated GPU problem**  
If you have an integrated graphics unit in your CPU, e.g. Intel HD graphics, it might happen that this OpenCL device is preferred over a dedicated graphics card by default, which results in long computing times. This behavior can be easily fixed by blacklisting (parts of) the name of this device at the beginning of your program, e.g.  
`CTL::OCL::OpenCLConfig::instance(false).setDevices({""}, {"Intel"}, CL_DEVICE_TYPE_GPU);`  
The first argument specifies a possible whitelist (`""` matches everything), the second is the blacklist and the third restricts the device search to a specific type (GPUs in this case).

**The missing voxel size problem**  
If you use the DEN file format to read in volume data, e.g.  
`auto volume = CTL::io::BaseTypeIO<CTL::io::DenFileIO>{}.readVolume<float>("path/to/volume.den");`  
be aware that the DEN format cannot store meta information about the voxel size. Therefore, it defaults to zero:
`CTL::VoxelVolume<float>::VoxelSize{0.0f, 0.0f, 0.0f}`.
Consequently, the result of executing `myProjector.configureAndProject(mySetup, volume)` does not show any projection of the volume as the volume has no extent. The voxel size needs to be manually set by `volume.setVoxelSize(...)`.

--------------

Statistics on the master branch:

![](https://tokei.rs/b1/GitLab/tpfeiffe/ctl?category=code)
![](https://tokei.rs/b1/GitLab/tpfeiffe/ctl?category=comments)
![](https://tokei.rs/b1/GitLab/tpfeiffe/ctl?category=files)

For more details click [this link](https://rf.p7.de/apps/cms_pico/pico/ctlstat/).

--------------

If you have any problems or questions regarding the CTL, please contact us via mail
<sw4g.production@gmail.com>
or join the [element room](https://element.io)
`#ctl-dev:matrix.org`.
