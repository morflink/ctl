#ifndef CTL_TRAJECTORIES_H
#define CTL_TRAJECTORIES_H

#include "abstractpreparestep.h"
#include "acquisitionsetup.h"
#include "preparesteps.h"
#include "mat/mat.h"

namespace CTL {
namespace protocols {

class HelicalTrajectory : public AbstractPreparationProtocol
{
    public: std::vector<std::shared_ptr<AbstractPrepareStep>>
            prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    HelicalTrajectory(double angleIncrement,
                      double pitchIncrement = 0.0,
                      double startPitch = 0.0,
                      double startAngle = 0.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    double angleIncrement = 0.0_deg;
    double pitchIncrement = 0.0; //!< [mm]
    double startPitch     = 0.0; //!< [mm]
    double startAngle     = 0.0_deg;
};

class WobbleTrajectory : public AbstractPreparationProtocol
{
    public: std::vector<std::shared_ptr<AbstractPrepareStep>>
            prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    WobbleTrajectory(double sourceToIsocenter,
                     double startAngle  = 0.0_deg,
                     double angleSpan   = -1.0_deg,
                     double wobbleAngle = 15.0_deg,
                     double wobbleFreq  = 1.0);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    double sourceToIsocenter = 0.0; //!< [mm]
    double startAngle        = 0.0_deg;
    double angleSpan         = -1.0_deg;
    double wobbleAngle       = 15.0_deg;
    double wobbleFreq        = 1.0; //!< number of wobbles (2 arcs) during the acquisition
};

class CirclePlusLineTrajectory : public AbstractPreparationProtocol
{
    public: std::vector<std::shared_ptr<AbstractPrepareStep>>
            prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    CirclePlusLineTrajectory(double sourceToIsocenter,
                             double lineLength,
                             double startAngle = 0.0_deg,
                             double angleSpan  = 360.0_deg,
                             double fractionOfViewsForLine = 0.5);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    double sourceToIsocenter      = 0.0; //!< [mm]
    double lineLength             = 0.0; //!< [mm]
    double startAngle             = 0.0_deg;
    double angleSpan              = 360.0_deg;
    double fractionOfViewsForLine = 0.5;
};

class ShortScanTrajectory : public AbstractPreparationProtocol
{
    public: std::vector<std::shared_ptr<AbstractPrepareStep>>
            prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    ShortScanTrajectory(double sourceToIsocenter,
                        double startAngle = 0.0_deg,
                        double angleSpan = -1.0_deg,
                        bool rotateClockwise = false);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    double sourceToIsocenter = 0.0; //!< [mm]
    double startAngle        = 0.0_deg;
    double angleSpan         = -1.0_deg;
    bool   rotateClockwise   = false;

};

class AxialScanTrajectory : public AbstractPreparationProtocol
{
    public: std::vector<std::shared_ptr<AbstractPrepareStep>>
            prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    AxialScanTrajectory(double startAngle = 0.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    double startAngle = 0.0_deg;
};

class TomosynthesisCircleTrajectory : public AbstractPreparationProtocol
{
    public: std::vector<std::shared_ptr<AbstractPrepareStep>>
            prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisCircleTrajectory(double sourceToIsocenter,
                                  double tomoAngle = 15.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    double sourceToIsocenter = 0.0; //!< [mm]
    double tomoAngle         = 15.0_deg;
};

template <uint nbArcs>
class TomosynthesisMultiArcTrajectory : public AbstractPreparationProtocol
{
    static_assert(nbArcs > 1, "Multi arc trajectory must have nb. arcs larger than 1.");

    public: std::vector<std::shared_ptr<AbstractPrepareStep>>
            prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisMultiArcTrajectory(double sourceToIsocenter,
                                    double startAngle = 0.0_deg,
                                    double tomoAngle = 15.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    double sourceToIsocenter = 0.0; //!< [mm]
    double startAngle        = 0.0_deg;
    double tomoAngle         = 15.0_deg;
};

template <>
class TomosynthesisMultiArcTrajectory<2> : public AbstractPreparationProtocol
{
    public: std::vector<std::shared_ptr<AbstractPrepareStep>>
            prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisMultiArcTrajectory(double sourceToIsocenter,
                                    double startAngle = 0.0_deg,
                                    double tomoAngle  = 15.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    double sourceToIsocenter = 0.0; //!< [mm]
    double startAngle        = 0.0_deg;
    double tomoAngle         = 15.0_deg;
};

using WedgeTrajectory                 = TomosynthesisMultiArcTrajectory<2>;
using TomosynthesisTriangleTrajectory = TomosynthesisMultiArcTrajectory<3>;
using TomosynthesisSaddleTrajectory   = TomosynthesisMultiArcTrajectory<4>;


#if __cplusplus >= 201703L
class TomosynthesisEllipticalTrajectory : public AbstractPreparationProtocol
{
    public: std::vector<std::shared_ptr<AbstractPrepareStep>>
            prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisEllipticalTrajectory(double sourceToIsocenter,
                                      double startAngle = 0.0_deg,
                                      double smallAngle = 15.0_deg,
                                      double largeAngle = 23.0_deg,
                                      bool landscapeDetectorOrientation = false);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    double sourceToIsocenter     = 0.0; //!< [mm]
    double startAngle            = 0.0_deg;
    double smallAngle            = 15.0_deg;
    double largeAngle            = 23.0_deg;
    bool landscapeDetOrientation = false;

private:
    double computeAngularEllipsisParameter(uint viewNb, uint nbViews) const;
};
#endif // C++17

class TomosynthesisCrossTrajectory : public AbstractPreparationProtocol
{
    public: std::vector<std::shared_ptr<AbstractPrepareStep>>
            prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisCrossTrajectory(double sourceToIsocenter,
                                 double twistAngle = 0.0_deg,
                                 double smallAngle = 15.0_deg,
                                 double largeAngle = 23.0_deg,
                                 double fractionOfViewsHorizontal = 0.5);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    double sourceToIsocenter         = 0.0; //!< [mm]
    double twistAngle                = 0.0_deg;
    double smallAngle                = 15.0_deg;
    double largeAngle                = 23.0_deg;
    double fractionOfViewsHorizontal = 0.5;
};

class TomosynthesisLinearTrajectory : public AbstractPreparationProtocol
{
    public: std::vector<std::shared_ptr<AbstractPrepareStep>>
            prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisLinearTrajectory(double sourceToIsocenter,
                                  double startAngle = 0.0_deg,
                                  double tomoAngle  = 15.0_deg,
                                  bool verticalOrientation = false);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    double sourceToIsocenter = 0.0; //!< [mm]
    double startAngle        = 0.0_deg;
    double tomoAngle         = 15.0_deg;
    bool verticalOrientation = false;
};


// Template implementations
// ========================

template <uint nbArcs>
TomosynthesisMultiArcTrajectory<nbArcs>::TomosynthesisMultiArcTrajectory(double sourceToIsocenter,
                                                                         double startAngle,
                                                                         double tomoAngle)
    : sourceToIsocenter(sourceToIsocenter)
    , startAngle(startAngle)
    , tomoAngle(tomoAngle)
{
}

template <uint nbArcs>
std::vector<std::shared_ptr<AbstractPrepareStep>>
TomosynthesisMultiArcTrajectory<nbArcs>::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    const auto viewsPerArc = setup.nbViews() / static_cast<double>(nbArcs);
    constexpr auto innerAngle = 2.0 * PI / double(nbArcs);
    const auto arcFactor = std::sqrt((1.0 - std::cos(2.0 * PI / nbArcs)) /
                                     (1.0 + std::cos(2.0 * PI / nbArcs)));
    const auto arcAngle = 2.0 * std::atan(arcFactor * std::sin(tomoAngle));
    const auto angleIncrement = arcAngle / viewsPerArc;

    auto arcRot = [](double xAngle, double yAngle) {
        return mat::rotationMatrix(-xAngle + PI_2, Qt::XAxis) *
               mat::rotationMatrix(yAngle, Qt::YAxis);
    };

    const auto rotAngle = std::fmod(viewNb, viewsPerArc) * angleIncrement - 0.5 * arcAngle;
    const auto arcNb = std::floor(viewNb / viewsPerArc);

    const auto viewRotation = mat::rotationMatrix(-arcNb * innerAngle + startAngle, Qt::YAxis) *
                              arcRot(-tomoAngle, rotAngle);

    const auto viewPosition = viewRotation * Vector3x1(0.0, 0.0, -sourceToIsocenter);

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));

    return ret;
}

template <uint nbArcs>
bool TomosynthesisMultiArcTrajectory<nbArcs>::isApplicableTo(const AcquisitionSetup& setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

} // namespace protocols
} // namespace CTL

#endif // CTL_TRAJECTORIES_H
