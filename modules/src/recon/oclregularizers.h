#ifndef CTL_OCLREGULARIZERS_H
#define CTL_OCLREGULARIZERS_H

#include "processing/genericoclvolumefilter.h"

namespace CTL {
namespace OCL {

/*!
 * \brief The HuberRegularizer class is a regularizer based on the Huber potential function.
 *
 * This regularizer uses the so-called Huber potential, which consists of a quadratic part that
 * transitions into a linear part for arguments whose absolute value exceeds a defined threshold
 * (called Huber edge here). Consequently, voxel differences (wrt. absolute value) below the
 * threshold contribute to the gradient linearly wrt. their difference. Differences above said
 * threshold are capped by that threshold value, thus, resulting in a TV-like contribution to the
 * regularization.
 *
 * The regularizer can be configured through a set of parameters, with the following effects:
 * - \a regularizationStrength linearly scales the portion of the gradient that is applied to change
 * a single voxel's value in one execution of filter()
 * [1.0 means 1% of the gradient is applied].
 * - \a huberEdge defines the (absolute) difference threshold in multiples of 100 HU (at 50 keV) to
 * which differences will be clamped for computation of the gradient. For differences above the
 * threshold, the regularization effect is comparable to TV minimization
 * [1.0 means the Huber edges are +-100 HU].
 * - \a weightZ and \a weightXY specify the relative strength of the regularization in Z- and XY-
 * plane, respectively (in most settings, this refers to between-plane and in-plane regularization)
 * [\a weightZ = \a weightXY means all dimensions are treated equally].
 * - \a directZNeighborWeight is a separate multiplicative weight factor for the two voxels that are
 * direct Z-neighbors of the voxel of interest
 * [1.0 means the direct Z-Neigbors are treated the same as all other Z-Neigbors].
 */
class HuberRegularizer : public GenericOCLVolumeFilter
{
    CTL_TYPE_ID(2101)

public:
    HuberRegularizer(float regularizationStrength, float huberEdge = 1.0f,
                     float weightZ = 1.0f, float weightXY = 1.0f, float directZNeighborWeight = 1.0f);

    void setRegularizationStrength(float strength);

private:
    HuberRegularizer() = default;
    using GenericOCLVolumeFilter::setAdditionalKernelArg;
    using GenericOCLVolumeFilter::setAdditionalKernelArgs;
};

/*!
 * \brief The TVRegularizer class provides an approximation of a total variation (TV) minimizing
 * regularizer.
 *
 * This class implements an anisotropic version of total variation (TV) regularization.
 *
 * The strength of the regularization effect is controlled via the regularization parameter. This
 * defines the maximum change in multiples of 10 Hounsfield units (HU) that can be applied to a
 * single voxel in one execution of filter().
 *
 * This change is applied proportionally, depending on the average number of neighbors of that voxel
 * that have a positive / negative difference to the voxel. Hence, if all surrounding voxels have
 * higher/lower values than the voxel of interest, its value will be increased/decreased by
 * \a regularizationStrength * 10 HU. If the tendency of neighbor values is mixed, only the
 * corresponding fraction of change is applied, respectively.
 */
class TVRegularizer : public GenericOCLVolumeFilter
{
    CTL_TYPE_ID(2102)

public:
    TVRegularizer(float regularizationStrength);

    void setRegularizationStrength(float strength);

private:
    TVRegularizer() = default;
    using GenericOCLVolumeFilter::setAdditionalKernelArg;
    using GenericOCLVolumeFilter::setAdditionalKernelArgs;
};

} // namespace OCL
} // namespace CTL

#endif // CTL_OCLREGULARIZERS_H
