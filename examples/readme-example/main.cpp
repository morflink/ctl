#include "ctl.h"
#include "ctl_ocl.h"
#include "ctl_qtgui.h"
#include <QApplication>

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);

    // create a cylinder as a volume
    const float radius = 60.0, height = 100.0, voxelSize = 0.5, fillValue = 0.03;
    const CTL::VoxelVolume<float> volume = CTL::VoxelVolume<float>::cylinderX(radius, height,
                                                                              voxelSize, fillValue);

    // create an acquisition setup using a predefined system
    const uint nbViews = 100;
    CTL::AcquisitionSetup myCarmSetup{ CTL::makeCTSystem<CTL::blueprints::GenericCarmCT>(),
                                       nbViews };
    // add a predefined trajectory to the setup
    const double sourceToIsocenter = 750.0; // mm is the standard unit for length dimensions
    const double startAngle = 42.0_deg; // floating-point literal _deg converts to rad
    myCarmSetup.applyPreparationProtocol(CTL::protocols::WobbleTrajectory{ sourceToIsocenter,
                                                                           startAngle });
    if(!myCarmSetup.isValid())
        return -1;

    // plot setup
    CTL::gui::plot(myCarmSetup);

    // configure a projector and project volume
    CTL::OCL::RayCasterProjector myProjector; // an ideal projector with default settings
    const auto projections = myProjector.configureAndProject(myCarmSetup, volume);

    // plot projections
    CTL::gui::plot(projections);

    return a.exec();
}

/*
 * NOTE: the project file (.pro) needs to include the following modules to be
 * able to compile this example program:
 *
 *  include(path/to/ctl.pri)
 *  include(path/to/ctl_ocl.pri)
 *  include(path/to/ctl_qtgui.pri)
 */
