# dependency
!OCL_CONFIG_MODULE: error("OCL_ROUTINES_MODULE needs OCL_CONFIG_MODULE -> include ocl_config.pri before ocl_routines.pri")
!CTL_CORE_MODULE: error("OCL_ROUTINES_MODULE needs CTL_CORE_MODULE -> include ctl_core.pri before ocl_routines.pri")

# declare module
CONFIG += OCL_ROUTINES_MODULE
DEFINES += OCL_ROUTINES_MODULE_AVAILABLE

HEADERS += \
    $$PWD/../src/processing/consistency.h \
    $$PWD/../src/processing/genericoclprojectionfilter.h \
    $$PWD/../src/processing/genericoclvolumefilter.h \
    $$PWD/../src/processing/imageresampler.h \
    $$PWD/../src/processing/oclprojectionfilters.h \
    $$PWD/../src/processing/radontransform2d.h \
    $$PWD/../src/processing/radontransform3d.h \
    $$PWD/../src/projectors/raycaster.h \
    $$PWD/../src/projectors/raycasteradapter.h \
    $$PWD/../src/projectors/raycasterprojector.h \
    $$PWD/../src/processing/volumeresampler.h \
    $$PWD/../src/processing/volumeslicer.h \
    $$PWD/../src/projectors/sfpprojector.h \
    $$PWD/../src/projectors/standardpipeline.h \
    $$PWD/../src/recon/artreconstructor.h \
    $$PWD/../src/recon/backprojectorbase.h \
    $$PWD/../src/recon/fdkreconstructor.h \
    $$PWD/../src/recon/oclregularizers.h \
    $$PWD/../src/recon/sfpbackprojector.h \
    $$PWD/../src/recon/simplebackprojector.h \
    $$PWD/../src/registration/abstractregistration2d3d.h

SOURCES += \
    $$PWD/../src/processing/consistency.cpp \
    $$PWD/../src/processing/genericoclprojectionfilter.cpp \
    $$PWD/../src/processing/genericoclvolumefilter.cpp \
    $$PWD/../src/processing/imageresampler.cpp \
    $$PWD/../src/processing/oclprojectionfilters.cpp \
    $$PWD/../src/processing/radontransform2d.cpp \
    $$PWD/../src/processing/radontransform3d.cpp \
    $$PWD/../src/processing/volumeresampler.cpp \
    $$PWD/../src/processing/volumeslicer.cpp \
    $$PWD/../src/projectors/raycaster.cpp \
    $$PWD/../src/projectors/raycasteradapter.cpp \
    $$PWD/../src/projectors/raycasterprojector.cpp \
    $$PWD/../src/projectors/sfpprojector.cpp \
    $$PWD/../src/projectors/standardpipeline.cpp \
    $$PWD/../src/recon/artreconstructor.cpp \
    $$PWD/../src/recon/backprojectorbase.cpp \
    $$PWD/../src/recon/fdkreconstructor.cpp \
    $$PWD/../src/recon/oclregularizers.cpp \
    $$PWD/../src/recon/sfpbackprojector.cpp \
    $$PWD/../src/recon/simplebackprojector.cpp \
    $$PWD/../src/registration/abstractregistration2d3d.cpp


# OpenCL source files
DISTFILES += \
    $$PWD/../src/ocl/cl_src/processing/apodization_filter.cl \
    $$PWD/../src/ocl/cl_src/processing/cosine_weighting.cl \
    $$PWD/../src/ocl/cl_src/processing/ramlak_filter.cl \
    $$PWD/../src/ocl/cl_src/processing/rev_parker_weighting.cl \
    $$PWD/../src/ocl/cl_src/projectors/external_raycaster.cl \
    $$PWD/../src/ocl/cl_src/projectors/projection_tr.cl \
    $$PWD/../src/ocl/cl_src/projectors/projection_tr_atomicFloat.cl \
    $$PWD/../src/ocl/cl_src/projectors/projection_tt.cl \
    $$PWD/../src/ocl/cl_src/projectors/projection_tt_atomicFloat.cl \
    $$PWD/../src/ocl/cl_src/projectors/projection_tt_generic.cl \
    $$PWD/../src/ocl/cl_src/projectors/projection_tt_generic_atomicFloat.cl \
    $$PWD/../src/ocl/cl_src/projectors/projection_tt_generic_sparse.cl \
    $$PWD/../src/ocl/cl_src/projectors/projection_tt_generic_sparse_atomicFloat.cl \
    $$PWD/../src/ocl/cl_src/projectors/raycasterprojector_interp.cl \
    $$PWD/../src/ocl/cl_src/processing/hilbert_filter.cl \
    $$PWD/../src/ocl/cl_src/processing/homToRadon.cl \
    $$PWD/../src/ocl/cl_src/processing/imageResampler.cl \
    $$PWD/../src/ocl/cl_src/processing/planeIntegral.cl \
    $$PWD/../src/ocl/cl_src/processing/radon2d.cl \
    $$PWD/../src/ocl/cl_src/processing/radonToHom.cl \
    $$PWD/../src/ocl/cl_src/processing/volumeResampler.cl \
    $$PWD/../src/ocl/cl_src/processing/volumeSlicer.cl \
    $$PWD/../src/ocl/cl_src/projectors/raycasterprojector_no_images.cl \
    $$PWD/../src/ocl/cl_src/projectors/raycasterprojector_no_interp.cl \
    $$PWD/../src/ocl/cl_src/recon/backprojection_tr.cl \
    $$PWD/../src/ocl/cl_src/recon/backprojection_tt.cl \
    $$PWD/../src/ocl/cl_src/recon/backprojection_tt_generic.cl \
    $$PWD/../src/ocl/cl_src/recon/huber_regularizer.cl \
    $$PWD/../src/ocl/cl_src/recon/nullifier.cl \
    $$PWD/../src/ocl/cl_src/recon/simple_backprojector.cl \
    $$PWD/../src/ocl/cl_src/recon/tv_regularizer.cl
