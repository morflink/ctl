# The purpose of this file is to define a CMake target `CTL` that you can link in your CMake project
# after you've compiled the QMake project "ctl.pro".
#
# Add the following to your "CMakeLists.txt" file:
#
#    include("path/to/this/ctl.cmake")
#    target_link_libraries(myTarget CTL)
#
# You should then be able to use the CTL headers in your C++ code, e.g. the meta headers:
#
#    #include <ctl.h>
#    #include <ctl_ocl.h>
#    #include <ctl_qtgui.h>
#
# NOTE: So far, the CTL NLopt module is not included.
#       If you want to use it as well, uncomment line 49 and 61 and line 17 in "ctl.pro".

# Qt
find_package(Qt6 COMPONENTS Core Widgets Charts 3DExtras REQUIRED)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

# OpenCL
find_package(OpenCL REQUIRED)

# CTL
add_library(CTL INTERFACE)

# NLopt
# ...missing...

target_compile_definitions(CTL INTERFACE
# +++++ CTL main module   +++++
    CTL_CORE_MODULE_AVAILABLE
    DEN_FILE_IO_MODULE_AVAILABLE
    NRRD_FILE_IO_MODULE_AVAILABLE

# +++++ CTL OpenCL module +++++
    OCL_CONFIG_MODULE_AVAILABLE
    OCL_ROUTINES_MODULE_AVAILABLE

# +++++ CTL QtGUI module  +++++
    GUI_WIDGETS_MODULE_AVAILABLE
    GUI_WIDGETS_3D_MODULE_AVAILABLE
    GUI_WIDGETS_CHARTS_MODULE_AVAILABLE
    GUI_WIDGETS_OCL_AVAILABLE

# +++++ CTL NLopt module  +++++
    # REGIST_2D3D_MODULE_AVAILABLE
    )

target_include_directories(CTL SYSTEM INTERFACE ${CMAKE_CURRENT_LIST_DIR}/../modules/src)
target_link_directories(CTL SYSTEM INTERFACE ${CMAKE_CURRENT_LIST_DIR})

target_link_libraries(CTL INTERFACE
    Qt6::Core
    Qt6::Widgets
    Qt6::Charts
    Qt6::3DExtras
    OpenCL::OpenCL
    # nlopt
    ctl
    )

if (MSVC)
    target_link_options(CTL INTERFACE "/WHOLEARCHIVE:ctl")
endif ()
