#include "trajectories.h"
#include "components/carmgantry.h"
#include "components/cylindricaldetector.h"
#include "components/flatpaneldetector.h"

namespace CTL {
namespace protocols {

namespace {
double fanAngle(const AcquisitionSetup& setup);
} // anonymous namespace

HelicalTrajectory::HelicalTrajectory(double angleIncrement,
                                     double pitchIncrement,
                                     double startPitch,
                                     double startAngle)
    : angleIncrement(angleIncrement)
    , pitchIncrement(pitchIncrement)
    , startPitch(startPitch)
    , startAngle(startAngle)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
HelicalTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup&) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::TubularGantryParam>();

    gantryPrep->setPitchPosition(viewNb * pitchIncrement + startPitch);
    gantryPrep->setRotationAngle(viewNb * angleIncrement + startAngle);

    ret.push_back(std::move(gantryPrep));

    qDebug() << "HelicalTrajectory --- add prepare steps for view: " << viewNb
             << "\n-rotation: " << viewNb * angleIncrement + startAngle
             << "\n-pitch: " << viewNb * pitchIncrement + startPitch;

    return ret;
}

bool HelicalTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::TubularGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

WobbleTrajectory::WobbleTrajectory(double sourceToIsocenter,
                                   double startAngle,
                                   double angleSpan,
                                   double wobbleAngle,
                                   double wobbleFreq)
    : sourceToIsocenter(sourceToIsocenter)
    , startAngle(startAngle)
    , angleSpan(angleSpan)
    , wobbleAngle(wobbleAngle)
    , wobbleFreq(wobbleFreq)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
WobbleTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();

    double usedAngleSpan;
    if(angleSpan >= 0.0)
        usedAngleSpan = angleSpan;
    else
        usedAngleSpan = 180.0_deg + fanAngle(setup);

    const uint nbViews = setup.nbViews();
    const Vector3x1 initialSrcPos(0.0, 0.0, -sourceToIsocenter);
    const Matrix3x3 fixedRotMat = mat::rotationMatrix(PI_2 + startAngle, Qt::ZAxis)
        * mat::rotationMatrix(-PI_2, Qt::XAxis);
    const double angleIncrement = (nbViews > 1) ? usedAngleSpan / double(nbViews - 1) : 0.0;

    const auto wobblePhase = std::sin(double(viewNb) / double(nbViews) * 2.0 * PI * wobbleFreq);
    const auto wobbleRotMat = mat::rotationMatrix(wobblePhase * wobbleAngle, Qt::XAxis);

    const auto viewRotation
        = mat::rotationMatrix(viewNb * angleIncrement, Qt::ZAxis) * fixedRotMat * wobbleRotMat;
    const auto viewPosition = viewRotation * initialSrcPos;

    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));

    ret.push_back(std::move(gantryPrep));

    return ret;
}

bool WobbleTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

CirclePlusLineTrajectory::CirclePlusLineTrajectory(double sourceToIsocenter,
                                                   double lineLength,
                                                   double startAngle,
                                                   double angleSpan,
                                                   double fractionOfViewsForLine)
    : sourceToIsocenter(sourceToIsocenter)
    , lineLength(lineLength)
    , startAngle(startAngle)
    , angleSpan(angleSpan)
    , fractionOfViewsForLine(fractionOfViewsForLine)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
CirclePlusLineTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();

    const uint nbViews = setup.nbViews();
    const uint nbViewsLine   = static_cast<uint>(std::floor(nbViews * fractionOfViewsForLine));
    const uint nbViewsCircle = nbViews - nbViewsLine;
    const Vector3x1 initialSrcPos(0.0, 0.0, -sourceToIsocenter);
    const Matrix3x3 fixedRotMat = mat::rotationMatrix(PI_2 + startAngle, Qt::ZAxis)
        * mat::rotationMatrix(-PI_2, Qt::XAxis);

    Vector3x1 viewPosition;
    Matrix3x3 viewRotation;

    if(viewNb < nbViewsCircle)
    {
        const double angleIncrement = (nbViewsCircle > 1) ? angleSpan / double(nbViewsCircle - 1) : 0.0;

        viewRotation = mat::rotationMatrix(viewNb * angleIncrement, Qt::ZAxis) * fixedRotMat;
        viewPosition = viewRotation * initialSrcPos;
    }
    else
    {
        const uint lineViewNb = viewNb - nbViewsCircle;
        const double lineIncrement = (nbViewsLine > 1) ? lineLength / double(nbViewsLine - 1) : 0.0;

        viewRotation = mat::rotationMatrix(angleSpan/2.0, Qt::ZAxis) * fixedRotMat;
        viewPosition = viewRotation * initialSrcPos;
        viewPosition.get<2>() += (lineViewNb - nbViewsLine/2.0) * lineIncrement;
    }

    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));

    ret.push_back(std::move(gantryPrep));

    return ret;
}

bool CirclePlusLineTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

ShortScanTrajectory::ShortScanTrajectory(double sourceToIsocenter, double startAngle, double angleSpan, bool rotateClockwise)
    : sourceToIsocenter(sourceToIsocenter)
    , startAngle(startAngle)
    , angleSpan(angleSpan)
    , rotateClockwise(rotateClockwise)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep> > ShortScanTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup &setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();

    double usedAngleSpan;
    if(angleSpan >= 0.0)
        usedAngleSpan = angleSpan;
    else
        usedAngleSpan = 180.0_deg + fanAngle(setup);

    qDebug() << "short scan angle span:" << usedAngleSpan * 180.0 / PI << "deg";

    const uint nbViews = setup.nbViews();
    const Vector3x1 initialSrcPos(0.0, 0.0, -sourceToIsocenter);
    const Matrix3x3 fixedRotMat = mat::rotationMatrix(PI_2 + startAngle, Qt::ZAxis)
        * mat::rotationMatrix(-PI_2, Qt::XAxis);
    const auto rotDirection = rotateClockwise ? -1.0 : 1.0;
    const double angleIncrement = (nbViews > 1) ? rotDirection * usedAngleSpan / double(nbViews - 1)
                                                : 0.0;

    const auto viewRotation = mat::rotationMatrix(viewNb * angleIncrement, Qt::ZAxis) * fixedRotMat;
    const auto viewPosition = viewRotation * initialSrcPos;

    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));

    ret.push_back(std::move(gantryPrep));

    return ret;
}

bool ShortScanTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

AxialScanTrajectory::AxialScanTrajectory(double startAngle)
    : startAngle(startAngle)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
AxialScanTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    float angleIncrement = (setup.nbViews() > 0) ? 360.0_deg / setup.nbViews()
                                                 : 0.0;
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::TubularGantryParam>();

    gantryPrep->setRotationAngle(viewNb * angleIncrement + startAngle);

    ret.push_back(std::move(gantryPrep));

    qDebug() << "AxialScanTrajectory --- add prepare steps for view: " << viewNb
             << "\n-rotation: " << viewNb * angleIncrement + startAngle;

    return ret;
}

bool AxialScanTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::TubularGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

TomosynthesisCircleTrajectory::TomosynthesisCircleTrajectory(double sourceToIsocenter,
                                                             double tomoAngle)
    : sourceToIsocenter(sourceToIsocenter)
    , tomoAngle(tomoAngle)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
TomosynthesisCircleTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    const auto nbViews = setup.nbViews();
    const auto fixedRotMat = mat::rotationMatrix(PI_2 + tomoAngle, Qt::XAxis);
    const auto angleIncrement = 360.0_deg / nbViews;

    const auto viewRotation = mat::rotationMatrix(viewNb * angleIncrement, Qt::YAxis) * fixedRotMat;
    const auto viewPosition = viewRotation * Vector3x1(0.0, 0.0, -sourceToIsocenter);

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));

    return ret;
}

bool TomosynthesisCircleTrajectory::isApplicableTo(const AcquisitionSetup& setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}


TomosynthesisMultiArcTrajectory<2>::TomosynthesisMultiArcTrajectory(double sourceToIsocenter,
                                                                    double startAngle,
                                                                    double tomoAngle)
    : sourceToIsocenter(sourceToIsocenter)
    , startAngle(startAngle)
    , tomoAngle(tomoAngle)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
TomosynthesisMultiArcTrajectory<2>::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    const auto viewsPerArc = setup.nbViews() / 2.0;
    constexpr auto arcAngle = PI;
    const auto angleIncrement = arcAngle / viewsPerArc;

    auto arcRot = [](double xAngle, double yAngle) {
        return mat::rotationMatrix(-xAngle + PI_2, Qt::XAxis) *
               mat::rotationMatrix(yAngle, Qt::YAxis);
    };

    const auto rotAngle = std::fmod(viewNb, viewsPerArc) * angleIncrement - 0.5 * arcAngle;
    const auto arcNb = std::floor(viewNb / viewsPerArc);

    const Matrix3x3 viewRotation = mat::rotationMatrix(-arcNb * PI + startAngle, Qt::YAxis) *
                                   arcRot(-tomoAngle, rotAngle);

    const auto viewPosition = viewRotation * Vector3x1(0.0, 0.0, -sourceToIsocenter);

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));

    return ret;
}

bool TomosynthesisMultiArcTrajectory<2>::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

#if __cplusplus >= 201703L
TomosynthesisEllipticalTrajectory::TomosynthesisEllipticalTrajectory(
    double sourceToIsocenter,
    double startAngle,
    double smallAngle,
    double largeAngle,
    bool landscapeDetectorOrientation)
    : sourceToIsocenter(sourceToIsocenter)
    , startAngle(startAngle)
    , smallAngle(smallAngle)
    , largeAngle(largeAngle)
    , landscapeDetOrientation(landscapeDetectorOrientation)
{
    if(smallAngle > largeAngle)
        throw std::domain_error("TomosynthesisEllipticalTrajectory: "
                                "smallAngle must not be larger than largeAngle.");
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
TomosynthesisEllipticalTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    const auto nbViews = setup.nbViews();

    // "portrait" detector orientation:
    auto fixedRotMat = mat::rotationMatrix(PI_2, Qt::XAxis);
    // "landscape" detector orientation:
    if(landscapeDetOrientation)
        fixedRotMat = mat::rotationMatrix(-PI_2, Qt::YAxis) * fixedRotMat;

    const auto smallRadius = sourceToIsocenter * std::tan(smallAngle);
    const auto largeRadius = sourceToIsocenter * std::tan(largeAngle);
    const auto angleParam = computeAngularEllipsisParameter(viewNb, nbViews);
    const auto locus = Vector3x1{ largeRadius * std::cos(angleParam),
                                  sourceToIsocenter,
                                  smallRadius * std::sin(angleParam) };

    // construct rotation from Y-axis to locus
    const auto rotColumnY = locus.normalized();
    const auto rotColumnZ = mat::cross(locus, Vector3x1{ 0.0, 1.0, 0.0 }).normalized();
    const auto rotColumnX = mat::cross(rotColumnY, rotColumnZ);
    const auto rotEllips = mat::horzcat(mat::horzcat(rotColumnX, rotColumnY), rotColumnZ);

    auto viewRotation = rotEllips * fixedRotMat;
    viewRotation = mat::rotationMatrix(startAngle, Qt::YAxis) * viewRotation;
    const auto viewPosition = viewRotation * Vector3x1(0.0, 0.0, -sourceToIsocenter);

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));

    return ret;
}

double TomosynthesisEllipticalTrajectory::computeAngularEllipsisParameter(uint viewNb,
                                                                          uint nbViews) const
{
    const double m = 1 - std::pow(std::tan(smallAngle) / std::tan(largeAngle), 2);
    const double k = std::sqrt(m);
    const double perimeter = 4 * sourceToIsocenter * std::tan(largeAngle) * std::comp_ellint_2(k);
    const double arcLength = perimeter / nbViews;
    const double z = std::comp_ellint_2(k) - viewNb * arcLength
                     / (sourceToIsocenter * std::tan(largeAngle));
    const double ksi = 1 - z / std::ellint_2(k, PI_2);
    const double r = std::sqrt(std::pow(1 - m, 2) + std::pow(ksi, 2));
    const double theta = std::atan((1 - m) / ksi);

    double phi = PI_2 + std::sqrt(r) * (theta - PI_2);

    for(int i = 1; ; ++i)
    { 
        constexpr auto maxNewtonIterations = 300;
        constexpr auto tol = 1.0e-15;

        const double error = std::ellint_2(k, phi) - z;
        phi -= error / std::sqrt(1 - m * std::sin(phi) * std::sin(phi));

        qDebug() << "viewNb" << viewNb
                 << ": the inverse elliptic integral estimation error at iteration" << i
                 << "was" << error;

        // stopping criteria
        if(std::abs(error) < tol)
            break;
        if(i >= maxNewtonIterations)
        {
            qWarning("inverse elliptic integral estimation has not converged");
            break;
        }
    }
        
    auto eta = std::acos(std::sin(phi));
    if(viewNb > nbViews / 2)
        eta = 2 * PI - eta;
    return eta;
}

bool TomosynthesisEllipticalTrajectory::isApplicableTo(const AcquisitionSetup& setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}
#endif // C++17

/*!
 * For an equal sampling along the vertical and the horizontal orbits, choose
 * fractionOfViewsHorizontal = ((nbViews - 1) * largeAngle + smallAngle) / (nbViews * (smallAngle + largeAngle));
 */
TomosynthesisCrossTrajectory::TomosynthesisCrossTrajectory(double sourceToIsocenter,
                                                                     double twistAngle,
                                                                     double smallAngle,
                                                                     double largeAngle,
                                                                     double fractionOfViewsHorizontal)
    : sourceToIsocenter(sourceToIsocenter)
    , twistAngle(twistAngle)
    , smallAngle(smallAngle)
    , largeAngle(largeAngle)
    , fractionOfViewsHorizontal(fractionOfViewsHorizontal)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep> >
TomosynthesisCrossTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup &setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    const uint nbViews = setup.nbViews();
    const uint nbViewsHorizontal   = static_cast<uint>(std::floor(nbViews * fractionOfViewsHorizontal));
    const uint nbViewsVertical = nbViews - nbViewsHorizontal;
    const Vector3x1 initialSrcPos(0.0, 0.0, -sourceToIsocenter);
    Vector3x1 viewPosition;
    Matrix3x3 viewRotation;

    if(viewNb < nbViewsVertical)
    {
        Matrix3x3 fixedRotMat = mat::rotationMatrix(PI_2, Qt::YAxis) * mat::rotationMatrix(PI_2, Qt::XAxis);
        fixedRotMat = mat::rotationMatrix(-smallAngle, Qt::XAxis) * fixedRotMat;
        const double angleIncrement = (nbViewsVertical > 1) ? 2.0 * smallAngle / double(nbViewsVertical - 1) : 0.0;
        viewRotation = mat::rotationMatrix(viewNb * angleIncrement, Qt::XAxis) * fixedRotMat;
        viewRotation = mat::rotationMatrix(twistAngle, Qt::YAxis) * viewRotation;
        viewPosition = viewRotation * initialSrcPos;
    }
    else
    {
        const uint horizontalViewNb = viewNb - nbViewsVertical;
        Matrix3x3 fixedRotMat = mat::rotationMatrix(-PI, Qt::YAxis) * mat::rotationMatrix(PI_2, Qt::XAxis);
        fixedRotMat = mat::rotationMatrix(-largeAngle, Qt::ZAxis) * fixedRotMat;
        const double angleIncrement = (nbViewsHorizontal > 1) ? 2.0 * largeAngle / double(nbViewsHorizontal - 1) : 0.0;
        viewRotation = mat::rotationMatrix(horizontalViewNb * angleIncrement, Qt::ZAxis) * fixedRotMat;
        viewRotation = mat::rotationMatrix(twistAngle, Qt::YAxis) * viewRotation;
        viewPosition = viewRotation * initialSrcPos;

    }

    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));
    return ret;
}

bool TomosynthesisCrossTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

TomosynthesisLinearTrajectory::TomosynthesisLinearTrajectory(double sourceToIsocenter,
                                                             double startAngle,
                                                             double tomoAngle,
                                                             bool verticalOrientation)
    : sourceToIsocenter(sourceToIsocenter)
    , startAngle(startAngle)
    , tomoAngle(tomoAngle)
    , verticalOrientation(verticalOrientation)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep> >
TomosynthesisLinearTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup &setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    const uint nbViews = setup.nbViews();
    const Vector3x1 initialSrcPos(0.0, 0.0, -sourceToIsocenter);
    const double angleIncrement = (nbViews > 1) ? 2.0 * tomoAngle / double(nbViews - 1) : 0.0;
    Vector3x1 viewPosition;
    Matrix3x3 viewRotation;

    if(verticalOrientation)
    {
        Matrix3x3 fixedRotMat
            = mat::rotationMatrix(PI_2, Qt::YAxis) * mat::rotationMatrix(PI_2, Qt::XAxis);
        fixedRotMat = mat::rotationMatrix(-startAngle - tomoAngle, Qt::XAxis) * fixedRotMat;
        viewRotation = mat::rotationMatrix(viewNb * angleIncrement, Qt::XAxis) * fixedRotMat;
    } else
    {
        Matrix3x3 fixedRotMat
            = mat::rotationMatrix(-PI, Qt::YAxis) * mat::rotationMatrix(PI_2, Qt::XAxis);
        fixedRotMat = mat::rotationMatrix(-startAngle - tomoAngle, Qt::ZAxis) * fixedRotMat;
        viewRotation = mat::rotationMatrix(viewNb * angleIncrement, Qt::ZAxis) * fixedRotMat;
    }
    viewPosition = viewRotation * initialSrcPos;

    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));
    return ret;
}


bool TomosynthesisLinearTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}


namespace {

double fanAngle(const AcquisitionSetup& setup)
{
    auto detector = setup.system()->detector();
    auto gantry   = static_cast<CarmGantry*>(setup.system()->gantry());

    double relevantWidth;
    switch (detector->type()) {
    case CylindricalDetector::Type:
    {
        const auto cylDetPtr = static_cast<CylindricalDetector*>(detector);
        const auto cylFan = cylDetPtr->fanAngle();
        relevantWidth = 2.0 * cylDetPtr->curvatureRadius() * std::sin(cylFan / 2.0);
        break;
    }
    case FlatPanelDetector::Type:
    {
        relevantWidth = static_cast<FlatPanelDetector*>(detector)->panelDimensions().width();
        break;
    }
    default:
        relevantWidth = 0.0;
        break;
    }

    return 2.0 * std::atan(0.5 * relevantWidth / gantry->cArmSpan());
}

} // anonymous namespace

} // namespace protocols
} // namespace CTL
