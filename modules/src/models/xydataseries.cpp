#include "xydataseries.h"
#include "abstractdatamodel.h"
#include "processing/range.h"
#include <QVector>
#include <stdexcept>

namespace CTL {

/*!
 * Constructs an XYDataSeries object and sets its data to \a dataSeries.
 */
XYDataSeries::XYDataSeries(QList<QPointF>&& dataSeries)
    : PointSeriesBase(std::move(dataSeries))
{
}

/*!
 * Constructs an XYDataSeries object and sets its data to \a dataSeries.
 */
XYDataSeries::XYDataSeries(const QList<QPointF>& dataSeries)
    : PointSeriesBase(dataSeries)
{
}

/*!
 * Constructs an XYDataSeries object and sets its data to the (x,y)-tuples formed by \a x and \a y.
 * \a x and \a y must have the same size; throws an exception otherwise.
 *
 * Example:
 * \code
 * QVector<float> x(5), y(5);
 * std::iota(x.begin(), x.end(), 0.0f);
 * std::generate(y.begin(), y.end(), std::mt19937());
 *
 * // create a series from x and y
 * XYDataSeries series(x, y);
 *
 * qInfo() << series.data();
 * // output: (QPointF(0,3.49921e+09), QPointF(1,5.81869e+08), QPointF(2,3.89035e+09), QPointF(3,3.58633e+09), QPointF(4,5.45404e+08))
 * \endcode
 */
XYDataSeries::XYDataSeries(const QVector<float>& x, const QVector<float>& y)
{
    if(x.size() != y.size())
        throw std::domain_error("XYDataSeries(const QVector<float>& x, const QVector<float>& y): "
                                "Vectors x and y must have same size");

    const auto nbSmpPts = static_cast<uint>(x.size());
    _data.reserve(nbSmpPts);
    for(uint smpPt = 0; smpPt<nbSmpPts; ++smpPt)
        append(QPointF(x.at(smpPt), y.at(smpPt)));
}

/*!
 * Constructs an XYDataSeries object and sets its data to the (x,y)-tuples formed by \a x and \a y.
 * \a x and \a y must have the same size; throws an exception otherwise.
 *
 * Example:
 * \code
 * std::vector<float> x(5), y(5);
 * std::iota(x.begin(), x.end(), 0.0f);
 * std::generate(y.begin(), y.end(), std::mt19937());
 *
 * // create a series from x and y
 * XYDataSeries series(x, y);
 *
 * qInfo() << series.data();
 * // output: (QPointF(0,3.49921e+09), QPointF(1,5.81869e+08), QPointF(2,3.89035e+09), QPointF(3,3.58633e+09), QPointF(4,5.45404e+08))
 * \endcode
 */
XYDataSeries::XYDataSeries(const std::vector<float>& x, const std::vector<float>& y)
{
    if(x.size() != y.size())
        throw std::domain_error("XYDataSeries(const std::vector<float>& x, const std::vector<float>& y): "
                                "Vectors x and y must have same size");

    const auto nbSmpPts = static_cast<uint>(x.size());
    _data.reserve(nbSmpPts);
    for(uint smpPt = 0; smpPt<nbSmpPts; ++smpPt)
        append(QPointF(x[smpPt], y[smpPt]));
}

/*!
 * Creates an XYDataSeries by sampling values from \a dataModel at each position specified in
 * \a samplingPoints.
 *
 * Example:
 * \code
 * GaussianModel1D gaussian;
 *
 * // define the sampling points and sample the series
 * auto samplingPts = QVector<float>( { -0.42f, 1.337f, 6.66f } );
 * auto sampledSeries = XYDataSeries::sampledFromModel(gaussian, samplingPts);
 *
 * qInfo() << sampledSeries.data();
 * // output: (QPointF(-0.42,0.365263), QPointF(1.337,0.163209), QPointF(6.66,9.31567e-11))
 * \endcode
 */
XYDataSeries XYDataSeries::sampledFromModel(const AbstractDataModel& dataModel,
                                            const QVector<float>& samplingPoints)
{
    XYDataSeries ret;

    for(auto smpPt : samplingPoints)
        ret.append(QPointF(smpPt, dataModel.valueAt(smpPt)));

    return ret;
}

/*!
 * Creates an XYDataSeries by sampling values from \a dataModel at each position specified in
 * \a samplingPoints.
 *
 * Example:
 * \code
 * auto gaussian = std::make_shared<GaussianModel1D>();
 *
 * // define the sampling points and sample the series
 * auto samplingPts = QVector<float>( { -0.42f, 1.337f, 6.66f } );
 * auto sampledSeries = XYDataSeries::sampledFromModel(gaussian, samplingPts);
 *
 * qInfo() << sampledSeries.data();
 * // output: (QPointF(-0.42,0.365263), QPointF(1.337,0.163209), QPointF(6.66,9.31567e-11))
 * \endcode
 */
XYDataSeries XYDataSeries::sampledFromModel(std::shared_ptr<AbstractDataModel> dataModel,
                                            const QVector<float>& samplingPoints)
{
    return sampledFromModel(*dataModel, samplingPoints);
}

/*!
 * Creates an XYDataSeries by sampling values from \a dataModel at each position specified in
 * \a samplingPoints.
 *
 * Example:
 * \code
 * GaussianModel1D gaussian;
 *
 * // define the sampling points and sample the series
 * auto samplingPts = std::vector<float>( { -0.42f, 1.337f, 6.66f } );
 * auto sampledSeries = XYDataSeries::sampledFromModel(gaussian, samplingPts);
 *
 * qInfo() << sampledSeries.data();
 * // output: (QPointF(-0.42,0.365263), QPointF(1.337,0.163209), QPointF(6.66,9.31567e-11))
 * \endcode
 */
XYDataSeries XYDataSeries::sampledFromModel(const AbstractDataModel& dataModel,
                                            const std::vector<float>& samplingPoints)
{
    XYDataSeries ret;

    for(auto smpPt : samplingPoints)
        ret.append(QPointF(smpPt, dataModel.valueAt(smpPt)));

    return ret;
}

/*!
 * Creates an XYDataSeries by sampling values from \a dataModel at each position specified in
 * \a samplingPoints.
 *
 * Example:
 * \code
 * auto gaussian = std::make_shared<GaussianModel1D>();
 *
 * // define the sampling points and sample the series
 * auto samplingPts = std::vector<float>( { -0.42f, 1.337f, 6.66f } );
 * auto sampledSeries = XYDataSeries::sampledFromModel(gaussian, samplingPts);
 *
 * qInfo() << sampledSeries.data();
 * // output: (QPointF(-0.42,0.365263), QPointF(1.337,0.163209), QPointF(6.66,9.31567e-11))
 * \endcode
 */
XYDataSeries XYDataSeries::sampledFromModel(std::shared_ptr<AbstractDataModel> dataModel,
                                            const std::vector<float>& samplingPoints)
{
    return sampledFromModel(*dataModel, samplingPoints);
}

/*!
 * Creates an XYDataSeries by sampling values from \a dataModel at \a nbSamples points covering
 * the range specified by \a samplingRange. Sampling points will be distributed across the range
 * according to the flag specified by \a samplingPattern. This can be either Linear or Exponential,
 * resulting in a linear (i.e. equidistant samples) pattern or sampling points with exponentially
 * increasing distances, respectively.
 *
 * Example:
 * \code
 * GaussianModel1D gaussian;
 *
 * // define a sampling range from -2 to 2
 * auto range = SamplingRange(-2.0f, 2.0f);
 * // sample 5 (equidistantly spaced) points from the model
 * auto sampledSeries = XYDataSeries::sampledFromModel(gaussian, range, 5);
 *
 * qInfo() << sampledSeries.data();
 * // output: (QPointF(-2,0.053991), QPointF(-1,0.241971), QPointF(0,0.398942), QPointF(1,0.241971), QPointF(2,0.053991))
 * \endcode
 */
XYDataSeries XYDataSeries::sampledFromModel(const AbstractDataModel& dataModel,
                                            SamplingRange samplingRange,
                                            uint nbSamples,
                                            Sampling samplingPattern)
{
    return sampledFromModel(dataModel, samplingRange.start(), samplingRange.end(),
                            nbSamples, samplingPattern);
}

/*!
 * Creates an XYDataSeries by sampling values from \a dataModel at \a nbSamples points covering
 * the range specified by \a samplingRange. Sampling points will be distributed across the range
 * according to the flag specified by \a samplingPattern. This can be either Linear or Exponential,
 * resulting in a linear (i.e. equidistant samples) pattern or sampling points with exponentially
 * increasing distances, respectively.
 *
 * Example:
 * \code
 * auto gaussian = std::make_shared<GaussianModel1D>();
 *
 * // define a sampling range from -2 to 2
 * auto range = SamplingRange(-2.0f, 2.0f);
 * // sample 5 (equidistantly spaced) points from the model
 * auto sampledSeries = XYDataSeries::sampledFromModel(gaussian, range, 5);
 *
 * qInfo() << sampledSeries.data();
 * // output: (QPointF(-2,0.053991), QPointF(-1,0.241971), QPointF(0,0.398942), QPointF(1,0.241971), QPointF(2,0.053991))
 * \endcode
 */
XYDataSeries XYDataSeries::sampledFromModel(std::shared_ptr<AbstractDataModel> dataModel,
                                            SamplingRange samplingRange,
                                            uint nbSamples,
                                            Sampling samplingPattern)
{
    return sampledFromModel(*dataModel, samplingRange.start(), samplingRange.end(),
                            nbSamples, samplingPattern);
}

/*!
 * Creates an XYDataSeries by sampling values from \a dataModel at \a nbSamples points covering
 * the range [\a from, \a to]. Sampling points will be distributed across the range
 * according to the flag specified by \a samplingPattern. This can be either Linear or Exponential,
 * resulting in a linear (i.e. equidistant samples) pattern or sampling points with exponentially
 * increasing distances, respectively.
 *
 * Example:
 * \code
 * GaussianModel1D gaussian;
 * // sample 5 values in the range from -2 to 2 (equidistantly spaced)
 * auto sampledSeries = XYDataSeries::sampledFromModel(gaussian, -2.0f, 2.0f, 5);
 *
 * qInfo() << sampledSeries.data();
 * // output: (QPointF(-2,0.053991), QPointF(-1,0.241971), QPointF(0,0.398942), QPointF(1,0.241971), QPointF(2,0.053991))
 * \endcode
 */
XYDataSeries XYDataSeries::sampledFromModel(const AbstractDataModel& dataModel, float from,
                                            float to, uint nbSamples, Sampling samplingPattern)
{
    switch (samplingPattern) {
    case Linear:
        return sampledFromModel(dataModel, SamplingRange::linspace(from, to, nbSamples));
    case Exponential:
        return sampledFromModel(dataModel, SamplingRange::expspace(from, to, nbSamples));
    }

    return XYDataSeries();
}

/*!
 * Creates an XYDataSeries by sampling values from \a dataModel at \a nbSamples points covering
 * the range [\a from, \a to]. Sampling points will be distributed across the range
 * according to the flag specified by \a samplingPattern. This can be either Linear or Exponential,
 * resulting in a linear (i.e. equidistant samples) pattern or sampling points with exponentially
 * increasing distances, respectively.
 *
 * Example:
 * \code
 * auto gaussian = std::make_shared<GaussianModel1D>();
 * // sample 5 values in the range from -2 to 2 (equidistantly spaced)
 * auto sampledSeries = XYDataSeries::sampledFromModel(gaussian, -2.0f, 2.0f, 5);
 *
 * qInfo() << sampledSeries.data();
 * // output: (QPointF(-2,0.053991), QPointF(-1,0.241971), QPointF(0,0.398942), QPointF(1,0.241971), QPointF(2,0.053991))
 * \endcode
 */
XYDataSeries XYDataSeries::sampledFromModel(std::shared_ptr<AbstractDataModel> dataModel, float from,
                                            float to, uint nbSamples, Sampling samplingPattern)
{
    return sampledFromModel(*dataModel, from, to, nbSamples, samplingPattern);
}

/*!
 * Appends the (x,y)-tuple \a sample to this series.
 */
void XYDataSeries::append(const QPointF& sample) { _data.append(sample); }

/*!
 * Appends the (\a x,\a y)-tuple to this series.
 */
void XYDataSeries::append(float x, float y) { _data.append(QPointF(x,y)); }

/*!
 * Appends all points from \a series to this series.
 */
void XYDataSeries::append(const QList<QPointF>& series) { _data.append(series); }

/*!
 * Removes the data point at index position \a idx. \ idx must be a valid index position in the
 * series.
 */
void XYDataSeries::remove(uint idx) { _data.removeAt(idx); }

/*!
 * Removes the data point \a sample from the series. Does nothing if this series contains no such
 * point. If \a sample is contained multiple times, all occurrences are removed.
 */
void XYDataSeries::remove(const QPointF &sample) { _data.removeAll(sample); }

/*!
 * Returns a (modifiable) reference to the data of this series.
 */
QList<QPointF>& XYDataSeries::data() { return _data; }



} // namespace CTL
