#include "thresholdvolumesparsifier.h"
#include "img/voxelvolume.h"

namespace CTL {

/*!
 * \brief Creates a ThresholdVolumeSparsifier instance and sets the threshold for the sparsifying
 * operation to \a threshold.
 */
ThresholdVolumeSparsifier::ThresholdVolumeSparsifier(float threshold)
    : _thresh(threshold)
{
}

/*!
 * \brief Sets the threshold for the sparsifying operation to \a threshold.
 */
void ThresholdVolumeSparsifier::setThreshold(float threshold)
{
    _thresh = threshold;
}

/*!
 * \brief Returns the threshold for the sparsifying operation.
 */
float ThresholdVolumeSparsifier::threshold() const
{
    return _thresh;
}

/*!
 * \copybrief AbstractVolumeSparsifier::sparsify
 *
 * Simple threshold-based sparsifying transform in which all voxels from \a volume that exceed the
 * threshold (set in constructor ot via setThreshold()) will be added to a SparseVoxelVolume, which
 * gets returned in the end.
 *
 * Example:
 * \code
 *  // create a volume with 100³ voxels and fill in uniform random values from [0, 1.5]
 *  auto volume = VoxelVolume<float>::cube(100, 1.0f, 0.0f);
 *  std::generate(volume.begin(), volume.end(),
 *                [] () { return QRandomGenerator::global()->bounded(1.5f); });
 *
 *  qInfo() << volume.totalVoxelCount();
 *  // output: 1000000
 *
 *  // sparsify with a threshold of 0.75
 *  const auto sparseVol = ThresholdVolumeSparsifier(0.75f).sparsify(volume);
 *
 *  qInfo() << sparseVol.nbVoxels();
 *  // output: 498925
 * \endcode
 */
SparseVoxelVolume ThresholdVolumeSparsifier::sparsify(const VoxelVolume<float>& volume)
{
    const auto& offset = volume.offset();
    const auto& dim    = volume.dimensions();
    const auto voxSize = SparseVoxelVolume::VoxelSize{ volume.voxelSize().x,
                                                       volume.voxelSize().y,
                                                       volume.voxelSize().z };

    const std::array<float, 3> volCorner { offset.x - 0.5f * float(dim.x - 1) * voxSize.x,
                                           offset.y - 0.5f * float(dim.y - 1) * voxSize.y,
                                           offset.z - 0.5f * float(dim.z - 1) * voxSize.z };

    std::vector<SparseVoxelVolume::SingleVoxel> sparseList;
    for(uint z = 0; z < volume.dimensions().z; ++z)
        for(uint y = 0; y < volume.dimensions().y; ++y)
            for(uint x = 0; x < volume.dimensions().x; ++x)
            {
                const auto value = volume(x,y,z);
                if(value >= _thresh)
                {
                    SparseVoxelVolume::SingleVoxel vox{ static_cast<float>(x) * voxSize.x + volCorner[0],
                                                        static_cast<float>(y) * voxSize.y + volCorner[1],
                                                        static_cast<float>(z) * voxSize.z + volCorner[2],
                                                        value };
                    sparseList.push_back(vox);
                }
            }

    return SparseVoxelVolume(voxSize, std::move(sparseList));
}


} // namespace CTL
