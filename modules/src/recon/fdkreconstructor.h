#ifndef CTL_FDKRECONSTRUCTOR_H
#define CTL_FDKRECONSTRUCTOR_H

#include "acquisition/acquisitionsetup.h"
#include "mat/matrix.h"
#include "recon/sfpbackprojector.h"

namespace CTL {

class AbstractProjectionFilter;

namespace OCL {

/*!
 * \brief The FDKReconstructor class implements the FDK reconstruction algorithm for reconstruction
 * of cone-beam projections from a circular scan orbit.
 *
 * This reconstruction method can be applied to data that fulfills the following requirements:
 * - projection data has only a single module (flat panel projections) (*)
 * - projection data has at least 3 (different) views (*)
 * - the scan trajectory is a circular orbit or at least a segment thereof (i.e. 'short scan'
 * covering 180° plus fan angle)
 * - the detector rows lay tangential on the scan trajectory
 *
 * (*) These are hard requirements. Reconstruction attempts on data that violate one (or both) of
 * them are aborted.
 * If any of the other requirements is (partially or fully) violated, reconstruction is still
 * carried out, but the result may be improper.
 * Requirement compliance can be checked with isApplicable().
 *
 * The algorithmic structure (or workflow) is described in reconstructToPlain().
 *
 * You can also find a lot of information on the general usage of reconstructors in the
 * documentation of AbstractReconstructor.
 *
 * Example: reconstructing a simulated (short) scan of a cylinder phantom
 * \code
 *  // create a cylinder phantom
 *  const auto volume = VoxelVolume<float>::cylinderZ(50.0f, 100.0f, 1.0f, 0.02f);
 *
 *  // create an AcquisitionSetup for a flat panel short scan configuration
 *  AcquisitionSetup setup(makeSimpleCTSystem<blueprints::GenericCarmCT>(), 100);
 *  setup.applyPreparationProtocol(protocols::ShortScanTrajectory(700.0));
 *
 *  // simulate projections
 *  const auto projections = OCL::RayCasterProjector().configureAndProject(setup, volume);
 *
 *  // create the FDKReconstructor and check if its applicable to our 'setup' (optional)
 *  OCL::FDKReconstructor rec;
 *  qInfo() << rec.isApplicableTo(setup);  // output: true
 *
 *  // do the actual reconstruction
 *  bool ok;  // 'ok' is used to capture the success of the procedure
 *  const auto recon = rec.configureAndReconstruct(setup, projections,
 *                                                 VoxelVolume<float>(volume.dimensions(),
 *                                                                    volume.voxelSize()), &ok);
 *  qInfo() << ok;  // output: true
 *
 *  // visualize the result (note: requires 'gui_widgets.pri' submodule)
 *  gui::plot(recon);
 * \endcode
 * ![FDK reconstruction of a cylinder phantom from a 100-view short scan.](fdk_reco.png)
 */
class FDKReconstructor : public AbstractReconstructor
{ 
    CTL_TYPE_ID(100)

public: void configure(const AcquisitionSetup& setup) override;
public: bool reconstructToPlain(const ProjectionDataView& projections,
                                VoxelVolume<float>& targetVolume) override;
public:
    using ProjectionFilter = AbstractProjectionFilter;

    explicit FDKReconstructor(float q = 1.0f,
                              bool useSFPBackprojector = false);
    explicit FDKReconstructor(std::unique_ptr<ProjectionFilter> projectionFilter,
                              float q = 1.0f,
                              bool useSFPBackprojector = false);
    explicit FDKReconstructor(ProjectionFilter* projectionFilter,
                              float q = 1.0f,
                              bool useSFPBackprojector = false);
    void setProjectionFilter(std::unique_ptr<ProjectionFilter> projectionFilter);
    void setProjectionFilter(ProjectionFilter* projectionFilter);
    void setRevisedParkerWeightQ(float q);
    void useSFPBackprojector(SFPBackprojector::FootprintType footprintType = SFPBackprojector::TR);
    void useSimpleBackrojector();

    SFPBackprojector::FootprintType footprintType() const;
    bool isSFPBackprojectorInUse() const;
    bool isSimpleBackprojectorInUse() const;
    float revisedParkerWeightQ() const;

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    // AbstractReconstructor interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

private:
    AcquisitionSetup _setup;
    std::unique_ptr<ProjectionFilter> _projectionFilter;
    float _q = 1.0f;
    bool _useSFPBackprojector = false;
    SFPBackprojector::FootprintType _footprintType = SFPBackprojector::TR;

    bool consistencyChecks(const ProjectionDataView& projections) const;

    static float estimateSDD(AcquisitionSetup& setup);
    static float estimateSID(AcquisitionSetup& setup);
    static bool isCircularSourceOrbit(AcquisitionSetup& setup);
    static bool isDetectorTangentialToOrbit(AcquisitionSetup& setup);
};

} // namespace OCL

namespace assist{

/*!
 * \brief The Circle3D struct represents a circle in three dimensions.
 *
 * The circle is represented by its center point, its radius (in mm), and the normal vector
 * (normalized).
 */
struct Circle3D{
    mat::Matrix<3,1> center;
    mat::Matrix<3,1> normal;
    double radius;

    double distanceToPoint(const mat::Matrix<3,1>& pt) const;

    static Circle3D fromThreePoints(const mat::Matrix<3,1>& p1,
                                    const mat::Matrix<3,1>& p2,
                                    const mat::Matrix<3,1>& p3);
};

} // namespace assist

} // namespace CTL

#endif // CTL_FDKRECONSTRUCTOR_H
